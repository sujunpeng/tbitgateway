package com.tbit.test;

import com.tbit.uqbike.service.redis.RedisService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by MyWin on 2018/6/14 0014.
 */
public class RedisTest implements Runnable {
    static volatile AtomicInteger cnt = new AtomicInteger(0);
    private RedisService redisService;
    private CountDownLatch countDownLatch;

    public static void main(String[] args) {
        ApplicationContext springFacCtx = new ClassPathXmlApplicationContext("applicationContext.xml");
        int threadCnt = 50;
        RedisService redisService = (RedisService) springFacCtx.getBean("redisService");
        CountDownLatch countDownLatch = new CountDownLatch(threadCnt);
        for (int i = 0; i < threadCnt; i++) {
            Thread thread = new Thread(new RedisTest(redisService, countDownLatch));
            thread.start();
        }
        long start = 0;
        while (true) {
            long end = System.currentTimeMillis();
            int c = cnt.getAndSet(0);
            long ts = end - start;
            if (c != 0 && start != 0) {
                System.out.println(String.format("use %d ms,write %d,avr %f", ts, c, c * 1000.0 / ts));
            }
            try {
                start = System.currentTimeMillis();
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public RedisTest(RedisService redisService, CountDownLatch countDownLatch) {
        this.redisService = redisService;
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void run() {
        Random random = new Random();
        while (true) {
            String hashId = String.format("uq.test.%d", random.nextInt(10000));
            redisService.get(hashId, "tLp");
            cnt.incrementAndGet();
        }
    }
}
