package com.tbit.utils;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019-11-12 14:03
 */
public class PlatformPointConst {
    public static final Integer CONN_CNT = 1;
    public static final Integer TER_CNT = 2;
    public static final Integer TER_NEW_CONN = 3;
}
