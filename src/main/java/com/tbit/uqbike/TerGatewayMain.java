package com.tbit.uqbike;


import com.tbit.uqbike.protocol.AnalyzeImpl.AAutoProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.AutoProtocolImpl;
import com.tbit.uqbike.service.db.DbService;
import com.tbit.uqbike.service.mq.MQProducer;
import com.tbit.uqbike.service.redis.RedisService;
import com.tbit.uqbike.tergateway.config.TerGatewayConfig;
import com.tbit.uqbike.tergateway.data.TerGatewayData;
import com.tbit.uqbike.tergateway.event.ITerEvent;
import com.tbit.uqbike.tergateway.event.impl.TerEventImpl;
import com.tbit.uqbike.tergateway.service.mynetty.TbitNettyServer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.integration.mqtt.outbound.MqttPahoMessageHandler;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by MyWin on 2017/4/24.
 */
public class TerGatewayMain {
    public final static ApplicationContext springFacCtx;
    public final static ITerEvent terEvnet;
    public static MqttPahoMessageHandler mqttSend = null;
    public final static AAutoProtocol autoProtocol;
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TerGatewayMain.class);

    static {
        /*加载spring配置文件*/
        springFacCtx = new ClassPathXmlApplicationContext("applicationContext.xml");
        /*初始化终端事件处理*/
        terEvnet = new TerEventImpl();
        /* 识别协议类 */
        autoProtocol = new AutoProtocolImpl();
        // mqtt 不是必须的
//        try {
//            mqttSend = (MqttPahoMessageHandler) springFacCtx.getBean("mqttSend");
//        } catch (Exception e) {
//            mqttSend = null;
//            logger.error("", e);
//        }
    }

    private static RedisService redisService;

    public static RedisService getRedis() {
        if (null == redisService) {
            redisService = (RedisService) springFacCtx.getBean("redisService");
        }
        return redisService;
    }

    public static MQProducer getProducer() {
        return (MQProducer) springFacCtx.getBean("rabbitmqService");
    }

    public static DbService getDbService() {
        return (DbService) springFacCtx.getBean("dbService");
    }

    public static void main(String[] args) {
        // 初始化内存数据
        TerGatewayData.startDataService();
        try {
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    TerGatewayData.showTerGatewayData();
                }
            }, 0, 30 * 1000);// 设定指定的时间time,此处为2000毫秒
        } catch (Exception e) {
            logger.error("启动数据循环输出线程", e);
            return;
        }

        //region start tcp server
        try {
            TbitNettyServer tns = new TbitNettyServer();
            logger.info(String.format("Start TCP Server(%s:%d)...", TerGatewayConfig.TcpListenIp, TerGatewayConfig.TcpListenPort));
            tns.service(TerGatewayConfig.TcpListenIp, TerGatewayConfig.TcpListenPort);
            //tns.ws_service(TerGatewayConfig.TcpListenIp, TerGatewayConfig.TcpListenPort);
        } catch (Exception e) {
            logger.error("启动TCP数据服务器异常", e);
            return;
        }
        //endregion
    }
}
