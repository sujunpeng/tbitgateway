package com.tbit.uqbike.test;

import com.tbit.uqbike.TerGatewayMain;
import com.tbit.uqbike.service.db.DbService;
import com.tbit.uqbike.service.redis.RedisService;
import com.tbit.uqbike.tergateway.pojo.TerInfo;
import com.tbit.uqbike.tergateway.pojo.TerMsg;
import com.tbit.uqbike.util.CharsetName;
import com.tbit.uqbike.util.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.tbit.uqbike.service.mq.*;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by MyWin on 2017/4/27.
 */
public class ComponentTest {
    private static Logger logger = LoggerFactory.getLogger(ComponentTest.class);

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");


        //region logger test
        logger.error("logger error test");
        logger.info("logger info test");
        logger.debug("logger debug test");
        //endregion

        //region rabbitmq test
        logger.info("rabbitmq test");
        byte[] data;
        try {
            data = "hello world!".getBytes(CharsetName.UTF_8);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            data = new byte[0];
        }
        Message msg = new Message(data, new org.springframework.amqp.core.MessageProperties());
        MQProducer producer = (MQProducer) context.getBean("rabbitmqService");
        for (int i = 0; i < 3; i++) {
            producer.sendDataToQueue("myname1234", msg);
            logger.info("rabbitmq send msg test");
        }
        //endregion

        //region mybatis test
        logger.info("mybatis test");
        DbService dbService = (DbService) context.getBean("dbService");
        TerMsg terMsg = new TerMsg();
        terMsg.setDt(new Date(0));
        terMsg.setMachineNO("135790246");
        terMsg.setMsg("hello world,test");
        List<TerMsg> terMsgList = new ArrayList<>();
        terMsgList.add(terMsg);
        dbService.batInsertTerMsg(terMsgList);
        logger.info("mybatis test ok");
        TerInfo terInfo = dbService.getTerInfoByMno("021032003");
        logger.info(terInfo.getMachineNO());
        logger.info("joinDt:" + DateUtil.ymdHmsDU.format(terInfo.getJoinTime()));
        logger.info("joinDt:");
        //endregion
    }

    public static void TestFun() {
        //region rabbitmq test
        logger.info("rabbitmq test");
        for (int i = 0; i < 1; i++) {
            TerGatewayMain.getProducer().sendDataToQueue("myname1234", "hello world");
            logger.info("rabbitmq send msg test");
        }
        //endregion

        //region mybatis test
        logger.info("mybatis test");
        TerMsg terMsg = new TerMsg();
        terMsg.setDt(new Date());
        terMsg.setMachineNO("135790246");
        terMsg.setMsg("hello world");
        List<TerMsg> terMsgList = new ArrayList<>();
        terMsgList.add(terMsg);
        TerGatewayMain.getDbService().batInsertTerMsg(terMsgList);
        logger.info("mybatis test ok");
        //endregion
    }
}
