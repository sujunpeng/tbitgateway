/*
// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice version 3.2.1

package com.tbit.uqbike.service.ice.icebase.CellIdManage;

public final class DPointPrxHelper extends Ice.ObjectPrxHelperBase implements DPointPrx {
    public static DPointPrx
    checkedCast(Ice.ObjectPrx __obj) {
        DPointPrx __d = null;
        if (__obj != null) {
            try {
                __d = (DPointPrx) __obj;
            } catch (ClassCastException ex) {
                if (__obj.ice_isA("::CellIdManage::DPoint")) {
                    DPointPrxHelper __h = new DPointPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static DPointPrx
    checkedCast(Ice.ObjectPrx __obj, java.util.Map<String, String> __ctx) {
        DPointPrx __d = null;
        if (__obj != null) {
            try {
                __d = (DPointPrx) __obj;
            } catch (ClassCastException ex) {
                if (__obj.ice_isA("::CellIdManage::DPoint", __ctx)) {
                    DPointPrxHelper __h = new DPointPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static DPointPrx
    checkedCast(Ice.ObjectPrx __obj, String __facet) {
        DPointPrx __d = null;
        if (__obj != null) {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try {
                if (__bb.ice_isA("::CellIdManage::DPoint")) {
                    DPointPrxHelper __h = new DPointPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            } catch (Ice.FacetNotExistException ex) {
            }
        }
        return __d;
    }

    public static DPointPrx
    checkedCast(Ice.ObjectPrx __obj, String __facet, java.util.Map<String, String> __ctx) {
        DPointPrx __d = null;
        if (__obj != null) {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try {
                if (__bb.ice_isA("::CellIdManage::DPoint", __ctx)) {
                    DPointPrxHelper __h = new DPointPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            } catch (Ice.FacetNotExistException ex) {
            }
        }
        return __d;
    }

    public static DPointPrx
    uncheckedCast(Ice.ObjectPrx __obj) {
        DPointPrx __d = null;
        if (__obj != null) {
            DPointPrxHelper __h = new DPointPrxHelper();
            __h.__copyFrom(__obj);
            __d = __h;
        }
        return __d;
    }

    public static DPointPrx
    uncheckedCast(Ice.ObjectPrx __obj, String __facet) {
        DPointPrx __d = null;
        if (__obj != null) {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            DPointPrxHelper __h = new DPointPrxHelper();
            __h.__copyFrom(__bb);
            __d = __h;
        }
        return __d;
    }

    @Override
    protected Ice._ObjectDelM
    __createDelegateM() {
        return new _DPointDelM();
    }

    @Override
    protected Ice._ObjectDelD
    __createDelegateD() {
        return new _DPointDelD();
    }

    public static void
    __write(IceInternal.BasicStream __os, DPointPrx v) {
        __os.writeProxy(v);
    }

    public static DPointPrx
    __read(IceInternal.BasicStream __is) {
        Ice.ObjectPrx proxy = __is.readProxy();
        if (proxy != null) {
            DPointPrxHelper result = new DPointPrxHelper();
            result.__copyFrom(proxy);
            return result;
        }
        return null;
    }
}
*/
