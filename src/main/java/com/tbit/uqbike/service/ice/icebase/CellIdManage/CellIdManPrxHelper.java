// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice version 3.2.1

package com.tbit.uqbike.service.ice.icebase.CellIdManage;

public final class CellIdManPrxHelper extends Ice.ObjectPrxHelperBase implements CellIdManPrx {
    @Override
    public DPoint
    GetDpCellid(String cellid) {
        return GetDpCellid(cellid, null, false);
    }

    @Override
    public DPoint
    GetDpCellid(String cellid, java.util.Map<String, String> __ctx) {
        return GetDpCellid(cellid, __ctx, true);
    }

    @SuppressWarnings("unchecked")
    private DPoint
    GetDpCellid(String cellid, java.util.Map<String, String> __ctx, boolean __explicitCtx) {
        if (__explicitCtx && __ctx == null) {
            __ctx = _emptyContext;
        }
        int __cnt = 0;
        while (true) {
            Ice._ObjectDel __delBase = null;
            try {
                __checkTwowayOnly("GetDpCellid");
                __delBase = __getDelegate();
                _CellIdManDel __del = (_CellIdManDel) __delBase;
                return __del.GetDpCellid(cellid, __ctx);
            } catch (IceInternal.LocalExceptionWrapper __ex) {
                __handleExceptionWrapper(__delBase, __ex);
            } catch (Ice.LocalException __ex) {
                __cnt = __handleException(__delBase, __ex, __cnt);
            }
        }
    }

    @Override
    public void
    UpMsg(int msgId, String strParam) {
        UpMsg(msgId, strParam, null, false);
    }

    @Override
    public void
    UpMsg(int msgId, String strParam, java.util.Map<String, String> __ctx) {
        UpMsg(msgId, strParam, __ctx, true);
    }

    @SuppressWarnings("unchecked")
    private void
    UpMsg(int msgId, String strParam, java.util.Map<String, String> __ctx, boolean __explicitCtx) {
        if (__explicitCtx && __ctx == null) {
            __ctx = _emptyContext;
        }
        int __cnt = 0;
        while (true) {
            Ice._ObjectDel __delBase = null;
            try {
                __delBase = __getDelegate();
                _CellIdManDel __del = (_CellIdManDel) __delBase;
                __del.UpMsg(msgId, strParam, __ctx);
                return;
            } catch (IceInternal.LocalExceptionWrapper __ex) {
                __handleExceptionWrapper(__delBase, __ex);
            } catch (Ice.LocalException __ex) {
                __cnt = __handleException(__delBase, __ex, __cnt);
            }
        }
    }

    @Override
    public void
    collectCellID(CellidInfo[] cellInfoArr) {
        collectCellID(cellInfoArr, null, false);
    }

    @Override
    public void
    collectCellID(CellidInfo[] cellInfoArr, java.util.Map<String, String> __ctx) {
        collectCellID(cellInfoArr, __ctx, true);
    }

    @SuppressWarnings("unchecked")
    private void
    collectCellID(CellidInfo[] cellInfoArr, java.util.Map<String, String> __ctx, boolean __explicitCtx) {
        if (__explicitCtx && __ctx == null) {
            __ctx = _emptyContext;
        }
        int __cnt = 0;
        while (true) {
            Ice._ObjectDel __delBase = null;
            try {
                __delBase = __getDelegate();
                _CellIdManDel __del = (_CellIdManDel) __delBase;
                __del.collectCellID(cellInfoArr, __ctx);
                return;
            } catch (IceInternal.LocalExceptionWrapper __ex) {
                __handleExceptionWrapper(__delBase, __ex);
            } catch (Ice.LocalException __ex) {
                __cnt = __handleException(__delBase, __ex, __cnt);
            }
        }
    }

    public static CellIdManPrx
    checkedCast(Ice.ObjectPrx __obj) {
        CellIdManPrx __d = null;
        if (__obj != null) {
            try {
                __d = (CellIdManPrx) __obj;
            } catch (ClassCastException ex) {
                if (__obj.ice_isA("::CellIdManage::CellIdMan")) {
                    CellIdManPrxHelper __h = new CellIdManPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static CellIdManPrx
    checkedCast(Ice.ObjectPrx __obj, java.util.Map<String, String> __ctx) {
        CellIdManPrx __d = null;
        if (__obj != null) {
            try {
                __d = (CellIdManPrx) __obj;
            } catch (ClassCastException ex) {
                if (__obj.ice_isA("::CellIdManage::CellIdMan", __ctx)) {
                    CellIdManPrxHelper __h = new CellIdManPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static CellIdManPrx
    checkedCast(Ice.ObjectPrx __obj, String __facet) {
        CellIdManPrx __d = null;
        if (__obj != null) {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try {
                if (__bb.ice_isA("::CellIdManage::CellIdMan")) {
                    CellIdManPrxHelper __h = new CellIdManPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            } catch (Ice.FacetNotExistException ex) {
            }
        }
        return __d;
    }

    public static CellIdManPrx
    checkedCast(Ice.ObjectPrx __obj, String __facet, java.util.Map<String, String> __ctx) {
        CellIdManPrx __d = null;
        if (__obj != null) {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try {
                if (__bb.ice_isA("::CellIdManage::CellIdMan", __ctx)) {
                    CellIdManPrxHelper __h = new CellIdManPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            } catch (Ice.FacetNotExistException ex) {
            }
        }
        return __d;
    }

    public static CellIdManPrx
    uncheckedCast(Ice.ObjectPrx __obj) {
        CellIdManPrx __d = null;
        if (__obj != null) {
            CellIdManPrxHelper __h = new CellIdManPrxHelper();
            __h.__copyFrom(__obj);
            __d = __h;
        }
        return __d;
    }

    public static CellIdManPrx
    uncheckedCast(Ice.ObjectPrx __obj, String __facet) {
        CellIdManPrx __d = null;
        if (__obj != null) {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            CellIdManPrxHelper __h = new CellIdManPrxHelper();
            __h.__copyFrom(__bb);
            __d = __h;
        }
        return __d;
    }

    @Override
    protected Ice._ObjectDelM
    __createDelegateM() {
        return new _CellIdManDelM();
    }

    @Override
    protected Ice._ObjectDelD
    __createDelegateD() {
        return new _CellIdManDelD();
    }

    public static void
    __write(IceInternal.BasicStream __os, CellIdManPrx v) {
        __os.writeProxy(v);
    }

    public static CellIdManPrx
    __read(IceInternal.BasicStream __is) {
        Ice.ObjectPrx proxy = __is.readProxy();
        if (proxy != null) {
            CellIdManPrxHelper result = new CellIdManPrxHelper();
            result.__copyFrom(proxy);
            return result;
        }
        return null;
    }
}
