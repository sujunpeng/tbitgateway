/*
// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice version 3.2.1

package com.tbit.uqbike.service.ice.icebase.CellIdManage;

public final class CellidInfoPrxHelper extends Ice.ObjectPrxHelperBase implements CellidInfoPrx {
    public static CellidInfoPrx
    checkedCast(Ice.ObjectPrx __obj) {
        CellidInfoPrx __d = null;
        if (__obj != null) {
            try {
                __d = (CellidInfoPrx) __obj;
            } catch (ClassCastException ex) {
                if (__obj.ice_isA("::CellIdManage::CellidInfo")) {
                    CellidInfoPrxHelper __h = new CellidInfoPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static CellidInfoPrx
    checkedCast(Ice.ObjectPrx __obj, java.util.Map<String, String> __ctx) {
        CellidInfoPrx __d = null;
        if (__obj != null) {
            try {
                __d = (CellidInfoPrx) __obj;
            } catch (ClassCastException ex) {
                if (__obj.ice_isA("::CellIdManage::CellidInfo", __ctx)) {
                    CellidInfoPrxHelper __h = new CellidInfoPrxHelper();
                    __h.__copyFrom(__obj);
                    __d = __h;
                }
            }
        }
        return __d;
    }

    public static CellidInfoPrx
    checkedCast(Ice.ObjectPrx __obj, String __facet) {
        CellidInfoPrx __d = null;
        if (__obj != null) {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try {
                if (__bb.ice_isA("::CellIdManage::CellidInfo")) {
                    CellidInfoPrxHelper __h = new CellidInfoPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            } catch (Ice.FacetNotExistException ex) {
            }
        }
        return __d;
    }

    public static CellidInfoPrx
    checkedCast(Ice.ObjectPrx __obj, String __facet, java.util.Map<String, String> __ctx) {
        CellidInfoPrx __d = null;
        if (__obj != null) {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            try {
                if (__bb.ice_isA("::CellIdManage::CellidInfo", __ctx)) {
                    CellidInfoPrxHelper __h = new CellidInfoPrxHelper();
                    __h.__copyFrom(__bb);
                    __d = __h;
                }
            } catch (Ice.FacetNotExistException ex) {
            }
        }
        return __d;
    }

    public static CellidInfoPrx
    uncheckedCast(Ice.ObjectPrx __obj) {
        CellidInfoPrx __d = null;
        if (__obj != null) {
            CellidInfoPrxHelper __h = new CellidInfoPrxHelper();
            __h.__copyFrom(__obj);
            __d = __h;
        }
        return __d;
    }

    public static CellidInfoPrx
    uncheckedCast(Ice.ObjectPrx __obj, String __facet) {
        CellidInfoPrx __d = null;
        if (__obj != null) {
            Ice.ObjectPrx __bb = __obj.ice_facet(__facet);
            CellidInfoPrxHelper __h = new CellidInfoPrxHelper();
            __h.__copyFrom(__bb);
            __d = __h;
        }
        return __d;
    }

    @Override
    protected Ice._ObjectDelM
    __createDelegateM() {
        return new _CellidInfoDelM();
    }

    @Override
    protected Ice._ObjectDelD
    __createDelegateD() {
        return new _CellidInfoDelD();
    }

    public static void
    __write(IceInternal.BasicStream __os, CellidInfoPrx v) {
        __os.writeProxy(v);
    }

    public static CellidInfoPrx
    __read(IceInternal.BasicStream __is) {
        Ice.ObjectPrx proxy = __is.readProxy();
        if (proxy != null) {
            CellidInfoPrxHelper result = new CellidInfoPrxHelper();
            result.__copyFrom(proxy);
            return result;
        }
        return null;
    }
}
*/
