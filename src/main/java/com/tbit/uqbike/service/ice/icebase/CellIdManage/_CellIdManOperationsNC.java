// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice version 3.2.1

package com.tbit.uqbike.service.ice.icebase.CellIdManage;

public interface _CellIdManOperationsNC {
    DPoint GetDpCellid(String cellid);

    void collectCellID(CellidInfo[] cellInfoArr);

    void UpMsg(int msgId, String strParam);
}
