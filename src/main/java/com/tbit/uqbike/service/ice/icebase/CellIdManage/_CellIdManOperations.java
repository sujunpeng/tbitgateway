// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice version 3.2.1

package com.tbit.uqbike.service.ice.icebase.CellIdManage;

public interface _CellIdManOperations {
    DPoint GetDpCellid(String cellid, Ice.Current __current);

    void collectCellID(CellidInfo[] cellInfoArr, Ice.Current __current);

    void UpMsg(int msgId, String strParam, Ice.Current __current);
}
