// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice version 3.2.1

package com.tbit.uqbike.service.ice.icebase.CellIdManage;

public final class _CellIdManDelM extends Ice._ObjectDelM implements _CellIdManDel {
    @Override
    public DPoint
    GetDpCellid(String cellid, java.util.Map<String, String> __ctx)
            throws IceInternal.LocalExceptionWrapper {
        IceInternal.Outgoing __og = __connection.getOutgoing(__reference, "GetDpCellid", Ice.OperationMode.Normal, __ctx, __compress);
        try {
            try {
                IceInternal.BasicStream __os = __og.os();
                __os.writeString(cellid);
            } catch (Ice.LocalException __ex) {
                __og.abort(__ex);
            }
            boolean __ok = __og.invoke();
            try {
                IceInternal.BasicStream __is = __og.is();
                if (!__ok) {
                    try {
                        __is.throwException();
                    } catch (Ice.UserException __ex) {
                        throw new Ice.UnknownUserException(__ex.ice_name());
                    }
                }
                DPointHolder __ret = new DPointHolder();
                __is.readObject(__ret.getPatcher());
                __is.readPendingObjects();
                return __ret.value;
            } catch (Ice.LocalException __ex) {
                throw new IceInternal.LocalExceptionWrapper(__ex, false);
            }
        } finally {
            __connection.reclaimOutgoing(__og);
        }
    }

    @Override
    public void
    UpMsg(int msgId, String strParam, java.util.Map<String, String> __ctx)
            throws IceInternal.LocalExceptionWrapper {
        IceInternal.Outgoing __og = __connection.getOutgoing(__reference, "UpMsg", Ice.OperationMode.Normal, __ctx, __compress);
        try {
            try {
                IceInternal.BasicStream __os = __og.os();
                __os.writeInt(msgId);
                __os.writeString(strParam);
            } catch (Ice.LocalException __ex) {
                __og.abort(__ex);
            }
            boolean __ok = __og.invoke();
            try {
                IceInternal.BasicStream __is = __og.is();
                if (!__ok) {
                    try {
                        __is.throwException();
                    } catch (Ice.UserException __ex) {
                        throw new Ice.UnknownUserException(__ex.ice_name());
                    }
                }
            } catch (Ice.LocalException __ex) {
                throw new IceInternal.LocalExceptionWrapper(__ex, false);
            }
        } finally {
            __connection.reclaimOutgoing(__og);
        }
    }

    @Override
    public void
    collectCellID(CellidInfo[] cellInfoArr, java.util.Map<String, String> __ctx)
            throws IceInternal.LocalExceptionWrapper {
        IceInternal.Outgoing __og = __connection.getOutgoing(__reference, "collectCellID", Ice.OperationMode.Normal, __ctx, __compress);
        try {
            try {
                IceInternal.BasicStream __os = __og.os();
                CellidInfoArryHelper.write(__os, cellInfoArr);
                __os.writePendingObjects();
            } catch (Ice.LocalException __ex) {
                __og.abort(__ex);
            }
            boolean __ok = __og.invoke();
            try {
                IceInternal.BasicStream __is = __og.is();
                if (!__ok) {
                    try {
                        __is.throwException();
                    } catch (Ice.UserException __ex) {
                        throw new Ice.UnknownUserException(__ex.ice_name());
                    }
                }
            } catch (Ice.LocalException __ex) {
                throw new IceInternal.LocalExceptionWrapper(__ex, false);
            }
        } finally {
            __connection.reclaimOutgoing(__og);
        }
    }
}
