// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice version 3.2.1

package com.tbit.uqbike.service.ice.icebase.CellIdManage;

public interface CellIdManPrx extends Ice.ObjectPrx {
    public DPoint GetDpCellid(String cellid);

    public DPoint GetDpCellid(String cellid, java.util.Map<String, String> __ctx);

    public void collectCellID(CellidInfo[] cellInfoArr);

    public void collectCellID(CellidInfo[] cellInfoArr, java.util.Map<String, String> __ctx);

    public void UpMsg(int msgId, String strParam);

    public void UpMsg(int msgId, String strParam, java.util.Map<String, String> __ctx);
}
