// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice version 3.2.1

package com.tbit.uqbike.service.ice.icebase.CellIdManage;

public final class DPointHolder {
    public DPointHolder() {
    }

    public DPointHolder(DPoint value) {
        this.value = value;
    }

    public class Patcher implements IceInternal.Patcher {
        @Override
        public void
        patch(Ice.Object v) {
            try {
                value = (DPoint) v;
            } catch (ClassCastException ex) {
                Ice.UnexpectedObjectException _e = new Ice.UnexpectedObjectException();
                _e.type = v.ice_id();
                _e.expectedType = type();
                throw _e;
            }
        }

        @Override
        public String
        type() {
            return "::CellIdManage::DPoint";
        }
    }

    public Patcher
    getPatcher() {
        return new Patcher();
    }

    public DPoint value;
}
