// **********************************************************************
//
// Copyright (c) 2003-2007 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice version 3.2.1

package com.tbit.uqbike.service.ice.icebase.CellIdManage;

public abstract class _CellIdManDisp extends Ice.ObjectImpl implements CellIdMan {
    protected void
    ice_copyStateFrom(Ice.Object __obj)
            throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    public static final String[] __ids =
            {
                    "::CellIdManage::CellIdMan",
                    "::Ice::Object"
            };

    @Override
    public boolean
    ice_isA(String s) {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    @Override
    public boolean
    ice_isA(String s, Ice.Current __current) {
        return java.util.Arrays.binarySearch(__ids, s) >= 0;
    }

    @Override
    public String[]
    ice_ids() {
        return __ids;
    }

    @Override
    public String[]
    ice_ids(Ice.Current __current) {
        return __ids;
    }

    @Override
    public String
    ice_id() {
        return __ids[0];
    }

    @Override
    public String
    ice_id(Ice.Current __current) {
        return __ids[0];
    }

    public static String
    ice_staticId() {
        return __ids[0];
    }

    @Override
    public final DPoint
    GetDpCellid(String cellid) {
        return GetDpCellid(cellid, null);
    }

    @Override
    public final void
    UpMsg(int msgId, String strParam) {
        UpMsg(msgId, strParam, null);
    }

    @Override
    public final void
    collectCellID(CellidInfo[] cellInfoArr) {
        collectCellID(cellInfoArr, null);
    }

    public static IceInternal.DispatchStatus
    ___GetDpCellid(CellIdMan __obj, IceInternal.Incoming __inS, Ice.Current __current) {
        __checkMode(Ice.OperationMode.Normal, __current.mode);
        IceInternal.BasicStream __is = __inS.is();
        IceInternal.BasicStream __os = __inS.os();
        String cellid;
        cellid = __is.readString();
        DPoint __ret = __obj.GetDpCellid(cellid, __current);
        __os.writeObject(__ret);
        __os.writePendingObjects();
        return IceInternal.DispatchStatus.DispatchOK;
    }

    public static IceInternal.DispatchStatus
    ___collectCellID(CellIdMan __obj, IceInternal.Incoming __inS, Ice.Current __current) {
        __checkMode(Ice.OperationMode.Normal, __current.mode);
        IceInternal.BasicStream __is = __inS.is();
        CellidInfo[] cellInfoArr;
        cellInfoArr = CellidInfoArryHelper.read(__is);
        __is.readPendingObjects();
        __obj.collectCellID(cellInfoArr, __current);
        return IceInternal.DispatchStatus.DispatchOK;
    }

    public static IceInternal.DispatchStatus
    ___UpMsg(CellIdMan __obj, IceInternal.Incoming __inS, Ice.Current __current) {
        __checkMode(Ice.OperationMode.Normal, __current.mode);
        IceInternal.BasicStream __is = __inS.is();
        int msgId;
        msgId = __is.readInt();
        String strParam;
        strParam = __is.readString();
        __obj.UpMsg(msgId, strParam, __current);
        return IceInternal.DispatchStatus.DispatchOK;
    }

    private final static String[] __all =
            {
                    "GetDpCellid",
                    "UpMsg",
                    "collectCellID",
                    "ice_id",
                    "ice_ids",
                    "ice_isA",
                    "ice_ping"
            };

    @Override
    public IceInternal.DispatchStatus
    __dispatch(IceInternal.Incoming in, Ice.Current __current) {
        int pos = java.util.Arrays.binarySearch(__all, __current.operation);
        if (pos < 0) {
            return IceInternal.DispatchStatus.DispatchOperationNotExist;
        }

        switch (pos) {
            case 0: {
                return ___GetDpCellid(this, in, __current);
            }
            case 1: {
                return ___UpMsg(this, in, __current);
            }
            case 2: {
                return ___collectCellID(this, in, __current);
            }
            case 3: {
                return ___ice_id(this, in, __current);
            }
            case 4: {
                return ___ice_ids(this, in, __current);
            }
            case 5: {
                return ___ice_isA(this, in, __current);
            }
            case 6: {
                return ___ice_ping(this, in, __current);
            }
        }

        assert (false);
        return IceInternal.DispatchStatus.DispatchOperationNotExist;
    }

    @Override
    public void
    __write(IceInternal.BasicStream __os) {
        __os.writeTypeId(ice_staticId());
        __os.startWriteSlice();
        __os.endWriteSlice();
        super.__write(__os);
    }

    @Override
    public void
    __read(IceInternal.BasicStream __is, boolean __rid) {
        if (__rid) {
            __is.readTypeId();
        }
        __is.startReadSlice();
        __is.endReadSlice();
        super.__read(__is, true);
    }

    @Override
    public void
    __write(Ice.OutputStream __outS) {
        Ice.MarshalException ex = new Ice.MarshalException();
        ex.reason = "type CellIdManage::CellIdMan was not generated with stream support";
        throw ex;
    }

    @Override
    public void
    __read(Ice.InputStream __inS, boolean __rid) {
        Ice.MarshalException ex = new Ice.MarshalException();
        ex.reason = "type CellIdManage::CellIdMan was not generated with stream support";
        throw ex;
    }
}
