package com.tbit.uqbike.service.ice.impl;


import com.tbit.uqbike.service.ice.icebase.CellIdManage.CellIdManPrx;
import com.tbit.uqbike.service.ice.icebase.CellIdManage.CellIdManPrxHelper;
import com.tbit.uqbike.service.ice.icebase.CellIdManage.CellidInfo;
import com.tbit.uqbike.service.ice.icebase.CellIdManage.DPoint;
import com.tbit.uqbike.tergateway.config.GateXmlConfig;

/**
 * Created by John on 2017/3/2.
 */
public class CellIdManageImpl {
    //region 类共享变量
    /**
     * 日志对象
     */
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(CellIdManageImpl.class);
    /**
     * 连接器
     */
    private static Ice.Communicator communicator = null;
    public static final String ConfigFile = "/ice.cellManage.properties";
    private static Ice.InitializationData initData;

    private static CellIdManPrx cellManageServerPrx = null;
    private static Ice.ObjectAdapter adapter = null;
    private static Ice.Identity ident = null;
    /***
     * 心跳超时计数
     */
    private static int signalCnt = 0;
    //endregion

    static {
        initData = new Ice.InitializationData();
        initData.properties = Ice.Util.createProperties();
        initData.properties.load(CellIdManageImpl.class.getResource(ConfigFile).getPath());
        communicator = Ice.Util.initialize(initData);
        communicator.addObjectFactory(DPoint.ice_factory(), DPoint.ice_staticId());
    }

    public static DPoint GetDpByCellId(String cellId) {
        if (!GateXmlConfig.supportCellDw) {
            return null;
        }
        CellIdManPrx prx = GetServerPrx();
        if (null != prx) {
            return prx.GetDpCellid(cellId);
        } else {
            return null;
        }
    }

    public static void CollCellInfo(CellidInfo[] array) {
        if (!GateXmlConfig.supportCellDw) {
            return;
        }
        CellIdManPrx prx = GetServerPrx();
        if (null != prx) {
            prx.collectCellID(array);
        }
    }

    private static CellIdManPrx GetServerPrx() {
        // 取一个临时变量
        if (cellManageServerPrx == null) {
            InitPrx();
        }
        return cellManageServerPrx;
    }

    private static synchronized boolean InitPrx() {

        try {
            cellManageServerPrx = CellIdManPrxHelper.checkedCast(communicator.propertyToProxy("CellidServer"));
            return true;
        } catch (Exception e) {
            logger.error("InitPrx", e);
            return false;
        }
    }

    /**
     * 测试功能
     *
     * @param args
     */
    public static void main(String[] args) {
        DPoint dp = CellIdManageImpl.GetDpByCellId("460.0.0.11");
        System.out.println(dp.x + "," + dp.y);
    }
}
