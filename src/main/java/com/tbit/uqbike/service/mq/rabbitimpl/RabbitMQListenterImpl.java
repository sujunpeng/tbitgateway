package com.tbit.uqbike.service.mq.rabbitimpl;

import com.rabbitmq.client.ConnectionFactory;
import com.tbit.uqbike.protocol.ABaseHandleObj;
import com.tbit.uqbike.protocol.IMqAnalyze;
import com.tbit.uqbike.tergateway.event.ITerEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by MyWin on 2017/5/5.
 */
public class RabbitMQListenterImpl implements MessageListener {
    private static Logger logger = LoggerFactory.getLogger(RabbitMQListenterImpl.class);
    // 暂时先写死，后面配置到配置文件中
    @Autowired
    private IMqAnalyze mqAnalyze;// = new MqAnalyzer();
    @Autowired
    private ITerEvent terEvnet;// = new TerEventImpl();;

    @Override
    public void onMessage(Message message) {
        try {
            //接收远程发过来的指令
            ConnectionFactory connectionFactory = null;
            List<ABaseHandleObj> list = mqAnalyze.analyzeMqMsg(message);
            terEvnet.handleBaseHandleObjs(list);
        } catch (Exception e) {
            logger.error("onMessage", e);
        }
    }
}
