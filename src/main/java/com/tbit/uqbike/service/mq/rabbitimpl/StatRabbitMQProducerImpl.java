package com.tbit.uqbike.service.mq.rabbitimpl;

import com.tbit.uqbike.service.mq.MQProducer;
import com.tbit.uqbike.util.CharsetName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.UnsupportedEncodingException;

/**
 * Created by MyWin on 2017/4/26.
 */
public class StatRabbitMQProducerImpl implements MQProducer {
    @Autowired
    private AmqpTemplate statAmqpTemplate;

    public StatRabbitMQProducerImpl(AmqpTemplate amqpTemplate) {
        this.statAmqpTemplate = amqpTemplate;
    }

    public StatRabbitMQProducerImpl() {
    }

    private static Logger logger = LoggerFactory.getLogger(StatRabbitMQProducerImpl.class);

    @Override
    public void sendDataToQueue(String queueKey, Message msg) {
        try {
            statAmqpTemplate.send(queueKey, msg);
        } catch (Exception e) {
            logger.error("sendDataToQueue", e);
        }
    }

    @Override
    public void sendDataToQueue(String queueKey, String str) {
        byte[] data;
        try {
            data = str.getBytes(CharsetName.UTF_8);
        } catch (UnsupportedEncodingException e) {
            logger.error("sendDataToQueue", e);
            return;
        }
        sendDataToQueue(queueKey, data);
    }

    @Override
    public void sendDataToQueue(String queueKey, byte[] data) {
        Message msg = new Message(data, new org.springframework.amqp.core.MessageProperties());
        sendDataToQueue(queueKey, msg);
    }
}
