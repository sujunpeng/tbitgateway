package com.tbit.uqbike.service.mq;

import io.netty.buffer.ByteBuf;
import org.springframework.amqp.core.Message;

/**
 * Created by MyWin on 2017/4/26.
 */
public interface MQProducer {
    /**
     * 发送消息到指定队列
     *
     * @param queueKey
     * @param msg
     */
    public void sendDataToQueue(String queueKey, Message msg);

    /**
     * @param queueKey
     * @param str
     */
    public void sendDataToQueue(String queueKey, String str);

    /**
     * @param queueKey
     * @param data
     */
    public void sendDataToQueue(String queueKey, byte[] data);

}
