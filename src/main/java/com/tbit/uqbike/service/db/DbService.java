package com.tbit.uqbike.service.db;

import com.tbit.uqbike.tergateway.pojo.*;

import java.util.Date;
import java.util.List;

/**
 * Created by MyWin on 2017/5/4.
 */
public interface DbService {
    /**
     * @param mno
     * @return
     */
    TerInfo getTerInfoByMno(String mno);

    /**
     * @param terInfo
     */
    void insertTerInfo(TerInfo terInfo);

    /**
     * 更新设备信息
     *
     * @param terInfo
     */
    void updateTerInfoByLogin(TerInfo terInfo);

    /**
     * @param list
     */
    void batUpdateTerInfos(List<TerInfo> list);

    /**
     * 获取位置
     *
     * @param mno
     * @return
     */
    TerPos selectLastPosInfo(String mno);

    void batUpdateTerLastPos(List<TerPos> list);

    void batUpdateTerBatter(List<TerBattery> list);

    /**
     * 插入最后位置
     *
     * @param terPos
     */
    void insertLastPosInfo(TerPos terPos);

    /**
     * 插入过滤数据库
     * @param pos
     */
    void insertFilterDb(TerPos pos);

    void updateTerAttr(TerAttrEx item);

    /**
     * 更新最后位置
     *
     * @param terPos
     */
    int updateLastPosInfo(TerPos terPos);

    /**
     * 批量插入历史轨迹
     *
     * @param terPosList
     */
    void batInsertHisPos(List<TerPos> terPosList);

    /**
     * 批量插入历史轨迹
     *
     * @param terAlarmList
     */
    void batInsertHisAlarm(List<TerAlarm> terAlarmList);

    /**
     * @param mno
     * @return
     */
    TerBattery selectLastTerBattery(String mno);

    /**
     * @param terBattery
     */
    void insertLastTerBattery(TerBattery terBattery);

    /**
     * @param terBattery
     */
    int updateLastTerBattery(TerBattery terBattery);

    /**
     * 批量插入历史轨迹
     *
     * @param terMsgList
     */
    void batInsertTerMsg(List<TerMsg> terMsgList);

    /**
     * @param tersoftwareKey
     * @return
     */
    byte[] getGjData(TersoftwareKey tersoftwareKey);

    TerConfig selectByMnoType(String machineno, int configType);

    int deleteByMnoType(String machineno, int configType);

    int insertTerUpdate(TerUpdate record);

    void updateOfflineOrderDownDt(String serNo, Date dt);

    void updateOfflineOrderRspResult(String serNo, Date dt, Integer ret, String kv);

    /**
     * 批量更新设备状态
     *
     * @param onlines
     */
    void batUpdateTerOnlines(List<TerOnline> onlines);

    void batUpdateTerOnlines_Dt(List<TerOnline> onlines);

    List<TerOnline> checkTimeOutOfflineTer();

    /**
     * 批量插入历史在线数据
     *
     * @param onlines
     */
    void batInsertHisTerOnlines(List<TerOnlineHis> onlines);
}
