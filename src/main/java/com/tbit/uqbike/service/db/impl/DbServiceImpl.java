package com.tbit.uqbike.service.db.impl;

import com.tbit.uqbike.service.db.DbService;
import com.tbit.uqbike.tergateway.config.GateXmlConfig;
import com.tbit.uqbike.tergateway.config.TerGatewayConfig;
import com.tbit.uqbike.tergateway.dao.terhis.HisTerOnlineMapper;
import com.tbit.uqbike.tergateway.dao.terhis.TerUpdateMapper;
import com.tbit.uqbike.tergateway.dao.terinfo.*;
import com.tbit.uqbike.tergateway.dao.terhis.TerAlarmDao;
import com.tbit.uqbike.tergateway.dao.termsg.TerMsgDao;
import com.tbit.uqbike.tergateway.dao.terhis.TerPosDao;
import com.tbit.uqbike.tergateway.pojo.*;
import com.tbit.uqbike.util.CollExUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by MyWin on 2017/5/4.
 */
public class DbServiceImpl implements DbService {
    private static Logger logger = LoggerFactory.getLogger(DbServiceImpl.class);
    @Autowired
    private TerMsgDao terMsgDao;
    @Autowired
    private TerInfoDao terInfoDao;
    @Autowired
    private TerLastPosDao terLastPosDao;
    @Autowired
    private TerPosDao terPosDao;
    @Autowired
    private TerLastBatteryDao terLastBatteryDao;
    @Autowired
    private TerAlarmDao terAlarmDao;
    @Autowired
    private TersoftwareMapper tersoftwareMapper;
    @Autowired
    private TerConfigMapper terConfigMapper;
    @Autowired
    private TerUpdateMapper terUpdateMapper;
    @Autowired
    private TerOfflineOrderMapper terOfflineOrderMapper;
    @Autowired
    private TerOnlineMapper terOnlineMapper;
    @Autowired
    private HisTerOnlineMapper hisTerOnlineMapper;
    @Autowired
    private TerAttrExMapper terAttrExMapper;

    @Override
    public TerInfo getTerInfoByMno(String mno) {
        return terInfoDao.selectInfo(mno);
    }

    @Override
    public void insertTerInfo(TerInfo terInfo) {
        terInfoDao.insertInfo(terInfo);
    }

    @Override
    public void updateTerInfoByLogin(TerInfo terInfo) {
        terInfoDao.updateTerInfoByLogin(terInfo);
    }

    @Override
    public void batUpdateTerInfos(List<TerInfo> list) {
        terInfoDao.batUpdateTerInfoByLogin(list);
    }

    @Override
    public TerPos selectLastPosInfo(String mno) {
        return terLastPosDao.selectLastPosInfo(mno);
    }

    @Override
    public void batUpdateTerLastPos(List<TerPos> list) {
        terLastPosDao.batUpdateLastTerPos(list);
    }

    @Override
    public void batUpdateTerBatter(List<TerBattery> list) {
        terLastBatteryDao.batUpdateLastTerBattery(list);
    }

    @Override
    public void insertLastPosInfo(TerPos terPos) {
        terLastPosDao.insertLastPosInfo(terPos);
    }

    @Override
    public void insertFilterDb(TerPos pos) {
        terPosDao.insertFilter(pos);
    }

    @Override
    public void updateTerAttr(TerAttrEx item) {
        terAttrExMapper.replaceItem(item);
    }

    @Override
    public int updateLastPosInfo(TerPos terPos) {
        return terLastPosDao.updateLastPosInfo(terPos);
    }

    @Override
    public void batInsertHisPos(List<TerPos> terPosList) {
        terPosDao.batInsertHisPos(terPosList);
    }

    @Override
    public void batInsertHisAlarm(List<TerAlarm> terAlarmList) {
        terAlarmDao.batInsertHisAlarm(terAlarmList);
    }

    @Override
    public TerBattery selectLastTerBattery(String mno) {
        return terLastBatteryDao.selectLastTerBattery(mno);
    }

    @Override
    public void insertLastTerBattery(TerBattery terBattery) {
        terLastBatteryDao.insertLastTerBattery(terBattery);
    }

    @Override
    public int updateLastTerBattery(TerBattery terBattery) {
        return terLastBatteryDao.updateLastTerBattery(terBattery);
    }

    @Override
    public void batInsertTerMsg(List<TerMsg> terMsgList) {
        terMsgDao.batInsertHisPos(terMsgList);
    }

    @Override
    public byte[] getGjData(TersoftwareKey tersoftwareKey) {
        Tersoftware tersoftware = tersoftwareMapper.selectByPrimaryKey(tersoftwareKey);
        if (null != tersoftware) {
            return tersoftware.getPkgdata();
        } else {
            return null;
        }
    }

    @Override
    public TerConfig selectByMnoType(String machineno, int configType) {
        return terConfigMapper.selectByMnoType(machineno, configType);
    }

    @Override
    public int deleteByMnoType(String machineno, int configType) {
        return terConfigMapper.deleteByMnoType(machineno, configType);
    }

    @Override
    public int insertTerUpdate(TerUpdate record) {
        return terUpdateMapper.insert(record);
    }

    @Override
    public void updateOfflineOrderDownDt(String serNo, Date dt) {
        TerOfflineOrder terOfflineOrder = new TerOfflineOrder();
        terOfflineOrder.setSerstr(serNo);
        terOfflineOrder.setDowndt(dt);
        terOfflineOrderMapper.updateByPrimaryKeySelective(terOfflineOrder);
    }

    @Override
    public void updateOfflineOrderRspResult(String serNo, Date dt, Integer ret, String kv) {
        TerOfflineOrder terOfflineOrder = new TerOfflineOrder();
        terOfflineOrder.setSerstr(serNo);
        terOfflineOrder.setTerrspdt(dt);
        terOfflineOrder.setTerret(ret);
        terOfflineOrder.setTerdata(kv);
        terOfflineOrderMapper.updateByPrimaryKeySelective(terOfflineOrder);
    }

    @Override
    public void batUpdateTerOnlines(List<TerOnline> onlines) {
        if (!GateXmlConfig.onlineFlag) {
            return;
        }
        terOnlineMapper.batUpdateTerOnlines(onlines);
    }

    @Override
    public void batUpdateTerOnlines_Dt(List<TerOnline> onlines) {
        if (!GateXmlConfig.onlineFlag) {
            return;
        }
        terOnlineMapper.batUpdateTerOnlineTime(onlines);
    }

    @Override
    @Transactional
    public List<TerOnline> checkTimeOutOfflineTer() {
        List<TerOnline> items = terOnlineMapper.selectTimeOutItems();
        if (!items.isEmpty()) {
            for (TerOnline item : items) {
                item.setOnline((byte) 0);
            }
            List<List<TerOnline>> listItems = CollExUtils.spiltList(items, TerGatewayConfig.iDbBatSize);
            for (List<TerOnline> list : listItems) {
                terOnlineMapper.batUpdateTerOnlineStatus(list);
            }
        }
        return items;
    }

    @Override
    public void batInsertHisTerOnlines(List<TerOnlineHis> onlines) {
        if (!GateXmlConfig.onlineHis) {
            return;
        }
        hisTerOnlineMapper.batInsertHisTerOnline(onlines);
    }
}
