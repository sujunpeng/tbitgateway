package com.tbit.uqbike.service.redis;


import com.tbit.uqbike.util.Pair;
import redis.clients.util.JedisByteHashMap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * redis service
 *
 * @author Leon
 * 2017年2月24日 下午4:09:17
 */
public interface RedisService {

    String getRunCnt();

    /**
     * 添加
     *
     * @param key
     * @param value
     */
    void set(String key, String value);

    /**
     * 添加
     *
     * @param hashId
     * @param key
     * @param value
     */
    void set(String hashId, String key, String value);

    /**
     * 移除过期时间
     *
     * @param key key
     */
    void persist(String key);

    void Key_Del(String key);

    /**
     * 获取
     *
     * @param key
     * @return
     */
    String get(String key);

    /**
     * 获取
     *
     * @param hashId
     * @param key
     * @return
     */
    String get(String hashId, String key);

    /**
     * 获取所有
     *
     * @param hashId
     * @return
     */
    Map<String, String> getAll(String hashId);

    List<Object> Hash_BatGetAll(final List<String> keys);

    String getFirstItem(String key);

    String remoteFirstItem(String key);

    List<String> LRange(String key, int start, int end);

    Long LDelValue(String key, long count, Object obj);

    List<Object> BatSetHashValue(final HashMap<String, String> dic, final String proName);

    List<String> BatGetHashValue(final List<String> keys, final String proName);

    List<String> BatGetValue(final List<String> keys);

    List<Long> BatGetListLen(final List<String> keys);

    List<Object> BatSetHashValue(final HashMap<String, List<Map.Entry<String, String>>> dic);

    List<Object> Hash_BatSetValue(final List<Pair<String, List<Pair<String, String>>>> data);

    List<Object> Set_BatSetKey(final String setKey, final List<String> items);

    HashMap<String, String> Hash_FormatResult(JedisByteHashMap byteHashMap);

    Set<String> Set_GetAllItem(String setKey);
}