package com.tbit.uqbike.tergateway.codedifine;

import java.util.HashMap;

/**
 * Created by MyWin on 2018/3/13 0013.
 */
public class AlarmExCode {
    // 电池锁故障
    public static Integer BATTERY_LOCK_ERROR = 1;
    // 蓝牙设备故障
    public static Integer BLE_ERROR = 2;
    // ECU通讯故障
    public static Integer ECU_ERROR = 3;
    // 一线制故障
    public static Integer YXZ_ERROR = 4;
    // 后轮锁故障
    public static Integer REAR_LOCK_ERROR = 5;
    // BMS故障
    public static Integer BMS_ERROR = 6;
    // 语音喇叭故障
    public static Integer SPEARKER_ERROR = 7;
    // 电机故障
    public static Integer MOTOR_ERROR = 8;
    // 欠压保护
    public static Integer NO_POWER_ERROR = 9;
    // 转把故障
    public static Integer ZHUANBA_ERROR = 10;
    // 防飞车保护
    public static Integer ANTI_RUN_ERROR = 11;
    // 刹车故障
    public static Integer BRAKE_ERROR = 12;
    // 电机运行中
    public static Integer MOTOR_RUN_ERROR = 13;
    // 堵转保护
    public static Integer STALL_ERROR = 14;

    private static HashMap<Integer, String> exAlarmCodeMap = new HashMap<>();

    static {
        exAlarmCodeMap.put(BATTERY_LOCK_ERROR, "电池锁故障");
        exAlarmCodeMap.put(BLE_ERROR, "蓝牙设备故障");
        exAlarmCodeMap.put(ECU_ERROR, "ECU通讯故障");
        exAlarmCodeMap.put(YXZ_ERROR, "一线制故障");
        exAlarmCodeMap.put(REAR_LOCK_ERROR, "后轮锁故障");
        exAlarmCodeMap.put(BMS_ERROR, "BMS故障");
        exAlarmCodeMap.put(SPEARKER_ERROR, "语音喇叭故障");
        exAlarmCodeMap.put(MOTOR_ERROR, "电机故障");
        exAlarmCodeMap.put(NO_POWER_ERROR, "欠压保护");
        exAlarmCodeMap.put(ZHUANBA_ERROR, "转把故障");
        exAlarmCodeMap.put(ANTI_RUN_ERROR, "防飞车保护");
        exAlarmCodeMap.put(BRAKE_ERROR, "刹车故障");
        exAlarmCodeMap.put(MOTOR_RUN_ERROR, "电机运行中");
        exAlarmCodeMap.put(STALL_ERROR, "堵转保护");
    }

    public static String getExAlarmString(Integer code) {
        if (null == code) {
            return "null";
        } else if (exAlarmCodeMap.containsKey(code)) {
            return exAlarmCodeMap.get(code);
        } else {
            return code.toString();
        }
    }
}
