package com.tbit.uqbike.tergateway.turnpkg;

import com.tbit.uqbike.protocol.ATerPkg;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import io.netty.buffer.ByteBuf;

/**
 * Created by MyWin on 2018/6/26 0026.
 */
public class TurnPkgInfo extends ATerPkg {
    public short cmd;
    public short reserve;
    public String turnConnId;
    public String terConnId;
    public byte[] data;

    @Override
    public void doBusiness() {

    }

    @Override
    public boolean autoRsp() {
        return false;
    }

    @Override
    public ByteBuf getRsp(AConnInfo conn, AProtocol protocol) {
        return null;
    }
}
