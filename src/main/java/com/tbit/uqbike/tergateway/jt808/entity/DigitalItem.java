package com.tbit.uqbike.tergateway.jt808.entity;

import com.tbit.uqbike.tergateway.pojo.TerPoint;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019-11-07 16:59
 */
public class DigitalItem extends BaseItem {
    public long value;

    @Override
    public void init(byte[] bs, int index) {
        initBase(bs, index);
        index += 4;
    }

    @Override
    public TerPoint toTp(String mno) {
        TerPoint tp = new TerPoint();
        initTerPoint(tp);
        tp.setTerIdent(mno);
        tp.setDataValue(Long.toString(value));
        return tp;
    }
}
