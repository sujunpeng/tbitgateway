package com.tbit.uqbike.tergateway.jt808.entity;

import com.tbit.uqbike.tergateway.pojo.TerPoint;
import com.tbit.uqbike.util.ByteUtil;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019-11-07 17:16
 */
public class HexItem extends BaseItem {
    public String value;

    @Override
    public void init(byte[] bs, int index) {
        initBase(bs, index);
        index += 4;
        value = ByteUtil.BytesToHexString(bs, index, len);
    }

    @Override
    public TerPoint toTp(String mno) {
        TerPoint tp = new TerPoint();
        initTerPoint(tp);
        tp.setTerIdent(mno);
        tp.setDataValue(value);
        return tp;
    }
}
