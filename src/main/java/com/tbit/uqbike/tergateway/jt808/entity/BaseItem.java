package com.tbit.uqbike.tergateway.jt808.entity;

import com.tbit.uqbike.tergateway.pojo.TerPoint;
import com.tbit.uqbike.util.ByteUtil;

import java.util.Date;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019-11-07 16:59
 */
public abstract class BaseItem {
    public int type;
    public int len;
    public Date dt;

    protected void initBase(byte[] bs, int index) {
        this.type = ByteUtil.getUnsignedShort(bs, index);
        index += 2;
        this.len = ByteUtil.getUnsignedShort(bs, index);
        index += 2;
        this.dt = new Date();
    }

    protected void initTerPoint(TerPoint tp) {
        tp.setDataType(type);
        tp.setDt(dt);
    }

    public abstract void init(byte[] bs, int index);

    public abstract TerPoint toTp(String mno);
}
