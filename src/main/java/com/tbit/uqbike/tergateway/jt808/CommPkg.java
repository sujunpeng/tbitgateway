package com.tbit.uqbike.tergateway.jt808;

import com.tbit.uqbike.protocol.ATerPkg;
import com.tbit.uqbike.util.ByteUtil;
import com.tbit.uqbike.util.StringUtil;
import com.tbit.utils.BitUtil;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019-10-30 13:33
 */
public abstract class CommPkg extends ATerPkg {

    public static class PackageInfo {
        // 总包数
        public Integer total;
        // 当前包从1开始
        public Integer pkgNum;
    }

    public Integer msgId;

    public Integer msgFlag;

    // 扩展设备编号
    public boolean isLongPhoneFlag() {
        return BitUtil.check(msgFlag, 14);
    }

    public void setLongPhoneFlag(boolean longPhoneFlag) {
        msgFlag = BitUtil.set(msgFlag, 14, longPhoneFlag);
    }

    // 是否分包
    public boolean isSpiltPkgFlag() {
        return BitUtil.check(msgFlag, 13);
    }

    public void setSpiltPkgFlag(boolean spiltPkgFlag) {
        msgFlag = BitUtil.set(msgFlag, 13, spiltPkgFlag);
    }

    // 加密方式
    public Integer getEncodeType() {
        return BitUtil.readBit(msgFlag, 10, 3);
    }

    public void setEncodeType(Integer encodeType) {
        msgFlag = BitUtil.writeBit(msgFlag, 10, 3, encodeType);
    }

    // 消息体长度
    public Integer getBodyLen() {
        return BitUtil.readBit(msgFlag, 0, 10);
    }

    public void setBodyLen(Integer bodyLen) {
        msgFlag = BitUtil.writeBit(msgFlag, 0, 10, bodyLen);
    }

    public static int getFlag(boolean longtid, boolean spiltPkg, int enType, int bodyLen) {
        int i = 0;
        i = BitUtil.set(i, 14, longtid);
        i = BitUtil.set(i, 13, spiltPkg);
        i = BitUtil.writeBit(i, 10, 3, enType);
        i = BitUtil.writeBit(i, 0, 10, bodyLen);
        return i;
    }

    public static int reSetLen(int i, int len) {
        return BitUtil.writeBit(i, 0, 10, len);
    }

    public byte[] phone;
    public Integer serNo;
    public PackageInfo packageInfo;

    public int getHeadLen() {
        return 2 + 2 + (isLongPhoneFlag() ? 9 : 6) + 2 + (isSpiltPkgFlag() ? 4 : 0);
    }

    public byte checkByte;

    /**
     * 初始化好头部信息
     *
     * @param bs
     * @param len
     */
    public void initHead(byte[] bs, int index, int len) {
        msgId = ByteUtil.getUnsignedShort(bs, index);
        index += 2;
        msgFlag = ByteUtil.getUnsignedShort(bs, index);
        index += 2;

        int mnoLen;
        if (isLongPhoneFlag()) {
            mnoLen = 9;
            mno = ByteUtil.BytesToHexString(bs, index, mnoLen);
            mno = StringUtil.TrimEnd(mno, 'F');
        } else {
            mnoLen = 6;
            mno = ByteUtil.BytesToHexString(bs, index, mnoLen);
            mno = StringUtil.TrimEnd(mno, 'F');
        }
        phone = new byte[mnoLen];
        System.arraycopy(bs, index, phone, 0, phone.length);
        index += phone.length;

        serNo = ByteUtil.getUnsignedShort(bs, index);
        index += 2;
        if (isSpiltPkgFlag()) {
            packageInfo = new PackageInfo();
            packageInfo.total = ByteUtil.getUnsignedShort(bs, index);
            index += 2;
            packageInfo.pkgNum = ByteUtil.getUnsignedShort(bs, index);
            index += 2;
        }
    }
}
