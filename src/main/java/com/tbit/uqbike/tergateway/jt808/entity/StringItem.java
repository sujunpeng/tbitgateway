package com.tbit.uqbike.tergateway.jt808.entity;

import com.tbit.uqbike.tergateway.pojo.TerPoint;
import com.tbit.uqbike.util.CharsetName;

import java.io.UnsupportedEncodingException;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019-11-07 16:59
 */
public class StringItem extends BaseItem {
    public String value;

    @Override
    public void init(byte[] bs, int index) {
        initBase(bs, index);
        index += 4;
        try {
            value = new String(bs, index, len, CharsetName.UTF_8);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public TerPoint toTp(String mno) {
        TerPoint tp = new TerPoint();
        initTerPoint(tp);
        tp.setTerIdent(mno);
        tp.setDataValue(value);
        return tp;
    }
}
