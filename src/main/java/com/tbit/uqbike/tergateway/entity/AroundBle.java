package com.tbit.uqbike.tergateway.entity;

import com.tbit.uqbike.tergateway.wa206pkg.extend.BleInfo;

import java.util.LinkedList;
import java.util.List;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019-10-12 16:16
 * 周边蓝牙dto
 */
public class AroundBle {
    static class BleNode {
        public String bleMAC;
        public String bleTID;
        public int bleLevel = -1;
        // 扫描到的设备类型：0 ：单车设备信息   1：道钉
        public int bleType = -1;
        // 硬件版本号
        public int hdVer = -1;
        // 软件版本号
        public int softVer = -1;
        /**
         * 道钉状态
         * Bit 5-bit7 发送功率：0到7等级
         * Bit 4 0：系统无故障
         * Bit 3 0：LED灯闪烁处于关闭状态
         * Bit 2 0：电池未处于充电状态
         * Bit0~Bit1 用于标识外接电源电量等级
         * <p>
         * 单车状态
         * bit6 故障状态。0：车辆为正常状态；1：车辆为故障状态
         * bit5 GPS定位状态。0：GPS为未定位状态；1：GPS为已定位状态
         * bit4 网络连接状态。0：网络为中断状态；1：网路为连接状态
         * bit3 电池锁状态。0：电池锁为打开状态；1：电池锁为加锁状态
         * bit2 电池状态。0：电池为断电状态；1：电池为供电状态
         * bit1 运动状态。0：车辆为静止状态；1：车辆为运动状态
         * bit0 借还车状态。0：车辆为还车状态；1：车辆为借车状态
         */
        public int bleStatus = -1;
    }

    // 周边蓝牙设备
    public List<BleNode> bleNodes = new LinkedList<>();
    // 自身设备信息
    public String mno;

    /**
     * @param bleInfo
     */
    public void addBleNode(BleInfo bleInfo) {
        BleNode node = new BleNode();
        node.bleMAC = bleInfo.bleMAC;
        node.bleTID = bleInfo.bleTID;
        node.bleLevel = bleInfo.bleLevel;
        node.bleType = bleInfo.bleType;
        node.hdVer = bleInfo.hdVer;
        node.softVer = bleInfo.softVer;
        node.bleStatus = bleInfo.bleStatus;
        bleNodes.add(node);
    }
}
