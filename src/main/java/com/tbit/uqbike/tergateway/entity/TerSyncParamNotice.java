package com.tbit.uqbike.tergateway.entity;

import com.alibaba.fastjson.JSON;
import com.tbit.uqbike.protocol.ABaseHandleObj;
import com.tbit.uqbike.protocol.AnalyzeImpl.AAutoProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.WA206DeEnCoder;
import com.tbit.uqbike.tergateway.data.TerGatewayData;
import com.tbit.uqbike.tergateway.log.LOG;
import com.tbit.uqbike.util.ErrorCode;
import io.netty.buffer.ByteBuf;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019-6-11 20:03
 */
public class TerSyncParamNotice extends ABaseHandleObj {

    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TerSyncParamNotice.class);

    private String mno;
    private Integer syncTime;
    public String syncParams;

    public String getMno() {
        return mno;
    }

    public void setMno(String mno) {
        this.mno = mno;
    }

    public Integer getSyncTime() {
        return syncTime;
    }

    public void setSyncTime(Integer syncTime) {
        this.syncTime = syncTime;
    }

    public String getSyncParams() {
        return syncParams;
    }

    public void setSyncParams(String syncParams) {
        this.syncParams = syncParams;
    }

    @Override
    public void doBusiness() {
        int code = ErrorCode.ErrorTerOffline;
        try {
            TerTempData terTempData = TerGatewayData.tryGetTerTempDataByMno(this.mno);
            if (null != terTempData) {
                AConnInfo connInfo = terTempData.getConnInfo();
                if (null != connInfo) {
                    AProtocol protocol = AAutoProtocol.getProtocol(terTempData.protocolName);
                    if (null != protocol) {
                        if (protocol instanceof WA206DeEnCoder) {
                            // 再尝试构造下行消息
                            final ByteBuf byteBuf = ((WA206DeEnCoder) protocol).builtSyncParamRsp(connInfo, 0, syncTime, syncParams);
                            // 最后发送下行数据
                            if (byteBuf != null && byteBuf.readableBytes() > 0) {
                                connInfo.downMsg(byteBuf);
                                code = ErrorCode.OK;
                            } else {
                                code = ErrorCode.ErrorDownBufEmpty;
                            }
                        } else {
                            code = ErrorCode.ErrorNotSupport;
                        }
                    } else {
                        code = ErrorCode.ErrorCtxNotExist;
                    }
                } else {
                    if (LOG.bConnMnoLog) {
                        LOG.NET.info(String.format("get null conn:%s", terTempData.getConnId()));
                    }
                    code = ErrorCode.ErrorConnNotExist;
                }
            }
        } catch (Exception e) {
            logger.error("doBusiness", e);
        } finally {
            logger.info(String.format("Sync Info:%scode:%d",
                    JSON.toJSONString(this), code));
        }
    }
}
