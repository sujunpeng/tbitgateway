package com.tbit.uqbike.tergateway.entity;

import java.util.Date;

/**
 * Created by MyWin on 2017/5/24.
 */
public class TerLastStatus {
    public String mno;
    public Date statusDt;
    public int carStatus;
    public int signStatus;

    public void cloneObj(TerLastStatus status) {
        this.mno = status.mno;
        this.statusDt = status.statusDt;
        this.carStatus = status.carStatus;
        this.signStatus = status.signStatus;
    }
}
