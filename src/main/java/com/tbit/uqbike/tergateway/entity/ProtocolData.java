package com.tbit.uqbike.tergateway.entity;

/**
 * Created by MyWin on 2017/12/27 0027.
 */
public class ProtocolData {
    private int version;
    private int reserve;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getReserve() {
        return reserve;
    }

    public void setReserve(int reserve) {
        this.reserve = reserve;
    }

    public ProtocolData(int version, int reserve) {
        this.version = version;
        this.reserve = reserve;
    }

    public boolean compare(int version, int reserve) {
        return version == this.version && reserve == this.reserve;
    }
}
