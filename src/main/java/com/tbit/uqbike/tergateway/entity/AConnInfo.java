package com.tbit.uqbike.tergateway.entity;

/**
 * Created by MyWin on 2018/6/26 0026.
 */
public abstract class AConnInfo implements IConnInfo {
    public String mno;
    public String proName;//协议名称
    public String connId;//连接ID
    public byte version;
    public int reserve;
}
