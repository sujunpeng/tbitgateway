package com.tbit.uqbike.tergateway.entity;

import java.util.Date;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019-09-24 9:36
 */
public class TerLastVol {
    public String mno;
    public Date dt;
    public int vol;
}
