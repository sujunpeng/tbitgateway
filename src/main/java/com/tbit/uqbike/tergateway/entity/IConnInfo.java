package com.tbit.uqbike.tergateway.entity;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

/**
 * Created by MyWin on 2018/6/26 0026.
 */
public interface IConnInfo {
    ChannelHandlerContext getCtx();

    boolean downMsg(ByteBuf byteBuf);

    void closeConn();
}
