package com.tbit.uqbike.tergateway.entity;

import com.tbit.uqbike.tergateway.pojo.PlatformPoint;

import java.util.List;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019-11-08 10:47
 */
public class PlatformPointData {
    public String ident;
    public List<PlatformPoint> datas;
}
