package com.tbit.uqbike.tergateway.entity;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2020-03-03 16:35
 * 信号情况，包含gsm信号强度，吸附基站号，gps信号强度
 */
public class SignalInfo {
    // 0-9
    public Integer gps;
    // 0-9
    public Integer gsm;
    //
    public String cellId;
}
