package com.tbit.uqbike.tergateway.entity;

import java.util.Date;

/**
 * Created by MyWin on 2018/7/2 0002.
 */
public class TerEventMsg {

    public String mno;

    public Date dt;

    public int eventType;

    public double lon;

    public double lat;

    public int high;

    public int dir;

    public int speed;

    public int signalStatus;


    public int mcc;

    public int mnc;

    public int lac;

    public int cellid;


    public int soc;

    public int powEU;

    public int totalMile;

    public int signMile;

    public int terStatus;
}
