package com.tbit.uqbike.tergateway.entity;

import com.tbit.uqbike.TerGatewayMain;
import com.tbit.uqbike.service.db.DbService;
import com.tbit.uqbike.tergateway.data.TerGatewayData;
import com.tbit.uqbike.tergateway.pojo.TerBattery;
import com.tbit.uqbike.util.IDelayUpdateObj;

/**
 * Created by MyWin on 2017/5/9.
 */
public class TerLastBattery implements IDelayUpdateObj {
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TerLastPos.class);
    private TerBattery terBattery;

    public TerLastBattery(TerBattery terBattery) {
        this.terBattery = terBattery;
    }

    @Override
    public void handle() {
        // 更新终端的最后位置
        DbService dbService = TerGatewayMain.getDbService();
        //logger.info(String.format("Update Ter Last Battery:%s", this.terBattery.getMachineNO()));
        int iCnt = dbService.updateLastTerBattery(this.terBattery);
        if (iCnt == 0) {
            dbService.insertLastTerBattery(this.terBattery);
        }
    }
}
