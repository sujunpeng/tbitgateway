package com.tbit.uqbike.tergateway.entity;

import com.tbit.uqbike.TerGatewayMain;
import com.tbit.uqbike.tergateway.data.TerGatewayData;
import com.tbit.uqbike.util.ConstDefine;
import com.tbit.uqbike.util.ErrorCode;
import com.tbit.uqbike.util.StringUtil;
import io.netty.channel.ChannelHandlerContext;

/**
 * Created by MyWin on 2017/5/10.
 */
public class TerControlOrder {
    public String sn;
    public String controlType;
    public String serNO;
    public String paramName;
    public String paramValue;

    public TerControlOrderDownRsp getTerControlOrderDownRsp(int code) {
        TerControlOrderDownRsp rsp = new TerControlOrderDownRsp();
        rsp.serNO = this.serNO;
        rsp.controlRet = Integer.toString(code);
        return rsp;
    }

    public TerControlOrderTerRsp getTerControlOrderTerRsp(int code, String param) {
        TerControlOrderTerRsp rsp = new TerControlOrderTerRsp();
        rsp.serNO = serNO;
        rsp.controlRet = Integer.toString(code);
        rsp.paramRet = param;
        return rsp;
    }

    public RemoteControl getRemoteControl(MqJsonMsg mqJsonMsg) {
        RemoteControl remoteControl = new RemoteControl();
        remoteControl.controlType = this.controlType;
        remoteControl.paramName = this.paramName;
        remoteControl.paramValue = this.paramValue;
        remoteControl.serNO = this.serNO;
        remoteControl.sn = this.sn;

        remoteControl.fromType = ConstDefine.SYS_IDENT_MQ;
        remoteControl.from = mqJsonMsg.feedback;
        remoteControl.msgId = mqJsonMsg.msgId;
        return remoteControl;
    }
}
