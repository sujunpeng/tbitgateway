package com.tbit.uqbike.tergateway.entity;

import com.tbit.uqbike.TerGatewayMain;
import com.tbit.uqbike.service.db.DbService;
import com.tbit.uqbike.tergateway.pojo.TerMsg;
import com.tbit.uqbike.tergateway.pojo.TerPos;
import com.tbit.uqbike.util.IBatHandle;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by MyWin on 2017/5/15.
 */
public class TerHisMsg implements IBatHandle<TerMsg> {
    public TerMsg terMsg;
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TerHisMsg.class);
    private LinkedList<IBatHandle> insertList;

    public TerHisMsg(TerMsg terMsg) {
        this.terMsg = terMsg;
    }

    @Override
    public void insertData(LinkedList<IBatHandle> insertList) {
        this.insertList = insertList;
        if (this.insertList != null && this.insertList.size() > 0) {
            Thread thread = new Thread(this);
            thread.start();
        }
    }

    @Override
    public void setData(TerMsg terMsg) {
        this.terMsg = terMsg;
    }

    @Override
    public TerMsg getData() {
        return terMsg;
    }

    @Override
    public void run() {
        if (this.insertList != null && this.insertList.size() > 0) {
            // 批量插入历史轨迹
            DbService dbService = TerGatewayMain.getDbService();
            List<TerMsg> terMsgList = new LinkedList<>();
            for (IBatHandle<TerMsg> terMsg : insertList) {
                terMsgList.add(terMsg.getData());
            }
            logger.info(String.format("Bat Inser Ter His Msg:%d", terMsgList.size()));
            dbService.batInsertTerMsg(terMsgList);
        }
    }
}
