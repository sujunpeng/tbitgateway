package com.tbit.uqbike.tergateway.entity;

import com.tbit.uqbike.tergateway.pojo.TerBattery;
import com.tbit.uqbike.tergateway.pojo.TerInfo;
import com.tbit.uqbike.tergateway.pojo.TerPos;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by MyWin on 2018/3/14 0014.
 */
public class CacheData {
    private AtomicInteger redisUpdateCnt;
    private AtomicInteger dbUpdateCnt;
    private String sn;
    /**
     * 电量
     */
    private TerBattery lastBattery;
    /**
     * 位置
     */
    private TerPos lastPos;
    /**
     * 最后报文时间
     */
    private Date lastPkgDt;
    /**
     * 最后状态
     */
    private TerLastStatus status;
    /**
     * 最后电压
     */
    private TerLastVol vol;
    /**
     * 路由key
     */
    private String routeKey;
    /**
     * 协议相关
     */
    private ProtocolData protocolData;
    /**
     * 终端信息
     */
    private TerInfo terInfo;

    public CacheData(String sn) {
        this.sn = sn;
        this.redisUpdateCnt = new AtomicInteger();
        this.dbUpdateCnt = new AtomicInteger();
    }

    public String getSn() {
        return sn;
    }

    public boolean haveRedisCacheData() {
        return this.redisUpdateCnt.get() > 0;
    }

    public boolean haveDbCacheData() {
        return this.dbUpdateCnt.get() > 0;
    }

    public TerInfo getTerInfo() {
        return terInfo;
    }

    public void setTerInfo(TerInfo terInfo) {
        this.terInfo = terInfo;
        this.dbUpdateCnt.incrementAndGet();
    }

    public TerBattery getLastBattery() {
        return lastBattery;
    }

    public void setLastBattery(TerBattery lastBattery) {
        this.lastBattery = lastBattery;
        redisUpdateCnt.incrementAndGet();
        dbUpdateCnt.incrementAndGet();
    }

    public TerPos getLastPos() {
        return lastPos;
    }

    public void setLastPos(TerPos lastPos) {
        this.lastPos = lastPos;
        redisUpdateCnt.incrementAndGet();
        dbUpdateCnt.incrementAndGet();
    }

    public Date getLastPkgDt() {
        return lastPkgDt;
    }

    public void setLastPkgDt(Date lastPkgDt) {
        this.lastPkgDt = lastPkgDt;
        redisUpdateCnt.incrementAndGet();
        dbUpdateCnt.incrementAndGet();
    }

    public TerLastStatus getStatus() {
        return status;
    }

    public void setStatus(TerLastStatus status) {
        this.status = status;
        redisUpdateCnt.incrementAndGet();
    }

    public void setVol(TerLastVol vol) {
        this.vol = vol;
        redisUpdateCnt.incrementAndGet();
    }

    public TerLastVol getVol() {
        return this.vol;
    }

    public String getRouteKey() {
        return routeKey;
    }

    public void setRouteKey(String routeKey) {
        this.routeKey = routeKey;
        redisUpdateCnt.incrementAndGet();
    }

    public ProtocolData getProtocolData() {
        return protocolData;
    }

    public void setProtocolData(ProtocolData protocolData) {
        this.protocolData = protocolData;
        redisUpdateCnt.incrementAndGet();
    }
}
