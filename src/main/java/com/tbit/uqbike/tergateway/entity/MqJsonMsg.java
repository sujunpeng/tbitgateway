package com.tbit.uqbike.tergateway.entity;

import com.alibaba.fastjson.JSONObject;
import com.tbit.uqbike.TerGatewayMain;
import com.tbit.uqbike.util.ConstDefine;
import com.tbit.uqbike.util.StringUtil;
import io.netty.buffer.ByteBuf;

/**
 * Created by MyWin on 2017/5/10.
 */
public class MqJsonMsg {
    public String data;
    public String feedback;
    public int msgId;

}
