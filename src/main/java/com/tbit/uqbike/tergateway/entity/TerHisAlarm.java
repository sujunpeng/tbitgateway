package com.tbit.uqbike.tergateway.entity;

import com.tbit.uqbike.TerGatewayMain;
import com.tbit.uqbike.service.db.DbService;
import com.tbit.uqbike.tergateway.pojo.TerAlarm;
import com.tbit.uqbike.util.IBatHandle;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by MyWin on 2017/5/9.
 */
public class TerHisAlarm implements IBatHandle<TerAlarm> {
    public TerAlarm terAlarm;
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TerHisPos.class);
    private LinkedList<IBatHandle> insertList;

    public TerHisAlarm(TerAlarm terAlarm) {
        this.terAlarm = terAlarm;
    }

    @Override
    public void insertData(LinkedList<IBatHandle> insertList) {
        this.insertList = insertList;
        if (this.insertList != null && this.insertList.size() > 0) {
            Thread thread = new Thread(this);
            thread.start();
        }
    }

    @Override
    public void setData(TerAlarm terAlarm) {
        this.terAlarm = terAlarm;
    }

    @Override
    public TerAlarm getData() {
        return this.terAlarm;
    }


    @Override
    public void run() {
        if (this.insertList != null && this.insertList.size() > 0) {
            // 批量插入历史轨迹
            DbService dbService = TerGatewayMain.getDbService();
            List<TerAlarm> terHisInfoList = new LinkedList<>();
            for (IBatHandle<TerAlarm> terHisInfo : insertList) {
                terHisInfoList.add(terHisInfo.getData());
            }
            logger.info(String.format("Bat Inser Ter His Alarm:%d", terHisInfoList.size()));
            dbService.batInsertHisAlarm(terHisInfoList);
        }
    }
}
