package com.tbit.uqbike.tergateway.entity;

import com.tbit.uqbike.TerGatewayMain;
import com.tbit.uqbike.service.db.DbService;
import com.tbit.uqbike.tergateway.pojo.TerPos;
import com.tbit.uqbike.util.IDelayUpdateObj;

/**
 * Created by MyWin on 2017/5/9.
 */
public class TerLastPos implements IDelayUpdateObj {
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TerLastPos.class);
    private TerPos terPos;

    public TerLastPos(TerPos terPos) {
        this.terPos = terPos;
    }

    @Override
    public void handle() {
        // 更新终端的最后位置
        DbService dbService = TerGatewayMain.getDbService();
        //logger.info(String.format("Update Ter Last Pos:%s", this.terPos.getMachineNO()));
        int iCnt = dbService.updateLastPosInfo(this.terPos);
        if (iCnt == 0) {
            dbService.insertLastPosInfo(this.terPos);
        }
    }
}
