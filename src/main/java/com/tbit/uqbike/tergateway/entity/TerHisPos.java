package com.tbit.uqbike.tergateway.entity;

import com.tbit.uqbike.TerGatewayMain;
import com.tbit.uqbike.service.db.DbService;
import com.tbit.uqbike.tergateway.pojo.TerPos;
import com.tbit.uqbike.util.IBatHandle;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by MyWin on 2017/5/9.
 */
public class TerHisPos implements IBatHandle<TerPos> {
    public TerPos terPos;
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TerHisPos.class);
    private LinkedList<IBatHandle> insertList;

    public TerHisPos(TerPos terPos) {
        this.terPos = terPos;
    }

    @Override
    public void insertData(LinkedList<IBatHandle> insertList) {
        this.insertList = insertList;
        if (this.insertList != null && this.insertList.size() > 0) {
            Thread thread = new Thread(this);
            thread.start();
        }
    }

    @Override
    public void setData(TerPos terPos) {
        this.terPos = terPos;
    }

    @Override
    public TerPos getData() {
        return this.terPos;
    }


    @Override
    public void run() {
        if (this.insertList != null && this.insertList.size() > 0) {
            // 批量插入历史轨迹
            DbService dbService = TerGatewayMain.getDbService();
            List<TerPos> terPosList = new LinkedList<>();
            for (IBatHandle<TerPos> terHisPos : insertList) {
                terPosList.add(terHisPos.getData());
            }
            logger.info(String.format("Bat Inser Ter His Pos:%d", terPosList.size()));
            dbService.batInsertHisPos(terPosList);
        }
    }
}
