package com.tbit.uqbike.tergateway.entity;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2018/11/22 0022 14:12
 */
public class CarExData {
    @JSONField(name = "经度")
    public Double lon;
    @JSONField(name = "纬度")
    public Double lat;

    @JSONField(name = "轮速")
    public Short speed;
    @JSONField(name = "油门")
    public Short iol;
    @JSONField(name = "刹车高位")
    public Short highBrake;
    @JSONField(name = "刹车低位")
    public Short lowBrake;

    @JSONField(name = "Z")
    public Byte sharkZ;
    @JSONField(name = "Y")
    public Byte sharkY;
    @JSONField(name = "X")
    public Byte sharkX;
}
