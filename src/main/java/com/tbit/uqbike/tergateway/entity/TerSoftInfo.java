package com.tbit.uqbike.tergateway.entity;

/**
 * Created by MyWin on 2017/5/22.
 */
public class TerSoftInfo {

    // 客户编码
    private int customercode;
    // 硬件编码
    private int hardwaremodel;
    // 固件类型
    private int firmwaretype;

    //region 终端需要的属性
    // 为0
    public int softId;
    // 版本号
    public int version;
    // 包大小
    public int binSize;
    // 完整包的crc
    public int binCRC;
    // 时间戳 0
    public int time;
    // 加密标志 0
    public int enFlag;
    // 保留数据 0
    public byte[] reserveData = new byte[47];
    //endregion
}
