package com.tbit.uqbike.tergateway.entity;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;

/**
 * Created by MyWin on 2017/5/15.
 * 基本的socket连接对象
 */
public class ConnInfo extends AConnInfo {
    private ChannelHandlerContext ctx;

    public ConnInfo(ChannelHandlerContext ctx) {
        this.ctx = ctx;
        this.connId = ctx.channel().id().toString();
    }

    @Override
    public ChannelHandlerContext getCtx() {
        return ctx;
    }

    @Override
    public boolean downMsg(ByteBuf byteBuf) {
        try {
            ChannelHandlerContext downCtx = getCtx();
            if (null != downCtx) {
                downCtx.writeAndFlush(byteBuf);
                return true;
            }
        } catch (Exception e) {

        }
        return false;
    }

    @Override
    public void closeConn() {
        try {
            ChannelHandlerContext tempCtx = getCtx();
            if (null != tempCtx) {
                tempCtx.close();
            }
        } catch (Exception e) {

        }
    }

    public String getMno() {
        return mno;
    }

    public void setMno(String mno) {
        this.mno = mno;
    }

}
