package com.tbit.uqbike.tergateway.dao.terhis;

import com.tbit.uqbike.tergateway.pojo.TerOnlineHis;

import java.util.List;

public interface HisTerOnlineMapper {

    int batInsertHisTerOnline(List<TerOnlineHis> list);
}