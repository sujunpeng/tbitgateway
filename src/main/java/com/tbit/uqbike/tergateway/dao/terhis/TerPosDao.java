package com.tbit.uqbike.tergateway.dao.terhis;


import com.tbit.uqbike.tergateway.pojo.TerPos;

import java.util.List;

/**
 * 设备dao
 *
 * @author Leon
 * 2017年2月24日 下午4:02:36
 */
public interface TerPosDao {

    void insertFilter(TerPos pos);

    /**
     * @param mno
     * @return
     */
    TerPos selectLastPosInfo(String mno);

    /**
     * @param terPos
     */
    void insertLastPosInfo(TerPos terPos);

    /**
     * @param terPosList
     */
    void batInsertHisPos(List<TerPos> terPosList);
}