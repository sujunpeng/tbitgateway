package com.tbit.uqbike.tergateway.dao.terinfo;

import com.tbit.uqbike.tergateway.pojo.TerOnline;

import java.util.List;

public interface TerOnlineMapper {

    /**
     * 在线状态批量更新
     *
     * @param list
     * @return
     */
    int batUpdateTerOnlines(List<TerOnline> list);

    int batUpdateTerOnlineStatus(List<TerOnline> list);

    int batUpdateTerOnlineTime(List<TerOnline> list);

    List<TerOnline> selectTimeOutItems();
}