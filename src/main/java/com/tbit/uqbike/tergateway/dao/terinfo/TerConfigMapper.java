package com.tbit.uqbike.tergateway.dao.terinfo;

import com.tbit.uqbike.tergateway.pojo.TerConfig;
import org.apache.ibatis.annotations.Param;

public interface TerConfigMapper {
    int insert(TerConfig record);

    int insertSelective(TerConfig record);

    TerConfig selectByMnoType(@Param("machineno") String machineno, @Param("configType") int configType);

    int deleteByMnoType(@Param("machineno") String machineno, @Param("configType") int configType);
}