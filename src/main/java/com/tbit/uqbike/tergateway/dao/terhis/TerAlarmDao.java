package com.tbit.uqbike.tergateway.dao.terhis;


import com.tbit.uqbike.tergateway.pojo.TerAlarm;

import java.util.List;

/**
 * 设备dao
 *
 * @author Leon
 * 2017年2月24日 下午4:02:36
 */
public interface TerAlarmDao {

    void batInsertHisAlarm(List<TerAlarm> terAlarmList);
}