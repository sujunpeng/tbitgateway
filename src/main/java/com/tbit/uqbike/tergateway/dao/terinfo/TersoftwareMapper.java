package com.tbit.uqbike.tergateway.dao.terinfo;

import com.tbit.uqbike.tergateway.pojo.Tersoftware;
import com.tbit.uqbike.tergateway.pojo.TersoftwareKey;

public interface TersoftwareMapper {
    int deleteByPrimaryKey(TersoftwareKey key);

    int insert(Tersoftware record);

    int insertSelective(Tersoftware record);

    Tersoftware selectByPrimaryKey(TersoftwareKey key);

    int updateByPrimaryKeySelective(Tersoftware record);

    int updateByPrimaryKeyWithBLOBs(Tersoftware record);
}