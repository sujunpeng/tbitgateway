package com.tbit.uqbike.tergateway.dao.terinfo;


import com.tbit.uqbike.tergateway.pojo.TerInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 设备dao
 *
 * @author Leon
 * 2017年2月24日 下午4:02:36
 */
public interface TerInfoDao {
    /**
     * 插入
     *
     * @param terInfo
     */
    void insertInfo(TerInfo terInfo);

    /**
     * 根据设备编号获取终端信息
     *
     * @param machineNO
     * @return
     */
    TerInfo selectInfo(String machineNO);

    /**
     * 更新
     *
     * @param terInfo
     */
    void updateTerInfoByLogin(TerInfo terInfo);

    int batUpdateTerInfoByLogin(@Param("list") List<TerInfo> list);
}