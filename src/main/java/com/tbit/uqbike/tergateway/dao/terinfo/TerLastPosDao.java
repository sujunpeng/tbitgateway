package com.tbit.uqbike.tergateway.dao.terinfo;


import com.tbit.uqbike.tergateway.pojo.TerPos;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 设备dao
 *
 * @author Leon
 * 2017年2月24日 下午4:02:36
 */
public interface TerLastPosDao {

    /**
     * @param mno
     * @return
     */
    TerPos selectLastPosInfo(String mno);

    /**
     * @param terPos
     */
    void insertLastPosInfo(TerPos terPos);

    /**
     * @param terPos
     */
    int updateLastPosInfo(TerPos terPos);

    int batUpdateLastTerPos(@Param("list") List<TerPos> list);
}