package com.tbit.uqbike.tergateway.dao.terhis;

import com.tbit.uqbike.tergateway.pojo.TerUpdate;

public interface TerUpdateMapper {
    int insert(TerUpdate record);

    int insertSelective(TerUpdate record);
}