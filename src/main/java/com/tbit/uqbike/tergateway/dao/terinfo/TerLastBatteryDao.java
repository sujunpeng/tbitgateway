package com.tbit.uqbike.tergateway.dao.terinfo;

import com.tbit.uqbike.tergateway.pojo.TerBattery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by MyWin on 2017/5/9.
 */
public interface TerLastBatteryDao {
    /**
     * @param mno
     * @return
     */
    TerBattery selectLastTerBattery(String mno);

    /**
     * @param terBattery
     */
    void insertLastTerBattery(TerBattery terBattery);

    /**
     * @param terBattery
     */
    int updateLastTerBattery(TerBattery terBattery);

    int batUpdateLastTerBattery(@Param("list") List<TerBattery> list);
}
