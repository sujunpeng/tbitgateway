package com.tbit.uqbike.tergateway.dao.terinfo;

import com.tbit.uqbike.tergateway.pojo.TerOfflineOrder;

public interface TerOfflineOrderMapper {
    int deleteByPrimaryKey(String serstr);

    int insert(TerOfflineOrder record);

    int insertSelective(TerOfflineOrder record);

    TerOfflineOrder selectByPrimaryKey(String serstr);

    int updateByPrimaryKeySelective(TerOfflineOrder record);

    int updateByPrimaryKey(TerOfflineOrder record);
}