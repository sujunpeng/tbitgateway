package com.tbit.uqbike.tergateway.dao.terinfo;


import com.tbit.uqbike.tergateway.pojo.TerAttrEx;
import com.tbit.uqbike.tergateway.pojo.TerAttrExKey;

public interface TerAttrExMapper {
    int deleteByPrimaryKey(TerAttrExKey key);

    int insert(TerAttrEx record);

    int replaceItem(TerAttrEx record);

    int insertSelective(TerAttrEx record);

    TerAttrEx selectByPrimaryKey(TerAttrExKey key);

    int updateByPrimaryKeySelective(TerAttrEx record);

    int updateByPrimaryKey(TerAttrEx record);
}