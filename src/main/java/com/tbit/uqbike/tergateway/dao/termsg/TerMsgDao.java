package com.tbit.uqbike.tergateway.dao.termsg;

import com.tbit.uqbike.tergateway.pojo.TerMsg;
import org.apache.ibatis.annotations.Delete;

import java.util.List;

/**
 * 登录dao
 *
 * @author Leon
 * 2017年2月24日 下午4:02:36
 */
public interface TerMsgDao {

    /**
     * @param terMsgList
     */
    void batInsertHisPos(List<TerMsg> terMsgList);
}