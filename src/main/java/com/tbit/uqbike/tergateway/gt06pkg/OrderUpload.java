package com.tbit.uqbike.tergateway.gt06pkg;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.tergateway.data.TerGatewayData;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import com.tbit.uqbike.tergateway.entity.RemoteControl;
import com.tbit.uqbike.tergateway.entity.TerTempData;
import com.tbit.uqbike.util.ConstDefine;
import io.netty.buffer.ByteBuf;

import java.util.Objects;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019-6-27 16:18
 */
public class OrderUpload extends CommPkg {

    private final String SUCC_STR = "Success!";

    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(OrderUpload.class);
    @JSONField(name = "指令长度")
    public int orderLen;
    @JSONField(name = "指令流水号")
    public int serNo;
    @JSONField(name = "指令内容")
    public String orderContent;
    @JSONField(name = "内容")
    public int lang;

    @Override
    public void doBusiness() {
        logger.info(JSON.toJSONString(this));

        TerTempData terTempData = null;
        if (null != this.mno && !this.mno.isEmpty()) {
            terTempData = TerGatewayData.getTerTempDataByMno(this.mno);
        }
        Object obj = null;
        // 先根据流水号来
        if (null != terTempData) {
            obj = terTempData.getSerNoAndDel(serNo);
        }
        if (null != obj) {
            if (obj instanceof RemoteControl) {
                RemoteControl remoteControl = (RemoteControl) obj;
                if (Objects.equals(ConstDefine.CONTROL_TYPE_GET, remoteControl.controlType) ||
                        Objects.equals(ConstDefine.CONTROL_TYPE_SET, remoteControl.controlType)) {
                    remoteControl.buildTerKvRsp(orderContent);
                } else if (Objects.equals(ConstDefine.CONTROL_TYPE_CONTROL, remoteControl.controlType)) {
                    if (orderContent.contains(SUCC_STR)) {
                        remoteControl.buildTerRetRsp(1);
                    } else {
                        remoteControl.buildTerKvRsp(orderContent);
                    }
                }

            }
        }
    }

    @Override
    public boolean autoRsp() {
        return false;
    }

    @Override
    public ByteBuf getRsp(AConnInfo conn, AProtocol protocol) {
        return null;
    }
}
