package com.tbit.uqbike.tergateway.gt06pkg;

import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.Gt06DeEnCoder;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import io.netty.buffer.ByteBuf;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019-6-27 16:24
 */
public class MediaInfo extends CommPkg {


    @Override
    public void doBusiness() {

    }

    @Override
    public boolean autoRsp() {
        return true;
    }

    @Override
    public ByteBuf getRsp(AConnInfo conn, AProtocol protocol) {
        ByteBuf byteBuf = Gt06DeEnCoder.buildCommRsp(this);
        return byteBuf;
    }
}
