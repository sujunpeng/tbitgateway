package com.tbit.uqbike.tergateway.gt06pkg;

import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.protocol.ATerPkg;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019-6-27 16:13
 */
public abstract class CommPkg extends ATerPkg {
    @JSONField(name = "起始码")
    public byte[] startCode = new byte[2];
    // 1 或者 2字节
    @JSONField(name = "长度")
    public int pkgLen;
    // 1
    @JSONField(name = "命令字")
    public int cmd;
    // 2
    @JSONField(name = "流水号")
    public int serNo;
    // 2
    @JSONField(name = "CRC校验")
    public int crcCode;
    @JSONField(name = "停止码")
    public byte[] stopCode = new byte[2];
}
