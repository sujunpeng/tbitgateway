package com.tbit.uqbike.tergateway.gt06pkg;

import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.Gt06DeEnCoder;
import com.tbit.uqbike.tergateway.data.TerGatewayData;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import com.tbit.uqbike.tergateway.entity.TerTempData;
import com.tbit.uqbike.tergateway.pojo.TerPos;
import io.netty.buffer.ByteBuf;

import java.util.Date;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019-6-27 16:18
 */
public class TerStatus extends CommPkg {
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TerStatus.class);
    @JSONField(name = "终端状态")
    public int terStatus;
    @JSONField(name = "电量")
    public int power;
    @JSONField(name = "GSM")
    public int gsmLevel;
    @JSONField(name = "告警码")
    public int alarmCode;
    @JSONField(name = "语言")
    public int lang;

    @Override
    public void doBusiness() {
        TerTempData terTempData = TerGatewayData.getTerTempDataByMno(this.mno);
        if (null != terTempData) {
            int carStatus = getCarStatus();
            if (carStatus != -1) {
                // 更新最后状态
                terTempData.updateStatus(new Date(), carStatus, 0);
                // 更新内存中的最后位置
                TerPos terLastPos = terTempData.getLastPos();
                if (terLastPos != null) {
                    terLastPos.setCarStatus(carStatus);
                }
            }
        }
    }

    @Override
    public boolean autoRsp() {
        return true;
    }

    @Override
    public ByteBuf getRsp(AConnInfo conn, AProtocol protocol) {
        ByteBuf byteBuf = Gt06DeEnCoder.buildCommRsp(this);
        return byteBuf;
    }

    // 尝试获取状态，如果不能完全确定状态，所以定义返回-1位无效状态
    @JSONField(serialize = false)
    public int getCarStatus() {
        // 油电接通更撤防表示借车状态
        // 油电断开跟设防表示还车状态
        // gt06 的第 7 位表示油电状态， 0表示接通 1表示断
        // gt06 的第 0 位表示设防撤防状态， 0表示撤防 1表示设防
        // 业务平台状态第 0 位为借还车状态， 0表示借车 1表示还车
        boolean oilFlag = ((terStatus >> 7) & 1) == 1;
        boolean sfFlag = ((terStatus >> 0) & 1) == 1;
        if (oilFlag && sfFlag) {
            return 1;
        } else if (!oilFlag && !sfFlag) {
            return 0;
        } else {
            return -1;
        }
    }
}
