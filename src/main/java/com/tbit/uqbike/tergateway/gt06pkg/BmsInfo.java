package com.tbit.uqbike.tergateway.gt06pkg;

import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.TerGatewayMain;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.Gt06DeEnCoder;
import com.tbit.uqbike.tergateway.data.TerGatewayData;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import com.tbit.uqbike.tergateway.entity.TerTempData;
import com.tbit.uqbike.tergateway.pojo.TerBattery;
import com.tbit.uqbike.util.StringUtil;
import io.netty.buffer.ByteBuf;

import java.util.Date;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019-6-27 16:24
 */
public class BmsInfo extends CommPkg {
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(BmsInfo.class);
    @JSONField(name = "时间", format = "yyyy-MM-dd HH:mm:ss")
    public Date dt;
    @JSONField(name = "相对SOC")
    public int soc;
    /**
     * 可用剩余容量
     */
    @JSONField(name = "可用剩余容量")
    public Integer batteryRemaining;
    /**
     * 对soc
     */
    @JSONField(name = "绝对SOC")
    public Short batteryRelativelyRemaining;
    /**
     * 绝对满电容量
     */
    @JSONField(name = "绝对满电容量")
    public Integer batteryElectricity;
    @JSONField(name = "SOH")
    public int soh;
    @JSONField(name = "内部温度")
    public int batteryTemp;
    @JSONField(name = "实时电流")
    public int batteryEI;
    @JSONField(name = "电压")
    public int batteryEU;
    @JSONField(name = "循环次数")
    public int dischargeCnt;
    @JSONField(name = "1～7节电池电压")
    public String power1_7;
    @JSONField(name = "8～14节电池电压")
    public String power8_14;
    @JSONField(name = "当前充电间隔时间")
    public Integer currChargeInv;
    @JSONField(name = "最大充电间隔时间")
    public Integer maxChargeInv;
    @JSONField(name = "读写成本条形码")
    public String codeStr;
    @JSONField(name = "读版本号")
    public String batteryVer;
    @JSONField(name = "电池组制造厂名称")
    public String factoryName;
    @JSONField(name = "电池状态")
    public String batteryStatus;

    @Override
    public void doBusiness() {
        logger.debug(this.toString());
        TerTempData terTempData = TerGatewayData.getTerTempDataByMno(this.mno);
        // 获取电量对象
        TerBattery newTerBattery = getTerBattery();
        TerGatewayMain.terEvnet.terRecvNewBattery(terTempData, newTerBattery);
    }

    private TerBattery getTerBattery() {
        TerBattery terBattery = new TerBattery();
        terBattery.setBatteryEI(this.batteryEI);
        terBattery.setBatteryEU(this.batteryEU);
        terBattery.setBatteryStatus(this.batteryStatus);
        terBattery.setBatteryTemp(this.batteryTemp);
        terBattery.setDischargeCnt(this.dischargeCnt);
        terBattery.setDt(this.dt);
        terBattery.setExData(StringUtil.Empty);
        terBattery.setMachineNO(this.mno);
        terBattery.setSoc(this.soc);
        terBattery.setSoh(this.soh);
        if (this.batteryRemaining != null) {
            terBattery.setBatteryRemaining(batteryRemaining);
        }
        if (this.batteryRelativelyRemaining != null) {
            terBattery.setBatteryRelativelyRemaining(batteryRelativelyRemaining);
        }
        if (this.batteryElectricity != null) {
            terBattery.setBatteryElectricity(batteryElectricity);
        }
        terBattery.setBatteryId(codeStr);
        terBattery.setBatteryVer(batteryVer);
        return terBattery;
    }

    @Override
    public boolean autoRsp() {
        return true;
    }

    @Override
    public ByteBuf getRsp(AConnInfo conn, AProtocol protocol) {
        ByteBuf byteBuf = Gt06DeEnCoder.buildCommRsp(this);
        return byteBuf;
    }
}
