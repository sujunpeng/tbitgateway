package com.tbit.uqbike.tergateway.gt06pkg;

import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import io.netty.buffer.ByteBuf;

import java.util.Date;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019-6-27 16:19
 */
public class AddrQuery extends CommPkg {
    public Date posDt;
    public int gpsCnt;
    public double lon;
    public double lat;
    public int speed;
    public int status;
    public int dir;
    public int pointType = 0;

    public String phone;
    public int lang;

    @Override
    public void doBusiness() {

    }

    @Override
    public boolean autoRsp() {
        return false;
    }

    @Override
    public ByteBuf getRsp(AConnInfo conn, AProtocol protocol) {
        return null;
    }
}
