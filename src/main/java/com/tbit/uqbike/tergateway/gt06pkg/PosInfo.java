package com.tbit.uqbike.tergateway.gt06pkg;

import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.TerGatewayMain;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.Gt06DeEnCoder;
import com.tbit.uqbike.service.ice.icebase.CellIdManage.DPoint;
import com.tbit.uqbike.service.ice.impl.CellIdManageImpl;
import com.tbit.uqbike.tergateway.config.TerGatewayConfig;
import com.tbit.uqbike.tergateway.data.TerGatewayData;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import com.tbit.uqbike.tergateway.entity.TerTempData;
import com.tbit.uqbike.tergateway.pojo.TerPos;
import com.tbit.uqbike.util.ConstDefine;
import com.tbit.uqbike.util.ErrorCode;
import com.tbit.uqbike.util.StringUtil;
import io.netty.buffer.ByteBuf;

import java.util.Date;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019-6-27 16:17
 */
public class PosInfo extends CommPkg {
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(PosInfo.class);
    @JSONField(name = "时间", format = "yyyy-MM-dd HH:mm:ss")
    public Date posDt;
    @JSONField(name = "卫星个数")
    public int gpsCnt;
    @JSONField(name = "经度")
    public double lon;
    @JSONField(name = "纬度")
    public double lat;
    @JSONField(name = "速度")
    public int speed;
    @JSONField(name = "状态")
    public int status;
    @JSONField(name = "方向角")
    public int dir;
    @JSONField(name = "定位方式")
    public int pointType = 0;
    @JSONField(name = "基站")
    public String cellId;

    @Override
    public void doBusiness() {
        if (TerGatewayConfig.bHandlePos) {
            //logger.info(this.toString());
            TerTempData terTempData = TerGatewayData.getTerTempDataByMno(this.mno);
            TerPos terLastPos = terTempData.getLastPos();
            // 先判断是否合法，不合法直接过滤掉不处理
            TerPos newPos = getTerPos(terLastPos);
            int code = ErrorCode.OK;
            // 判断经纬度合法性
            if (!newPos.isLatlonOk()) {
                code = ErrorCode.ErrorLngLatError;
            } else {
                code = TerGatewayMain.terEvnet.terRecvNewPos(terTempData, newPos);
            }
            if (code != ErrorCode.OK) {
                logger.info(String.format("由于:[%s]过滤掉轨迹:[%s]", ErrorCode.ErrorString(code), newPos.toString()));
            }
        }
    }

    private TerPos getTerPos(TerPos terLastPos) {
        TerPos terPos = new TerPos();
        terPos.setDir(this.dir);
        terPos.setDt(this.posDt);
        terPos.setExData(StringUtil.Empty);
        terPos.setHigh(0);
        terPos.setMachineNO(this.mno);
        terPos.setSpeed(this.speed);
        terPos.setLat(this.lat);
        terPos.setLon(this.lon);
        terPos.setMile(0);
        if (terLastPos != null) {
            terPos.setCarStatus(terLastPos.getCarStatus());
        } else {
            terPos.setCarStatus(0);
        }

        terPos.signalStatus = 0;
        terPos.signMile = 0;
        terPos.hallSpeed = 0;

        terPos.setPointType(pointType);
        // 进行小区定位
        if (!terPos.isSatellite()) {
            DPoint dPoint = null;
            try {
                dPoint = CellIdManageImpl.GetDpByCellId(cellId);
            } catch (Exception e) {
                logger.error("CellIdManageImpl.GetDpByCellId", e);
            }
            if (null == dPoint) {
                pointType = ConstDefine.POINT_TYPE_UNKNOW;
            } else {
                pointType = ConstDefine.POINT_TYPE_CELL;
                terPos.setLon(dPoint.x);
                terPos.setLat(dPoint.y);
            }
        }
        terPos.setPointType(pointType);
        return terPos;
    }

    @Override
    public boolean autoRsp() {
        return true;
    }

    @Override
    public ByteBuf getRsp(AConnInfo conn, AProtocol protocol) {
        ByteBuf byteBuf = Gt06DeEnCoder.buildCommRsp(this);
        return byteBuf;
    }
}
