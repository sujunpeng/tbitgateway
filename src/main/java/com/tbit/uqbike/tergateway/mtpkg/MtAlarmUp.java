package com.tbit.uqbike.tergateway.mtpkg;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.MeituanDeEnCoder;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import io.netty.buffer.ByteBuf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019/2/20 0020 10:49
 */
public class MtAlarmUp extends MtBasePkg {
    public static class CarInfo {
        public int lock;
        public int bl;
        public int gsm;
        public int ks;
        public List<Object> et;
    }

    public static class GpsInfo {
        @JSONField(name = "纬度")
        public double lat;
        @JSONField(name = "经度")
        public double lng;
        @JSONField(name = "卫星个数")
        public int satelites;
        @JSONField(name = "当前里程数")
        public int mile;
        @JSONField(name = "速度")
        public double gs;
        @JSONField(name = "方向")
        public int course;
        @JSONField(name = "定位状态")
        public int gt;
        @JSONField(name = "扩展信息")
        public int ext;
        @JSONField(name = "时间戳")
        public int timestamp;
    }

    public static class BatterInfo {
        @JSONField(name = "剩余电量百分比")
        public int battery;
        @JSONField(name = "循环次数")
        public int chargeCount;
        @JSONField(name = "充电状态")
        public int chargeStatus;
        @JSONField(name = "总电压")
        public int batteryVoltage;
        @JSONField(name = "实时电流")
        public int batteryCurrent;
        @JSONField(name = "电池容量")
        public int batteryCapacity;
        @JSONField(name = "电池总容量")
        public int totalBatteryCapacity;
        @JSONField(name = "健康状态百分比")
        public int batteryHealth;
        @JSONField(name = "电池温度")
        public int batteryTemperature;
        @JSONField(name = "电池版本")
        public String batteryVersion;
        @JSONField(name = "电池故障")
        public String btb;
        @JSONField(name = "时间戳")
        public int timeStamp;
        @JSONField(name = "扩展信息")
        public Object ext;
    }


    private static Logger logger = LoggerFactory.getLogger(MtAlarmUp.class);

    @JSONField(name = "中控id")
    public String sn;
    @JSONField(name = "时间戳")
    public int timestamp;
    @JSONField(name = "告警类型")
    public String warnType;
    @JSONField(name = "车辆信息")
    public CarInfo epst;
    @JSONField(name = "定位信息")
    public GpsInfo gps;
    @JSONField(name = "电池信息")
    public BatterInfo battery;


    @Override
    public void doBusiness() {
        logger.info(JSONObject.toJSONString(this));
    }

    @Override
    public boolean autoRsp() {
        return true;
    }

    @Override
    public ByteBuf getRsp(AConnInfo conn, AProtocol protocol) {
        return ((MeituanDeEnCoder) protocol).buildDownMsg(head, serNo, "");
    }
}
