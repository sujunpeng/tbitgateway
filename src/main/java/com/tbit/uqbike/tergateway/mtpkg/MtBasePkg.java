package com.tbit.uqbike.tergateway.mtpkg;

import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.protocol.ATerPkg;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019/2/20 0020 11:22
 */
public abstract class MtBasePkg extends ATerPkg {
    @JSONField(name = "报头")
    public MtPkgHead head;
    @JSONField(name = "序列号")
    public String serNo;
}
