package com.tbit.uqbike.tergateway.mtpkg;

import com.tbit.uqbike.tergateway.entity.RemoteControl;
import com.tbit.uqbike.util.ConstDefine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019/2/20 0020 11:59
 */
public class TerControl {
    private static Logger logger = LoggerFactory.getLogger(TerControl.class);

    public static class CmdKv {
        public String key;
        public String type = "java.lang.String";
        public Object value;
    }

    public static class CmdContent {
        public String c;
        public List<CmdKv> params;
    }

    public static class ControlCmd {
        public String cmdId;
        public List<CmdContent> content;
    }

    public String sn;
    public ControlCmd cmd;


    public static TerControl buildTerControl(RemoteControl remoteControl) {
        TerControl tc = null;
        if (ConstDefine.CONTROL_TYPE_GET.equals(remoteControl.controlType)) {
            tc = new TerControl();
            tc.sn = remoteControl.sn;
            tc.cmd = new ControlCmd();
            tc.cmd.cmdId = remoteControl.serNO;
            tc.cmd.content = new LinkedList<>();
            CmdContent cmdContent = new CmdContent();
            tc.cmd.content.add(cmdContent);
            cmdContent.c = "QUERY_PARAMS";
            cmdContent.params = new LinkedList<>();
            CmdKv kv = new CmdKv();
            cmdContent.params.add(kv);
            kv.key = remoteControl.paramName.replace(";", "");
            kv.value = true;
        } else if (ConstDefine.CONTROL_TYPE_SET.equals(remoteControl.controlType)) {
            tc = new TerControl();
            tc.sn = remoteControl.sn;
            tc.cmd = new ControlCmd();
            tc.cmd.cmdId = remoteControl.serNO;
            tc.cmd.content = new LinkedList<>();
            CmdContent cmdContent = new CmdContent();
            tc.cmd.content.add(cmdContent);
            cmdContent.c = "SET_PARAMS";
            cmdContent.params = new LinkedList<>();
            CmdKv kv = new CmdKv();
            cmdContent.params.add(kv);
            String[] params = remoteControl.paramName.replace(";", "").split("=");
            kv.key = params[0];
            kv.value = params[1];
        } else if (ConstDefine.CONTROL_TYPE_CONTROL.equals(remoteControl.controlType)) {
            tc = new TerControl();
            tc.sn = remoteControl.sn;
            tc.cmd = new ControlCmd();
            tc.cmd.cmdId = remoteControl.serNO;
            tc.cmd.content = new LinkedList<>();
            CmdContent cmdContent = new CmdContent();
            tc.cmd.content.add(cmdContent);
            cmdContent.params = new LinkedList<>();
            CmdKv kv = new CmdKv();
            cmdContent.params.add(kv);
            Integer code = Integer.parseInt(remoteControl.paramName);
            if (code == 0x1 || code == 0x2) {
                cmdContent.c = "SECURITY_MODEL";
                kv.key = "open";
                if (code == 0x01) {
                    kv.value = 1;
                } else {
                    kv.value = 0;
                }
            } else if (code == 0xf || code == 0x10) {
                cmdContent.c = "BATTERY_LOCK";
                kv.key = "open";
                if (code == 0xf) {
                    kv.value = 1;
                } else {
                    kv.value = 0;
                }
            } else if (code == 0x3) {
                cmdContent.c = "RESTART";
                kv.key = "restart";
                kv.value = 1;
            } else if (code == 0xb || code == 0xc) {
                cmdContent.c = "OPEN_LOCK";
                kv.key = "lock";
                if (code == 0xb) {
                    kv.value = 1;
                } else {
                    kv.value = 0;
                }
            }
        } else if (ConstDefine.CONTROL_TYPE_VOICE.equals(remoteControl.controlType)) {
            tc = new TerControl();
            tc.sn = remoteControl.sn;
            tc.cmd = new ControlCmd();
            tc.cmd.cmdId = remoteControl.serNO;
            tc.cmd.content = new LinkedList<>();
            CmdContent cmdContent = new CmdContent();
            tc.cmd.content.add(cmdContent);
            cmdContent.c = "SPEECH";
            cmdContent.params = new LinkedList<>();
            CmdKv kv = new CmdKv();
            cmdContent.params.add(kv);
            kv.key = "voiceId";
            kv.value = remoteControl.paramName;
        } else {
            logger.error(String.format("构造下行数据包异常,未知的指令类型:[%s]", remoteControl.controlType));
            return null;
        }
        return tc;
    }
}
