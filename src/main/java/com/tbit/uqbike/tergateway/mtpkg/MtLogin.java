package com.tbit.uqbike.tergateway.mtpkg;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.MeituanDeEnCoder;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import io.netty.buffer.ByteBuf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019/2/20 0020 10:48
 */
public class MtLogin extends MtBasePkg {
    private static Logger logger = LoggerFactory.getLogger(MtLogin.class);
    @JSONField(name = "固件版本号")
    public String osVer;
    @JSONField(name = "厂商")
    public String hardwareVer;
    @JSONField(name = "型号")
    public String model;
    @JSONField(name = "IMSI")
    public String imsi;
    @JSONField(name = "产品ID")
    public String regcode;
    @JSONField(name = "连接类型")
    public int connectType;
    @JSONField(name = "设备状态")
    public int status;
    @JSONField(name = "errCode")
    public int errCode;

    public static class LoginRsp {
        public int errorCode;//0:成功，1:失败(未激活)
        public int interval;//心跳周期（单位：s ）
        public long currentTime;//时间戳
    }

    @Override
    public void doBusiness() {
        logger.info(JSONObject.toJSONString(this));
    }

    @Override
    public boolean autoRsp() {
        return true;
    }

    @Override
    public ByteBuf getRsp(AConnInfo conn, AProtocol protocol) {
        LoginRsp loginRsp = new LoginRsp();
        loginRsp.errorCode = 0;
        loginRsp.interval = 20;
        loginRsp.currentTime = System.currentTimeMillis();
        return ((MeituanDeEnCoder) protocol).buildDownMsg(head, serNo, JSONObject.toJSONString(loginRsp));
    }
}
