package com.tbit.uqbike.tergateway.mtpkg;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.TerGatewayMain;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.MeituanDeEnCoder;
import com.tbit.uqbike.tergateway.data.TerGatewayData;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import com.tbit.uqbike.tergateway.entity.TerTempData;
import com.tbit.uqbike.tergateway.pojo.TerPos;
import com.tbit.uqbike.util.ErrorCode;
import com.tbit.uqbike.util.StringUtil;
import io.netty.buffer.ByteBuf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019/2/20 0020 10:49
 */
public class MtInfoUpload extends MtBasePkg {

    public static class BatteryInfo {
        @JSONField(name = "数据类型", serialize = false)
        public int type;
        @JSONField(name = "中控ID")
        public String sn;
        @JSONField(name = "剩余电量百分比")
        public int battery;
        @JSONField(name = "电池")
        public String BMSID;
        @JSONField(name = "循环次数")
        public int chargeCount;
        @JSONField(name = "充电状态")
        public int chargeStatus;
        @JSONField(name = "总电压")
        public int batteryVoltage;
        @JSONField(name = "可用剩余电池容量")
        public int batteryCurrent;
        @JSONField(name = "电池总容量")
        public int totalBatteryCapacity;
        @JSONField(name = "健康状态百分比")
        public int batteryHealth;
        @JSONField(name = "实时最高温度")
        public int batteryMaxTemperature;
        @JSONField(name = "实时最低温度")
        public int batteryMinTemperature;
        @JSONField(name = "电池版本")
        public String batteryVersion;
        @JSONField(name = "电池状态")
        public List<Integer> batteryStatus;
        @JSONField(name = "型号")
        public String modelNumber;
        @JSONField(name = "厂商")
        public String merchantCode;
        @JSONField(name = "时间戳")
        public int timeStamp;
        @JSONField(name = "扩展字段")
        public Object ext;
    }

    public static class GpsInfo {
        @JSONField(name = "数据类型", serialize = false)
        public int type;
        @JSONField(name = "中控ID")
        public String sn;
        @JSONField(name = "纬度")
        public double lat;
        @JSONField(name = "经度")
        public double lng;
        @JSONField(name = "卫星个数")
        public int satelites;
        @JSONField(name = "当前里程数")
        public int mile;
        @JSONField(name = "速度")
        public double gpsSpeed;
        @JSONField(name = "方向")
        public int course;
        @JSONField(name = "定位状态")
        public int gpsStatus;
        @JSONField(name = "健康状态百分比")
        public int batteryHealth;
        @JSONField(name = "定位类型")
        public int gpsType;
        @JSONField(name = "电池温度")
        public int ext;
        @JSONField(name = "时间戳")
        public int timestamp;
    }

    public static class CarInfo {
        @JSONField(name = "数据类型", serialize = false)
        public int type;
        @JSONField(name = "中控ID")
        public String sn;
        @JSONField(name = "控制器sn")
        public String csn;
        @JSONField(name = "防盗状态")
        public int securityState;
        @JSONField(name = "电门状态")
        public int lock;
        @JSONField(name = "电池链接状态")
        public int batteryLink;
        @JSONField(name = "信号强度")
        public int gsm;
        @JSONField(name = "车辆起停")
        public int isMove;
        @JSONField(name = "速度")
        public double speed;
        @JSONField(name = "总里程")
        public double totalMileage;
        @JSONField(name = "单次里程")
        public double mileage;
        @JSONField(name = "剩余里程")
        public double remainingileage;
        @JSONField(name = "支架状态")
        public int ks;
        @JSONField(name = "车辆故障")
        public List<Object> estatus;
        @JSONField(name = "扩展字段")
        public Object ext;
        @JSONField(name = "时间戳")
        public int timestamp;

    }

    private static Logger logger = LoggerFactory.getLogger(MtInfoUpload.class);

    @JSONField(name = "数据类型")
    public int type;
    public BatteryInfo batteryInfo;
    public GpsInfo gpsInfo;
    public CarInfo carInfo;

    @Override
    public void doBusiness() {
        logger.info(JSONObject.toJSONString(this));
        if (type != 2) {
            return;
        }
        TerTempData terTempData = TerGatewayData.getTerTempDataByMno(this.mno);
        TerPos terLastPos = terTempData.getLastPos();
        TerPos newPos = _getTerPos();
        int code = ErrorCode.OK;
        // 判断经纬度合法性
        if (!newPos.isLatlonOk()) {
            code = ErrorCode.ErrorLngLatError;
        } else {
            code = TerGatewayMain.terEvnet.terRecvNewPos(terTempData, newPos);
        }
        if (code != ErrorCode.OK) {
            logger.info(String.format("由于:[%s]过滤掉轨迹:[%s]", ErrorCode.ErrorString(code), newPos.toString()));
        }
    }

    public TerPos _getTerPos() {
        TerPos terPos = new TerPos();
        terPos.setDir(gpsInfo.course);
        terPos.setDt(new Date(gpsInfo.timestamp * 1000L));
        terPos.setExData(StringUtil.Empty);
        terPos.setHigh(0);
        terPos.setMachineNO(this.mno);
        terPos.setSpeed((int) gpsInfo.gpsSpeed);
        terPos.setLat(gpsInfo.lat);
        terPos.setLon(gpsInfo.lng);
        terPos.setMile(gpsInfo.mile);
        terPos.setCarStatus(0);
        terPos.setPointType(gpsInfo.gpsStatus);
        return terPos;
    }

    @Override
    public boolean autoRsp() {
        return true;
    }

    @Override
    public ByteBuf getRsp(AConnInfo conn, AProtocol protocol) {
        return ((MeituanDeEnCoder) protocol).buildDownMsg(head, serNo, "");
    }
}
