package com.tbit.uqbike.tergateway.mtpkg;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.MeituanDeEnCoder;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import io.netty.buffer.ByteBuf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019/2/20 0020 10:49
 */
public class MtEventUpload extends MtBasePkg {
    private static Logger logger = LoggerFactory.getLogger(MtEventUpload.class);

    @JSONField(name = "中控id")
    public String sn;
    @JSONField(name = "时间戳")
    public int timestamp;
    @JSONField(name = "事件类型")
    public String eventType;
    @JSONField(name = "车辆信息")
    public MtAlarmUp.CarInfo epst;
    @JSONField(name = "定位信息")
    public MtAlarmUp.GpsInfo gps;
    @JSONField(name = "电池信息")
    public MtAlarmUp.BatterInfo battery;

    @Override
    public void doBusiness() {
        logger.info(JSONObject.toJSONString(this));
    }

    @Override
    public boolean autoRsp() {
        return true;
    }

    @Override
    public ByteBuf getRsp(AConnInfo conn, AProtocol protocol) {
        return ((MeituanDeEnCoder) protocol).buildDownMsg(head, serNo, "");
    }
}
