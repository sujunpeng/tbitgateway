package com.tbit.uqbike.tergateway.mtpkg;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019/2/20 0020 10:26
 */
public class MtPkgHead {
    @JSONField(name = "报文类型")
    public int type;
    @JSONField(name = "协议版本")
    public int version;
    @JSONField(name = "设备编号")
    public String sn;
    @JSONField(name = "校验码")
    public int crc;
    @JSONField(name = "数据长度")
    public int len;
}
