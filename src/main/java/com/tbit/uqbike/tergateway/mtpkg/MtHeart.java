package com.tbit.uqbike.tergateway.mtpkg;

import com.alibaba.fastjson.JSONObject;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.MeituanDeEnCoder;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import io.netty.buffer.ByteBuf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019/2/20 0020 10:48
 */
public class MtHeart extends MtBasePkg {
    private static Logger logger = LoggerFactory.getLogger(MtHeart.class);

    @Override
    public void doBusiness() {
        logger.info(JSONObject.toJSONString(this));
    }

    @Override
    public boolean autoRsp() {
        return true;
    }

    @Override
    public ByteBuf getRsp(AConnInfo conn, AProtocol protocol) {
        return ((MeituanDeEnCoder) protocol).buildDownMsg(head);
    }
}
