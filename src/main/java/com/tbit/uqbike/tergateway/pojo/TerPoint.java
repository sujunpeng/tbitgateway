package com.tbit.uqbike.tergateway.pojo;

import java.util.Date;

public class TerPoint {
    private String terIdent;

    private Date dt;

    private Integer dataType;

    private String dataValue;

    public String getTerIdent() {
        return terIdent;
    }

    public void setTerIdent(String terIdent) {
        this.terIdent = terIdent == null ? null : terIdent.trim();
    }

    public Date getDt() {
        return dt;
    }

    public void setDt(Date dt) {
        this.dt = dt;
    }

    public Integer getDataType() {
        return dataType;
    }

    public void setDataType(Integer dataType) {
        this.dataType = dataType;
    }

    public String getDataValue() {
        return dataValue;
    }

    public void setDataValue(String dataValue) {
        this.dataValue = dataValue == null ? null : dataValue.trim();
    }
}