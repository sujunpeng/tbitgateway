package com.tbit.uqbike.tergateway.pojo;

public class Tersoftware extends TersoftwareKey {
    private byte[] pkgdata;

    public Tersoftware(Integer customercode, Integer hardwaremodel, Integer version, Integer firmwaretype) {
        super(customercode, hardwaremodel, version, firmwaretype);
    }

    public Tersoftware() {
    }

    public byte[] getPkgdata() {
        return pkgdata;
    }

    public void setPkgdata(byte[] pkgdata) {
        this.pkgdata = pkgdata;
    }
}