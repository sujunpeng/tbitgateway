package com.tbit.uqbike.tergateway.pojo;

import java.util.Date;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2020-04-22 20:04
 */
public class TerOnlineHis extends TerOnline {
    private Date endDt;

    public Date getEndDt() {
        return endDt;
    }

    public void setEndDt(Date endDt) {
        this.endDt = endDt;
    }
}
