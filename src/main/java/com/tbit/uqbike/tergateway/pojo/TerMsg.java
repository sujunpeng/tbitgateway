package com.tbit.uqbike.tergateway.pojo;

import java.util.Date;

/**
 * Created by MyWin on 2017/5/4.
 */
public class TerMsg {
    private String machineNO;
    private Date dt;
    private String msg;
    private String exMsg;

    public TerMsg() {
    }

    public TerMsg(String machineNO, Date dt, String msg, String exMsg) {
        this.machineNO = machineNO;
        this.dt = dt;
        this.msg = msg;
        this.exMsg = exMsg;
    }

    public String getExMsg() {
        return exMsg;
    }

    public void setExMsg(String exMsg) {
        this.exMsg = exMsg;
    }

    public String getMachineNO() {
        return machineNO;
    }

    public void setMachineNO(String machineNO) {
        this.machineNO = machineNO;
    }

    public Date getDt() {
        return dt;
    }

    public void setDt(Date dt) {
        this.dt = dt;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "TerMsg{" +
                "machineNO='" + machineNO + '\'' +
                ", dt=" + dt +
                ", msg='" + msg + '\'' +
                ", exMsg='" + exMsg + '\'' +
                '}';
    }
}
