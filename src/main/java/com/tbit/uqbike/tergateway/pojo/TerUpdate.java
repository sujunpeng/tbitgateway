package com.tbit.uqbike.tergateway.pojo;

import java.util.Date;

public class TerUpdate {
    private String machineno;

    private Date dt;

    private Integer customercode;

    private Integer hardwaremodel;

    private Integer version;

    private Integer firmwaretype;

    public String getMachineno() {
        return machineno;
    }

    public void setMachineno(String machineno) {
        this.machineno = machineno == null ? null : machineno.trim();
    }

    public Date getDt() {
        return dt;
    }

    public void setDt(Date dt) {
        this.dt = dt;
    }

    public Integer getCustomercode() {
        return customercode;
    }

    public void setCustomercode(Integer customercode) {
        this.customercode = customercode;
    }

    public Integer getHardwaremodel() {
        return hardwaremodel;
    }

    public void setHardwaremodel(Integer hardwaremodel) {
        this.hardwaremodel = hardwaremodel;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getFirmwaretype() {
        return firmwaretype;
    }

    public void setFirmwaretype(Integer firmwaretype) {
        this.firmwaretype = firmwaretype;
    }
}