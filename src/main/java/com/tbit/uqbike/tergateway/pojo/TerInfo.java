package com.tbit.uqbike.tergateway.pojo;

import java.util.Date;

/**
 * Created by MyWin on 2017/5/4.
 */
public class TerInfo {
    private String machineNO;
    private String imei;
    private String imsi;
    private String funCode;
    private String factoryCode;
    private String softVersion;
    private Date joinTime;
    private int syncTick;
    private String syncParam;

    /**
     * 是否支持头盔 1 支持 0 不支持
     */
    private int helmetInfo;

    public String getMachineNO() {
        return machineNO;
    }

    public void setMachineNO(String machineNO) {
        this.machineNO = machineNO;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getFunCode() {
        return funCode;
    }

    public void setFunCode(String funCode) {
        this.funCode = funCode;
    }

    public String getFactoryCode() {
        return factoryCode;
    }

    public void setFactoryCode(String factoryCode) {
        this.factoryCode = factoryCode;
    }

    public String getSoftVersion() {
        return softVersion;
    }

    public void setSoftVersion(String softVersion) {
        this.softVersion = softVersion;
    }

    public Date getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(Date joinTime) {
        this.joinTime = joinTime;
    }

    public int getSyncTick() {
        return syncTick;
    }

    public void setSyncTick(int syncTick) {
        this.syncTick = syncTick;
    }

    public String getSyncParam() {
        return syncParam;
    }

    public void setSyncParam(String syncParam) {
        this.syncParam = syncParam;
    }

    public int getHelmetInfo() {
        return helmetInfo;
    }

    public void setHelmetInfo(int helmetInfo) {
        this.helmetInfo = helmetInfo;
    }

    @Override
    public String toString() {
        return "TerInfo{" +
                "machineNO='" + machineNO + '\'' +
                ", imei='" + imei + '\'' +
                ", imsi='" + imsi + '\'' +
                ", funCode='" + funCode + '\'' +
                ", factoryCode='" + factoryCode + '\'' +
                ", softVersion='" + softVersion + '\'' +
                ", joinTime=" + joinTime +
                ", syncTick=" + syncTick +
                ", syncParam='" + syncParam + '\'' +
                '}';
    }
}
