package com.tbit.uqbike.tergateway.pojo;

import com.tbit.uqbike.tergateway.entity.RemoteControl;
import com.tbit.uqbike.util.ConstDefine;

import java.util.Date;

public class TerOfflineOrder {
    private String serstr;

    private String machineno;

    private String controltype;

    private Date moddt;

    private String paramname;

    private String paramvalue;

    private Date downdt;

    private Integer terret;

    private String terdata;

    private Date terrspdt;

    public RemoteControl toRemoteControl() {
        RemoteControl remoteControl = new RemoteControl();
        remoteControl.serNO = serstr;
        remoteControl.sn = machineno;
        remoteControl.controlType = controltype;
        remoteControl.paramValue = paramvalue;
        remoteControl.paramName = paramname;
        remoteControl.isOfflineOrder = true;

        remoteControl.fromType = ConstDefine.SYS_IDENT_REDIS;
        remoteControl.from = "";
        return remoteControl;
    }

    public String getSerstr() {
        return serstr;
    }

    public void setSerstr(String serstr) {
        this.serstr = serstr == null ? null : serstr.trim();
    }

    public String getMachineno() {
        return machineno;
    }

    public void setMachineno(String machineno) {
        this.machineno = machineno == null ? null : machineno.trim();
    }

    public String getControltype() {
        return controltype;
    }

    public void setControltype(String controltype) {
        this.controltype = controltype == null ? null : controltype.trim();
    }

    public Date getModdt() {
        return moddt;
    }

    public void setModdt(Date moddt) {
        this.moddt = moddt;
    }

    public String getParamname() {
        return paramname;
    }

    public void setParamname(String paramname) {
        this.paramname = paramname == null ? null : paramname.trim();
    }

    public String getParamvalue() {
        return paramvalue;
    }

    public void setParamvalue(String paramvalue) {
        this.paramvalue = paramvalue == null ? null : paramvalue.trim();
    }

    public Date getDowndt() {
        return downdt;
    }

    public void setDowndt(Date downdt) {
        this.downdt = downdt;
    }

    public Integer getTerret() {
        return terret;
    }

    public void setTerret(Integer terret) {
        this.terret = terret;
    }

    public String getTerdata() {
        return terdata;
    }

    public void setTerdata(String terdata) {
        this.terdata = terdata == null ? null : terdata.trim();
    }

    public Date getTerrspdt() {
        return terrspdt;
    }

    public void setTerrspdt(Date terrspdt) {
        this.terrspdt = terrspdt;
    }
}