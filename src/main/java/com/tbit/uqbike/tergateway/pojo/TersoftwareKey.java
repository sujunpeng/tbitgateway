package com.tbit.uqbike.tergateway.pojo;

public class TersoftwareKey {

    public static final TersoftwareKey Default = new TersoftwareKey();

    private Integer customercode;

    private Integer hardwaremodel;

    private Integer version;

    private Integer firmwaretype;

    public TersoftwareKey() {
    }

    public TersoftwareKey(Integer customercode, Integer hardwaremodel, Integer version, Integer firmwaretype) {
        this.customercode = customercode;
        this.hardwaremodel = hardwaremodel;
        this.version = version;
        this.firmwaretype = firmwaretype;
    }

    public Integer getCustomercode() {
        return customercode;
    }

    public void setCustomercode(Integer customercode) {
        this.customercode = customercode;
    }

    public Integer getHardwaremodel() {
        return hardwaremodel;
    }

    public void setHardwaremodel(Integer hardwaremodel) {
        this.hardwaremodel = hardwaremodel;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Integer getFirmwaretype() {
        return firmwaretype;
    }

    public void setFirmwaretype(Integer firmwaretype) {
        this.firmwaretype = firmwaretype;
    }

}