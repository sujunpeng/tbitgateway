package com.tbit.uqbike.tergateway.pojo;

import java.util.Date;

public class TerOnline {
    private String mno;

    private Date dt;

    private Byte online;

    public String getMno() {
        return mno;
    }

    public void setMno(String mno) {
        this.mno = mno == null ? null : mno.trim();
    }

    public Date getDt() {
        return dt;
    }

    public void setDt(Date dt) {
        this.dt = dt;
    }

    public Byte getOnline() {
        return online;
    }

    public void setOnline(Byte online) {
        this.online = online;
    }

    public TerOnline(String mno, Date dt, Byte online) {
        this.mno = mno;
        this.dt = dt;
        this.online = online;
    }

    public TerOnline(String mno, Date dt) {
        this.mno = mno;
        this.dt = dt;
    }

    public TerOnline(String mno, Byte online) {
        this.mno = mno;
        this.online = online;
    }

    public TerOnline() {
    }
}