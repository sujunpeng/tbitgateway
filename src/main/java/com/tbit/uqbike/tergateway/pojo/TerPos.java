package com.tbit.uqbike.tergateway.pojo;

import com.tbit.uqbike.tergateway.config.TerGatewayConfig;
import com.tbit.uqbike.util.ConstDefine;
import com.tbit.uqbike.util.StringUtil;
import com.tbit.utils.BitUtil;

import java.util.Date;

/**
 * Created by MyWin on 2017/5/4.
 */
public class TerPos {

    private String machineNO;
    private Date dt;
    private int pointType;
    private double lon;
    private double lat;
    private int high;
    private int dir;
    private int speed;
    private int mile;
    private int carStatus;
    private String exData;

    // 扩展属性，其它应用需要的属性
    public int signalStatus;
    public int signMile;
    public int hallSpeed;
    public boolean wa206c;// 是否是206c版本
    // 小区号
    public String lacId = "";
    // 基站号
    public String cellId = "";

    public TerPos() {
    }

    public int getCarStatus() {
        return carStatus;
    }

    public void setCarStatus(int carStatus) {
        this.carStatus = carStatus;
    }

    public int getMile() {
        return mile;
    }

    public void setMile(int mile) {
        this.mile = mile;
    }

    public String getMachineNO() {
        return machineNO;
    }

    public void setMachineNO(String machineNO) {
        this.machineNO = machineNO;
    }

    public Date getDt() {
        return dt;
    }

    public void setDt(Date dt) {
        this.dt = dt;
    }

    public int getPointType() {
        return pointType;
    }

    public void setPointType(int pointType) {
        this.pointType = pointType;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public int getHigh() {
        return high;
    }

    public void setHigh(int high) {
        this.high = high;
    }

    public int getDir() {
        return dir;
    }

    public void setDir(int dir) {
        this.dir = dir;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public String getExData() {
        return exData;
    }

    public void setExData(String exData) {
        this.exData = exData;
    }

    public void cloneObj(TerPos obj) {
        this.machineNO = obj.machineNO;
        this.dt = obj.dt;
        this.pointType = obj.pointType;
        this.lon = obj.lon;
        this.lat = obj.lat;
        this.high = obj.high;
        this.dir = obj.dir;
        this.speed = obj.speed;
        this.mile = obj.mile;
        this.carStatus = obj.carStatus;
        this.exData = obj.exData;
        this.signalStatus = obj.signalStatus;
        this.signMile = obj.signMile;
        this.hallSpeed = obj.hallSpeed;
        this.wa206c = obj.wa206c;

        this.lacId = obj.lacId;
        this.cellId = obj.cellId;
    }

    @Override
    public String toString() {
        return "TerPos{" +
                "machineNO='" + machineNO + '\'' +
                ", dt=" + dt +
                ", pointType=" + pointType +
                ", lon=" + lon +
                ", lat=" + lat +
                ", high=" + high +
                ", dir=" + dir +
                ", speed=" + speed +
                ", mile=" + mile +
                ", exData='" + exData + '\'' +
                '}';
    }

    /**
     * 默认位置，先写死，后面部分属性可以移动到spring配置文件里面初始化，便于更改
     *
     * @param mno
     * @return
     */
    public static TerPos GetDefaultPos(String mno, Date dt) {
        TerPos terPos = new TerPos();
        terPos.machineNO = mno;
        terPos.pointType = 0;
        terPos.dt = dt;
        terPos.lat = TerGatewayConfig.DefaultPosLat;
        terPos.lon = TerGatewayConfig.DefaultPosLon;
        terPos.exData = StringUtil.Empty;
        terPos.carStatus = -1;
        return terPos;
    }

    /**
     * 是否是精确定位
     *
     * @return
     */
    public boolean isSatellite() {
        if (this.pointType == ConstDefine.POINT_TYPE_BEIDOU || pointType == ConstDefine.POINT_TYPE_GPS) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 是否是静止状态
     *
     * @return
     */
    public boolean isStop() {
        return this.speed == 0;
    }

    /**
     * @return
     */
    public boolean isLatlonOk() {
        if (lon < -180 || lon > 180 || lat < -90 || lat > 90) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 是否是历史轨迹
     *
     * @param lastPos
     * @return
     */
    public boolean isNewPos(TerPos lastPos) {
        return dt.after(lastPos.dt);
    }

    public void addExData(String key, String value) {
        if (exData == null || exData.isEmpty()) {
            exData = String.format("%s=%s", key, value);
        } else {
            exData = String.format("%s;%s=%s", exData, key, value);
        }
    }

    /**
     * 检查位置是否为静止
     * @param pos
     * @return
     */
    public static boolean checkStop(TerPos pos) {
        return !BitUtil.check(pos.carStatus, 1);
    }
}
