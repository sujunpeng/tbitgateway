package com.tbit.uqbike.tergateway.pojo;

import com.alibaba.fastjson.JSONObject;
import com.tbit.uqbike.util.ConstDefine;
import com.tbit.uqbike.util.StringUtil;

import java.util.Date;
import java.util.List;

/**
 * Created by MyWin on 2017/5/4.
 */
public class TerAlarm {
    private String machineNO;
    private Date dt;
    private int alarmType;
    private String alarmExMsg;
    private String errCode;

    private List<Integer> alarmExCode;

    public List<Integer> getAlarmExCode() {
        return alarmExCode;
    }

    public void setAlarmExCode(List<Integer> alarmExCode) {
        this.alarmExCode = alarmExCode;
    }

    public String getAlarmExMsg() {
        return alarmExMsg;
    }

    public void setAlarmExMsg(String alarmExMsg) {
        this.alarmExMsg = alarmExMsg;
    }

    public String getMachineNO() {
        return machineNO;
    }

    public void setMachineNO(String machineNO) {
        this.machineNO = machineNO;
    }

    public Date getDt() {
        return dt;
    }

    public void setDt(Date dt) {
        this.dt = dt;
    }

    public int getAlarmType() {
        return alarmType;
    }

    public void setAlarmType(int alarmType) {
        this.alarmType = alarmType;
    }

    public String getErrCode() {
        return errCode;
    }

    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }

    public static String getAlarmPushMsg(TerAlarm terAlarm) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("msgId", ConstDefine.MQ_MSG_ID_ALARM_PUSH);
        jsonObject.put("feedback", StringUtil.Empty);
        jsonObject.put("data", terAlarm);
        return jsonObject.toJSONString();
    }

    @Override
    public String toString() {
        return "TerAlarm{" +
                "machineNO='" + machineNO + '\'' +
                ", dt=" + dt +
                ", alarmType=" + alarmType +
                ", errCode='" + errCode + '\'' +
                '}';
    }
}
