package com.tbit.uqbike.tergateway.pojo;

import java.util.Date;

public class TerAttrEx extends TerAttrExKey {
    private String attrValue;

    private Date attrDt;

    public String getAttrValue() {
        return attrValue;
    }

    public void setAttrValue(String attrValue) {
        this.attrValue = attrValue == null ? null : attrValue.trim();
    }

    public Date getAttrDt() {
        return attrDt;
    }

    public void setAttrDt(Date attrDt) {
        this.attrDt = attrDt;
    }
}