package com.tbit.uqbike.tergateway.pojo;

import java.util.Date;

public class OrderStat {
    private String project;

    private String subproject;

    private String module;

    private String submodule;

    private Date dt;

    private String controlType;

    private String controlName;

    private String from;

    private String to;

    private String controlRsp;

    private Date rspDt;

    private String orderIndent;

    private String mno;

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project == null ? null : project.trim();
    }

    public String getSubproject() {
        return subproject;
    }

    public void setSubproject(String subproject) {
        this.subproject = subproject == null ? null : subproject.trim();
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module == null ? null : module.trim();
    }

    public String getSubmodule() {
        return submodule;
    }

    public void setSubmodule(String submodule) {
        this.submodule = submodule == null ? null : submodule.trim();
    }

    public Date getDt() {
        return dt;
    }

    public void setDt(Date dt) {
        this.dt = dt;
    }

    public String getControlType() {
        return controlType;
    }

    public void setControlType(String controlType) {
        this.controlType = controlType == null ? null : controlType.trim();
    }

    public String getControlName() {
        return controlName;
    }

    public void setControlName(String controlName) {
        this.controlName = controlName == null ? null : controlName.trim();
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from == null ? null : from.trim();
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to == null ? null : to.trim();
    }

    public String getControlRsp() {
        return controlRsp;
    }

    public void setControlRsp(String controlRsp) {
        this.controlRsp = controlRsp == null ? null : controlRsp.trim();
    }

    public Date getRspDt() {
        return rspDt;
    }

    public void setRspDt(Date rspDt) {
        this.rspDt = rspDt;
    }

    public String getOrderIndent() {
        return orderIndent;
    }

    public void setOrderIndent(String orderIndent) {
        this.orderIndent = orderIndent == null ? null : orderIndent.trim();
    }

    public String getMno() {
        return mno;
    }

    public void setMno(String mno) {
        this.mno = mno == null ? null : mno.trim();
    }
}