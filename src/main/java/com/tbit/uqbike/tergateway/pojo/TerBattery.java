package com.tbit.uqbike.tergateway.pojo;

import java.util.Date;

/**
 * Created by MyWin on 2017/5/9.
 * 增加默认值-1
 */
public class TerBattery {
    private String machineNO;
    private Date dt;
    private int soc = -1;
    private int soh = -1;
    private int batteryTemp = -1;
    private int batteryEI = -1;
    private int batteryEU = -1;
    private int dischargeCnt = -1;
    private String batteryStatus;
    private String exData;
    private String batteryId;
    private String batteryVer;

    /**
     * 绝对满电容量
     */
    private Integer batteryElectricity;
    /**
     * 可用剩余容量
     */
    private Integer batteryRemaining;
    /**
     * 相对soc
     */
    private Short batteryRelativelyRemaining;

    public String getBatteryId() {
        return batteryId;
    }

    public void setBatteryId(String batteryId) {
        this.batteryId = batteryId;
    }

    public String getBatteryVer() {
        return batteryVer;
    }

    public void setBatteryVer(String batteryVer) {
        this.batteryVer = batteryVer;
    }

    public Integer getBatteryElectricity() {
        return batteryElectricity;
    }

    public void setBatteryElectricity(Integer batteryElectricity) {
        this.batteryElectricity = batteryElectricity;
    }

    public Integer getBatteryRemaining() {
        return batteryRemaining;
    }

    public void setBatteryRemaining(Integer batteryRemaining) {
        this.batteryRemaining = batteryRemaining;
    }

    public Short getBatteryRelativelyRemaining() {
        return batteryRelativelyRemaining;
    }

    public void setBatteryRelativelyRemaining(Short batteryRelativelyRemaining) {
        this.batteryRelativelyRemaining = batteryRelativelyRemaining;
    }

    public String getMachineNO() {
        return machineNO;
    }

    public void setMachineNO(String machineNO) {
        this.machineNO = machineNO;
    }

    public Date getDt() {
        return dt;
    }

    public void setDt(Date dt) {
        this.dt = dt;
    }

    public int getSoc() {
        return soc;
    }

    public void setSoc(int soc) {
        this.soc = soc;
    }

    public int getSoh() {
        return soh;
    }

    public void setSoh(int soh) {
        this.soh = soh;
    }

    public int getBatteryTemp() {
        return batteryTemp;
    }

    public void setBatteryTemp(int batteryTemp) {
        this.batteryTemp = batteryTemp;
    }

    public int getBatteryEI() {
        return batteryEI;
    }

    public void setBatteryEI(int batteryEI) {
        this.batteryEI = batteryEI;
    }

    public int getBatteryEU() {
        return batteryEU;
    }

    public void setBatteryEU(int batteryEU) {
        this.batteryEU = batteryEU;
    }

    public int getDischargeCnt() {
        return dischargeCnt;
    }

    public void setDischargeCnt(int dischargeCnt) {
        this.dischargeCnt = dischargeCnt;
    }

    public String getBatteryStatus() {
        return batteryStatus;
    }

    public void setBatteryStatus(String batteryStatus) {
        this.batteryStatus = batteryStatus;
    }

    public String getExData() {
        return exData;
    }

    public void setExData(String exData) {
        this.exData = exData;
    }

    public static TerBattery getDefTerbattery(String mno) {
        TerBattery temp = new TerBattery();
        temp.machineNO = mno;
        temp.dt = new Date();
        return temp;
    }

    @Override
    public String toString() {
        return "TerBattery{" +
                "machineNO='" + machineNO + '\'' +
                ", dt=" + dt +
                ", soc=" + soc +
                ", soh=" + soh +
                ", batteryTemp=" + batteryTemp +
                ", batteryEI=" + batteryEI +
                ", batteryEU=" + batteryEU +
                ", dischargeCnt=" + dischargeCnt +
                ", batteryStatus='" + batteryStatus + '\'' +
                ", exData='" + exData + '\'' +
                '}';
    }

    public boolean isNewBattery(TerBattery lastBattery) {
        return dt.after(lastBattery.dt);
    }

    /**
     * 克隆有效的值
     *
     * @param terBattery
     */
    public void clone(TerBattery terBattery) {
        this.machineNO = terBattery.getMachineNO();
        this.dt = terBattery.getDt();
        if (checkUpdate(this.soc, terBattery.getSoc())) {
            this.soc = terBattery.getSoc();
        }
        if (checkUpdate(this.soh, terBattery.getSoh())) {
            this.soh = terBattery.getSoh();
        }
        if (checkUpdate(this.batteryTemp, terBattery.getBatteryTemp())) {
            this.batteryTemp = terBattery.getBatteryTemp();
        }
        if (checkUpdate(this.batteryEI, terBattery.getBatteryEI())) {
            this.batteryEI = terBattery.getBatteryEI();
        }
        if (checkUpdate(this.batteryEU, terBattery.getBatteryEU())) {
            this.batteryEU = terBattery.getBatteryEU();
        }
        if (checkUpdate(this.dischargeCnt, terBattery.getDischargeCnt())) {
            this.dischargeCnt = terBattery.getDischargeCnt();
        }
        if (terBattery.getBatteryElectricity() != null) {
            this.batteryElectricity = terBattery.getBatteryElectricity();
        }
        if (terBattery.getBatteryRemaining() != null) {
            this.batteryRemaining = terBattery.getBatteryRemaining();
        }
        if (terBattery.getBatteryRelativelyRemaining() != null) {
            this.batteryRelativelyRemaining = terBattery.getBatteryRelativelyRemaining();
        }
        if (terBattery.getBatteryId() != null) {
            this.batteryId = terBattery.getBatteryId();
        }
        if (terBattery.getBatteryVer() != null) {
            this.batteryVer = terBattery.getBatteryVer();
        }
        // 最后做一次附加逻辑判定
        // 如果电量和剩余容量 及电压 都为0/65535，则认为电量和剩余容量都为0，如果 电量和剩余容量 为0，电压 不为0，则使用历史数据
        // 判定电压是否是合法值
        if (terBattery.getBatteryEU() == 0 && terBattery.getSoc() == 0 && terBattery.getBatteryRelativelyRemaining() == 0) {
            this.batteryEU = 0;
            this.soc = 0;
            this.batteryRelativelyRemaining = 0;
        }
    }

    private boolean checkUpdate(int oldValue, int newValue) {
        if (oldValue == -1) {
            return true;
        } else {
            return checkUpdate(newValue);
        }
    }

    private boolean checkUpdate(int value) {
        if (value == -1 || value == 0xffff || value == 0) {
            return false;
        } else {
            return true;
        }
    }
}
