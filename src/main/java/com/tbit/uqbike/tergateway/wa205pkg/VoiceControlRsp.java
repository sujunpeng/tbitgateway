package com.tbit.uqbike.tergateway.wa205pkg;

import com.tbit.uqbike.protocol.ATerPkg;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.tergateway.data.TerGatewayData;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import com.tbit.uqbike.tergateway.entity.RemoteControl;
import com.tbit.uqbike.tergateway.entity.TerTempData;
import com.tbit.uqbike.util.StringUtil;
import io.netty.buffer.ByteBuf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by MyWin on 2018/5/22 0022.
 */
public class VoiceControlRsp extends ATerPkg {
    private static Logger logger = LoggerFactory.getLogger(VoiceControlRsp.class);
    public String result;
    public String msgId;

    @Override
    public void doBusiness() {
        logger.debug(this.toString());

        TerTempData terTempData = TerGatewayData.getTerTempDataByMno(this.mno);
        Object obj = null;
        // 先根据流水号来
        if (!StringUtil.IsNullOrEmpty(this.msgId)) {
            obj = TerGatewayData.getStrSerNoMap(this.msgId);
        }
        if (null != obj) {
            if (obj instanceof RemoteControl) {
                RemoteControl remoteControl = (RemoteControl) obj;
                Integer code = Integer.parseInt(result);
                if (code == 200) {
                    code = 1;
                }
                remoteControl.buildTerRetRsp(code);
            }
        }
    }

    @Override
    public String toString() {
        return "VoiceControlRsp{" +
                "result='" + result + '\'' +
                ", msgId='" + msgId + '\'' +
                '}';
    }

    @Override
    public boolean autoRsp() {
        return false;
    }

    @Override
    public ByteBuf getRsp(AConnInfo info, AProtocol protocol) {
        return null;
    }
}
