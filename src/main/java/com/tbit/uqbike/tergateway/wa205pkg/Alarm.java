package com.tbit.uqbike.tergateway.wa205pkg;

import com.tbit.uqbike.TerGatewayMain;
import com.tbit.uqbike.protocol.ATerPkg;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.WA205DeEnCoder;
import com.tbit.uqbike.tergateway.config.TerGatewayConfig;
import com.tbit.uqbike.tergateway.data.TerGatewayData;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import com.tbit.uqbike.tergateway.pojo.TerAlarm;
import com.tbit.uqbike.util.ErrorCode;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.LinkedList;
import java.util.Objects;

/**
 * Created by MyWin on 2018/5/22 0022.
 */
public class Alarm extends ATerPkg {
    private static Logger logger = LoggerFactory.getLogger(Alarm.class);
    public String sn;
    public String lon;
    public String lat;
    public String azimuthState;
    public String safeStatus;
    public String batvol;
    public String alarm;


    @Override
    public void doBusiness() {
        TerAlarm terAlarm = getTerAlarm();
        if (null != terAlarm) {
            int code = TerGatewayMain.terEvnet.terRecvNewAlarm(terAlarm);
            if (code != ErrorCode.OK) {
                logger.info(String.format("由于:[%s]过滤掉告警信息:[%s]", ErrorCode.ErrorString(code), terAlarm.toString()));
            } else {
                // 插入历史轨迹
                TerGatewayData.addTerHisAlarm(terAlarm);
                // 推送给公共平台
                TerGatewayMain.getProducer().sendDataToQueue(TerGatewayConfig.alarmPushRouteKey, TerAlarm.getAlarmPushMsg(terAlarm));
            }
        }
    }

    @Override
    public boolean autoRsp() {
        return true;
    }

    @Override
    public ByteBuf getRsp(AConnInfo info, AProtocol protocol) {
        ByteBuf byteBuf = PooledByteBufAllocator.DEFAULT.directBuffer();
        WA205DeEnCoder.getCommRsp(byteBuf, "1");
        return byteBuf;
    }

    private TerAlarm getTerAlarm() {
        TerAlarm terAlarm = new TerAlarm();
        if (Objects.equals(this.alarm, "1")) {
            terAlarm.setAlarmType(1);
        } else if (Objects.equals(this.alarm, "2")) {
            terAlarm.setAlarmType(2);
        } else if (Objects.equals(this.alarm, "3")) {
            terAlarm.setAlarmType(3);
        } else if (Objects.equals(this.alarm, "4")) {
            terAlarm.setAlarmType(5);
        } else if (Objects.equals(this.alarm, "5")) {
            terAlarm.setAlarmType(0);
        } else if (Objects.equals(this.alarm, "6")) {
            terAlarm.setAlarmType(6);
        } else {
            logger.error(String.format("未知的告警类型:%s", this.toString()));
            return null;
        }
        terAlarm.setDt(new Date());
        terAlarm.setErrCode("");
        terAlarm.setMachineNO(this.mno);
        terAlarm.setAlarmExMsg("");
        terAlarm.setAlarmExCode(new LinkedList<Integer>());
        return terAlarm;
    }

    @Override
    public String toString() {
        return "Alarm{" +
                "mno='" + mno + '\'' +
                ", sn='" + sn + '\'' +
                ", lon='" + lon + '\'' +
                ", lat='" + lat + '\'' +
                ", azimuthState='" + azimuthState + '\'' +
                ", safeStatus='" + safeStatus + '\'' +
                ", batvol='" + batvol + '\'' +
                ", alarm='" + alarm + '\'' +
                '}';
    }
}
