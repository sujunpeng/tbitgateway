package com.tbit.uqbike.tergateway.wa205pkg;

import com.tbit.uqbike.protocol.ATerPkg;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.tergateway.data.TerGatewayData;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import com.tbit.uqbike.tergateway.entity.RemoteControl;
import com.tbit.uqbike.tergateway.entity.TerTempData;
import com.tbit.uqbike.util.ConstDefine;
import com.tbit.uqbike.util.StringUtil;
import io.netty.buffer.ByteBuf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by MyWin on 2018/5/22 0022.
 */
public class TerParamConfigRsp extends ATerPkg {
    private static Logger logger = LoggerFactory.getLogger(TerParamConfigRsp.class);
    public String msgId; //
    public String paramvalue;//查询有效
    public String paramret;//设置有效：1 表示成功， 0 表示失败

    @Override
    public void doBusiness() {
        logger.debug(this.toString());

        TerTempData terTempData = TerGatewayData.getTerTempDataByMno(this.mno);
        Object obj = null;
        // 先根据流水号来
        if (!StringUtil.IsNullOrEmpty(this.msgId)) {
            obj = TerGatewayData.getStrSerNoMap(this.msgId);
        }
        if (null != obj) {
            if (obj instanceof RemoteControl) {
                RemoteControl remoteControl = (RemoteControl) obj;
                // 区分查询和设置
                if (remoteControl.controlType.equals(ConstDefine.CONTROL_TYPE_GET)) {
                    if (paramvalue == null) {
                        paramvalue = String.format("%s=FAIL", remoteControl.paramName);
                    } else {
                        paramvalue = String.format("%s=%s", remoteControl.paramName, paramvalue);
                    }
                    remoteControl.buildTerKvRsp(paramvalue);
                } else if (remoteControl.controlType.equals(ConstDefine.CONTROL_TYPE_SET)) {
                    if ("1".equals(paramret)) {
                        paramret = String.format("%s=OK", remoteControl.paramName);
                    } else {
                        paramret = String.format("%s=FAIL", remoteControl.paramName);
                    }
                    remoteControl.buildTerKvRsp(paramret);
                }
            }
        }
    }

    @Override
    public String toString() {
        return "TerParamConfigRsp{" +
                "msgId='" + msgId + '\'' +
                ", paramvalue='" + paramvalue + '\'' +
                ", paramret='" + paramret + '\'' +
                '}';
    }

    @Override
    public boolean autoRsp() {
        return false;
    }

    @Override
    public ByteBuf getRsp(AConnInfo info, AProtocol protocol) {
        return null;
    }
}
