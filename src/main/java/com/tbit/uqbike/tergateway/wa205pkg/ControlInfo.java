package com.tbit.uqbike.tergateway.wa205pkg;

import com.tbit.uqbike.protocol.ATerPkg;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.WA205DeEnCoder;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by MyWin on 2018/5/22 0022.
 */
public class ControlInfo extends ATerPkg {
    private static Logger logger = LoggerFactory.getLogger(ControlInfo.class);
    public int time;
    public String sn;
    public int mileage;
    public int singlemileage;
    public int speed;
    public int error;
    public int vol;
    public int cur;
    public int bat;


    @Override
    public void doBusiness() {
        // 直接打出日志
        logger.debug(this.toString());
    }

    @Override
    public boolean autoRsp() {
        return true;
    }

    @Override
    public ByteBuf getRsp(AConnInfo info, AProtocol protocol) {
        ByteBuf byteBuf = PooledByteBufAllocator.DEFAULT.directBuffer();
        WA205DeEnCoder.getCommRsp(byteBuf, "1");
        return byteBuf;
    }

    @Override
    public String toString() {
        return "ControlInfo{" +
                "time=" + time +
                ", sn='" + sn + '\'' +
                ", mileage=" + mileage +
                ", singlemileage=" + singlemileage +
                ", speed=" + speed +
                ", mno='" + mno + '\'' +
                ", error=" + error +
                ", vol=" + vol +
                ", cur=" + cur +
                ", bat=" + bat +
                '}';
    }
}
