package com.tbit.uqbike.tergateway.event;

import com.tbit.uqbike.protocol.ABaseHandleObj;
import com.tbit.uqbike.protocol.ATerPkg;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import com.tbit.uqbike.tergateway.entity.SignalInfo;
import com.tbit.uqbike.tergateway.entity.TerTempData;
import com.tbit.uqbike.tergateway.pojo.TerAlarm;
import com.tbit.uqbike.tergateway.pojo.TerBattery;
import com.tbit.uqbike.tergateway.pojo.TerPos;
import com.tbit.uqbike.tergateway.wa206pkg.GetFirmware;
import io.netty.channel.ChannelHandlerContext;

import java.util.List;

/**
 * Created by MyWin on 2017/5/5.
 */
public interface ITerEvent {
    /**
     * 终端第一次收到合法的终端报文
     *
     * @param connId
     * @param mno
     */
    TerTempData TerRecvPkg(String connId, String mno, String protocolName);

    /**
     * 新链接接入
     *
     * @param ctx
     */
    void NewConnectComing(ChannelHandlerContext ctx);

    /**
     * 接收到数据
     *
     * @param ctx
     * @param msg
     */
    void channelRead(ChannelHandlerContext ctx, Object msg);

    void handleMsg(AConnInfo connInfo, ATerPkg msg);
    // 检测是否应该产生告警
    boolean interferenceSignalChcek(String sn, SignalInfo signalInfo);

    /**
     * 链接断开
     *
     * @param ctx
     */
    void channelInactive(ChannelHandlerContext ctx);

    /**
     * 终端收到新的位置
     *
     * @param terTempData
     * @param newPos
     * @return
     */
    int terRecvNewPos(TerTempData terTempData, TerPos newPos);

    /**
     * @param terTempData
     * @param newInfo
     * @return
     */
    int terRecvNewBattery(TerTempData terTempData, TerBattery newInfo);

    /**
     * @param newInfo
     * @return
     */
    int terRecvNewAlarm(TerAlarm newInfo);

    void handleBaseHandleObjs(List<ABaseHandleObj> aBaseHandleObjList);

    void handleBaseHandleObj(ABaseHandleObj aBaseHandleObj);

    void terUpdateCompete(GetFirmware getFirmware);
}
