package com.tbit.uqbike.tergateway.wa206pkg.extend;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019-8-19 10:05
 */
public class XiaomujiCalRet extends XiaomujiBase {
    @JSONField(name = "模型结果")
    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public XiaomujiCalRet(int value, String type, Integer len) {
        this.key = type;
        this.len = len;
        this.value = value;
    }
}
