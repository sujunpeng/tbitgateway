package com.tbit.uqbike.tergateway.wa206pkg;


import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.protocol.ATerPkg;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.WA206DeEnCoder;
import com.tbit.uqbike.tergateway.data.TerGatewayData;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import com.tbit.uqbike.tergateway.entity.RemoteControl;
import com.tbit.uqbike.tergateway.entity.TerTempData;
import com.tbit.uqbike.util.StringUtil;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;

import java.util.Date;
import java.util.Objects;

/**
 * Created by MyWin on 2017/4/27.
 */
public class RemoteControlRsp extends ATerPkg {
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(RemoteControlRsp.class);

    @JSONField(name = "时间", format = "yyyy-MM-dd HH:mm:ss")
    public Date dt;
    @JSONField(name = "控制结果")
    public int ret;
    @JSONField(name = "数字流水号")
    public int serNo;
    @JSONField(name = "字符流水号")
    public String serNoStr;

    @Override
    public String toString() {
        return "RemoteControlRsp{" +
                ", mno='" + mno + '\'' +
                ", head=" + head +
                ", dt=" + dt +
                ", ret=" + ret +
                ", serNo=" + serNo +
                ", serNoStr='" + serNoStr + '\'' +
                '}';
    }

    @Override
    public void doBusiness() {
        logger.info(this.toString());

        int serNo = head.getSerNo();
        TerTempData terTempData = null;
        if (null != this.mno && !this.mno.isEmpty()) {
            terTempData = TerGatewayData.getTerTempDataByMno(this.mno);
        }
        Object obj = null;
        // 先根据流水号来
        if (!StringUtil.IsNullOrEmpty(this.serNoStr)) {
            obj = TerGatewayData.getStrSerNoMap(this.serNoStr);
        } else {
            if (null != terTempData) {
                obj = terTempData.getSerNoAndDel(serNo);
            }
        }
        if (null != obj) {
            if (obj instanceof RemoteControl) {
                RemoteControl remoteControl = (RemoteControl) obj;
                reflushCacheStatus(remoteControl);
                remoteControl.buildTerRetRsp(ret);
            }
        }
    }

    /**
     * 有些状态返回太慢，此处收到应答，主动刷新状态
     */
    private void reflushCacheStatus(RemoteControl remoteControl) {
        if (Objects.equals(remoteControl.paramName, RemoteControl.CONTROL_TYPE_LOCK)) {

        } else if (Objects.equals(remoteControl.paramName, RemoteControl.CONTROL_TYPE_UNLOCK)) {

        } else if (Objects.equals(remoteControl.paramName, RemoteControl.CONTROL_TYPE_UNSTART)) {

        }
    }

    @Override
    public boolean autoRsp() {
        return true;
    }

    @Override
    public ByteBuf getRsp(AConnInfo info, AProtocol protocol) {
        WA206Head head206 = (WA206Head) this.head;
        if (head206.isWA206CVersion()) {
            ByteBuf rsp = PooledByteBufAllocator.DEFAULT.directBuffer();
            rsp.writeShort(WA206Head.StartCode); // 头
            rsp.writeShort(0);//长度
            rsp.writeByte(head206.version);//版本号
            rsp.writeByte(WA206Head.RemoteControlCmdRspRsp);//命令码
            rsp.writeByte(head206.serNo);//流水号
            rsp.writeMedium(head206.reserve);//保留字

            rsp.writeByte(1);

            rsp = WA206DeEnCoder.setLenAndCrc(rsp);
            return rsp;
        } else {
            return null;
        }
    }

    @Override
    public String getMachineNO() {
        return this.mno;
    }
}
