package com.tbit.uqbike.tergateway.wa206pkg;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;

import com.tbit.uqbike.protocol.ATerPkg;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.WA206DeEnCoder;

import com.tbit.uqbike.tergateway.entity.AConnInfo;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;

import java.util.Date;

/**
 * 兼容远程控制指令方式下 0xb6 方式上传的
 *
 * @author NING MEI
 * date: 2021/07/06
 */
public class RemoteControlRspTwo extends ATerPkg {

    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(RemoteControlRspTwo.class);

    public static String businessName = RemoteControlRspTwo.class.getName();

    @JSONField(name = "时间", format = "yyyy-MM-dd HH:mm:ss")
    public Date dt;
    @JSONField(name = "执行结果")
    public int ret;
    @JSONField(name = "扩展数据类型")                 //(0x00 默认类型 0x01 新版摄像头应答指令)
    public int extendType;

    @JSONField(name = "摄像头信息")
    public Camera camera;
    @JsonIgnore
    public boolean isCamera = false;
    @JSONField(name = "RFID信息")
    public RFID rfid;
    @JsonIgnore
    public boolean isrfid = false;
    @JSONField(name = "数字流水号")
    public int serNo;
    @JSONField(name = "字符流水号") // msgId
    public String serNoStr;


    @Override
    public void doBusiness() {
        logger.info("解析代码" + this.toString());
    }

    @Override
    public boolean autoRsp() {
        return true;
    }

    @Override
    public ByteBuf getRsp(AConnInfo conn, AProtocol protocol) {
        WA206Head head206 = (WA206Head) this.head;
        if (head206.isWA206CVersion()) {
            ByteBuf rsp = PooledByteBufAllocator.DEFAULT.directBuffer();
            rsp.writeShort(WA206Head.StartCode); // 头
            rsp.writeShort(0);//长度
            rsp.writeByte(head206.version);//版本号
            rsp.writeByte(WA206Head.RemoteControlCmdRspRsp);//命令码
            rsp.writeByte(head206.serNo);//流水号
            rsp.writeMedium(head206.reserve);//保留字
            rsp.writeByte(1);
            rsp = WA206DeEnCoder.setLenAndCrc(rsp);
            return rsp;
        } else {
            return null;
        }
    }

    @Override
    public String getMachineNO() {
        return this.mno;
    }

    @Override
    public String toString() {
        return "RemoteControlRspTwo{" +
                "dt=" + dt +
                ", ret=" + ret +
                ", camera=" + camera +
                ", rfid=" + rfid +
                ", serNo=" + serNo +
                ", serNoStr='" + serNoStr + '\'' +
                '}';
    }

}
