package com.tbit.uqbike.tergateway.wa206pkg;


import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.TerGatewayMain;
import com.tbit.uqbike.protocol.ATerPkg;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.WA206DeEnCoder;
import com.tbit.uqbike.tergateway.data.TerGatewayData;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import com.tbit.uqbike.tergateway.entity.TerTempData;
import com.tbit.uqbike.tergateway.pojo.TerBattery;
import com.tbit.uqbike.util.ConstDefine;
import com.tbit.uqbike.util.StringUtil;
import io.netty.buffer.ByteBuf;

import java.util.Date;

/**
 * Created by MyWin on 2017/4/27.
 */
public class BatteryInfo extends ATerPkg {

    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(BatteryInfo.class);

    @JSONField(name = "时间", format = "yyyy-MM-dd HH:mm:ss")
    public Date dt;
    @JSONField(name = "相对SOC", serialize = false)
    public int soc;
    @JSONField(name = "SOH")
    public int soh;
    @JSONField(name = "电池温度")
    public int batteryTemp;
    @JSONField(name = "电池电流")
    public int batteryEI;
    @JSONField(name = "电池电压")
    public int batteryEU;
    @JSONField(name = "充放电次数")
    public int dischargeCnt;
    @JSONField(name = "电池状态")
    public String batteryStatus;

    /**
     * 绝对满电容量
     */
    @JSONField(name = "绝对满电容量")
    public Integer batteryElectricity;
    /**
     * 可用剩余容量
     */
    @JSONField(name = "可用剩余容量")
    public Integer batteryRemaining;
    /**
     * 相对soc
     */
    @JSONField(name = "绝对SOC")
    public Short batteryRelativelyRemaining;
    @JSONField(name = "1～7节电池电压")
    public String power1_7;
    @JSONField(name = "8～14节电池电压")
    public String power8_14;
    @JSONField(name = "当前充电间隔时间")
    public Integer currChargeInv;
    @JSONField(name = "最大充电间隔时间")
    public Integer maxChargeInv;
    @JSONField(name = "读写成本条形码")
    public String codeStr;
    @JSONField(name = "读版本号")
    public String batteryVer;
    @JSONField(name = "电池组制造厂名称")
    public String factoryName;

    // 小木专用
    @JSONField(name = "2号温度")
    public Integer innerTemp2;
    @JSONField(name = "3号温度")
    public Integer innerTemp3;
    @JSONField(name = "4号温度")
    public Integer innerTemp4;
    @JSONField(name = "MOS温度")
    public Integer innerMosTemp;
    @JSONField(name = "湿度")
    public Integer innerMoisture;

    @Override
    public String toString() {
        return "BatteryInfo{" +
                "signPkg='" + signPkg + '\'' +
                ", mno='" + mno + '\'' +
                ", head=" + head +
                ", dt=" + dt +
                ", soc=" + soc +
                ", soh=" + soh +
                ", batteryTemp=" + batteryTemp +
                ", batteryEI=" + batteryEI +
                ", batteryEU=" + batteryEU +
                ", dischargeCnt=" + dischargeCnt +
                ", batteryStatus='" + batteryStatus + '\'' +
                '}';
    }

    @Override
    public void doBusiness() {
        logger.debug(this.toString());
        TerTempData terTempData = TerGatewayData.getTerTempDataByMno(this.mno);
        // 获取电量对象
        TerBattery newTerBattery = getTerBattery();
        TerGatewayMain.terEvnet.terRecvNewBattery(terTempData, newTerBattery);
    }

    @Override
    public boolean autoRsp() {
        return true;
    }

    public static String getBatterPushMsg(TerBattery batteryInfo) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("msgId", ConstDefine.MQ_MSG_ID_BATTERY_PUSH);
        jsonObject.put("feedback", StringUtil.Empty);
        jsonObject.put("data", batteryInfo);
        return jsonObject.toJSONString();
    }

    @Override
    public ByteBuf getRsp(AConnInfo info, AProtocol protocol) {
        return ((WA206DeEnCoder) protocol).getBatteryInfoCmd(info, this);
    }

    private TerBattery getTerBattery() {
        TerBattery terBattery = new TerBattery();
        terBattery.setBatteryEI(this.batteryEI);
        terBattery.setBatteryEU(this.batteryEU);
        terBattery.setBatteryStatus(this.batteryStatus);
        terBattery.setBatteryTemp(this.batteryTemp);
        terBattery.setDischargeCnt(this.dischargeCnt);
        terBattery.setDt(this.dt);
        terBattery.setExData(StringUtil.Empty);
        terBattery.setMachineNO(this.mno);
        terBattery.setSoc(this.soc);
        terBattery.setSoh(this.soh);
        if (this.batteryRemaining != null) {
            terBattery.setBatteryRemaining(batteryRemaining);
        }
        if (this.batteryRelativelyRemaining != null) {
            terBattery.setBatteryRelativelyRemaining(batteryRelativelyRemaining);
        }
        if (this.batteryElectricity != null) {
            terBattery.setBatteryElectricity(batteryElectricity);
        }
        terBattery.setBatteryId(codeStr);
        terBattery.setBatteryVer(batteryVer);
        return terBattery;
    }

    @Override
    public String getMachineNO() {
        return this.mno;
    }
}
