package com.tbit.uqbike.tergateway.wa206pkg.extend;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019-10-31 11:08
 */
public class CarStatusHelper {
    // key 第一个8位表示 起始位索引， 第二个8位表示，连续多少位,0表示连续1位
    // value null或者size为0，或者size不够，输出原始值，能索引到输出字符串
    public static List<AbstractMap.SimpleEntry<Integer, String[]>> bitNameList = Arrays.asList(
            new AbstractMap.SimpleEntry<Integer, String[]>(0x0000, new String[]{"借车", "还车"}),
            new AbstractMap.SimpleEntry<Integer, String[]>(0x0001, new String[]{"静止", "运动"}),
            new AbstractMap.SimpleEntry<Integer, String[]>(0x0002, new String[]{"电机加锁", "电机解锁"}),
            new AbstractMap.SimpleEntry<Integer, String[]>(0x0003, new String[]{"整车断电", "整车供电"}),
            new AbstractMap.SimpleEntry<Integer, String[]>(0x0104, new String[]{"未休眠", "半休眠", "全休眠"}),
            new AbstractMap.SimpleEntry<Integer, String[]>(0x0006, new String[]{"蓝牙未连接", "蓝牙连接"}),
            new AbstractMap.SimpleEntry<Integer, String[]>(0x0007, new String[]{"电池锁打开", "电池锁加锁"}),
            new AbstractMap.SimpleEntry<Integer, String[]>(0x0008, new String[]{"非骑行", "骑行"}),
            new AbstractMap.SimpleEntry<Integer, String[]>(0x0009, new String[]{"外电断开", "外电在位"}),
            new AbstractMap.SimpleEntry<Integer, String[]>(0x000A, new String[]{"转把失效", "转把有效"}),
            new AbstractMap.SimpleEntry<Integer, String[]>(0x000B, new String[]{"后轮加锁", "后轮解锁"}),
            new AbstractMap.SimpleEntry<Integer, String[]>(0x000C, new String[]{"非运营模式", "运营模式"}),
            new AbstractMap.SimpleEntry<Integer, String[]>(0x010D, new String[]{}),
            new AbstractMap.SimpleEntry<Integer, String[]>(0x000F, new String[]{"电池未充电", "电池充电"})
    );
}
