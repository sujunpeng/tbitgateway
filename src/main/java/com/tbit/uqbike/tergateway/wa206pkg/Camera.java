package com.tbit.uqbike.tergateway.wa206pkg;

import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.protocol.AnalyzeImpl.wa206.codedifine.CameraErrorCode;
import com.tbit.uqbike.protocol.AnalyzeImpl.wa206.codedifine.RemoteControllerRspRsCode;

/**
 * @author lzl
 * @date 2021/8/2 9:48
 * @Description 摄像头标识
 */
public class Camera {

    @JSONField(name = "执行结果")
    public int ret;
    @JSONField(name = "摄像头版本号")
    public String cameraVersion;
    @JSONField(name = "摄像头错误码")
    public int cameraErrorCode;
    @JSONField(name = "执行结果解析")
    public String retStr(){
        return RemoteControllerRspRsCode.getRsCodeString(ret);
    };
    @JSONField(name = "摄像头错误码解析")
    public String cameraErrorCodeStr(){
        return CameraErrorCode.getErrorCodeString(cameraErrorCode);
    }
    @JSONField(name = "识别相似度")                 //数字越大，则相似度也就越大
    public int similar;
    @JSONField(name = "图片识别角度")                //(未识别返回255)
    public long picDir;

    @Override
    public String toString() {
        return "Camera{" +
                "ret=" + ret +
                ", cameraVersion='" + cameraVersion + '\'' +
                ", cameraErrorCode=" + cameraErrorCode +
                ", similar=" + similar +
                ", picDir=" + picDir +
                '}';
    }
}
