package com.tbit.uqbike.tergateway.wa206pkg;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.TerGatewayMain;
import com.tbit.uqbike.protocol.ATerPkg;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.WA206DeEnCoder;
import com.tbit.uqbike.tergateway.config.TerGatewayConfig;
import com.tbit.uqbike.tergateway.data.TerGatewayData;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import com.tbit.uqbike.tergateway.pojo.TerAlarm;
import com.tbit.uqbike.util.ErrorCode;
import com.tbit.uqbike.util.StringUtil;
import io.netty.buffer.ByteBuf;

import java.util.Date;
import java.util.List;

/**
 * Created by MyWin on 2017/4/27.
 */
public class AlarmInfo extends ATerPkg {
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(AlarmInfo.class);

    @JSONField(name = "时间", format = "yyyy-MM-dd HH:mm:ss")
    public Date dt;
    @JSONField(name = "告警类型")
    public int alarmType;
    @JSONField(name = "故障扩展")
    public String alarmExMsg;//故障扩展码
    @JSONField(name = "错误码")
    public String errCode;
    @JSONField(serialize = false)// 不进行json序列化
    public byte[] errCodeBytes;
    @JSONField(name = "故障扩展码")
    public List<Integer> alarmExCode;

    @Override
    public String toString() {
        return "AlarmInfo{" +
                "signPkg='" + signPkg + '\'' +
                ", mno='" + mno + '\'' +
                ", head=" + head +
                ", dt=" + dt +
                ", alarmType=" + alarmType +
                ", errCode='" + errCode + '\'' +
                '}';
    }

    @Override
    public void doBusiness() {
        logger.debug(this.toString());
        // 先判断是否合法，不合法直接过滤掉不处理
        TerAlarm newInfo = getTerAlarm();
        int code = TerGatewayMain.terEvnet.terRecvNewAlarm(newInfo);
        if (code != ErrorCode.OK) {
            logger.info(String.format("由于:[%s]过滤掉告警信息:[%s]", ErrorCode.ErrorString(code), newInfo.toString()));
        } else {
            // 插入历史轨迹
            TerGatewayData.addTerHisAlarm(newInfo);
            if (needPush()) {
                // 推送给公共平台
                TerGatewayMain.getProducer().sendDataToQueue(TerGatewayConfig.alarmPushRouteKey, TerAlarm.getAlarmPushMsg(newInfo));
            }
        }
    }

    @Override
    public boolean autoRsp() {
        return true;
    }

    /**
     * 业务平台需要过滤一些告警
     * 只推送业务平台想要的告警
     *
     * @return
     */
    private boolean needPush() {
        if (null == errCodeBytes) {
            return true;
        }
        //
        if (alarmType != 8) {
            return true;
        }
        if (!StringUtil.IsNullOrEmpty(alarmExMsg)) {
            return true;
        }
        return false;
    }


    @Override
    public ByteBuf getRsp(AConnInfo info, AProtocol protocol) {
        return ((WA206DeEnCoder) protocol).getAlarmInfoRsp(info, this);
    }

    private TerAlarm getTerAlarm() {
        TerAlarm terAlarm = new TerAlarm();
        terAlarm.setAlarmType(this.alarmType);
        terAlarm.setDt(this.dt);
        terAlarm.setErrCode(this.errCode);
        terAlarm.setMachineNO(this.mno);
        terAlarm.setAlarmExMsg(this.alarmExMsg);
        terAlarm.setAlarmExCode(this.alarmExCode);
        return terAlarm;
    }

    @Override
    public String getMachineNO() {
        return this.mno;
    }

    public static void main(String[] args) {
        AlarmInfo alarmInfo = new AlarmInfo();
        alarmInfo.dt = new Date();
        logger.info(JSONObject.toJSONString(alarmInfo));
    }
}
