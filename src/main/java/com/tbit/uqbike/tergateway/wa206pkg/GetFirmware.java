package com.tbit.uqbike.tergateway.wa206pkg;


import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.TerGatewayMain;
import com.tbit.uqbike.protocol.ATerPkg;
import com.tbit.uqbike.protocol.AnalyzeImpl.AAutoProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.WA206DeEnCoder;
import com.tbit.uqbike.tergateway.data.TerGatewayData;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import com.tbit.uqbike.tergateway.entity.TerTempData;
import com.tbit.uqbike.util.TerPubUtil;
import io.netty.buffer.ByteBuf;

/**
 * Created by MyWin on 2017/4/27.
 */
public class GetFirmware extends ATerPkg {

    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(GetFirmware.class);
    @JSONField(name = "客户编码")
    public int customerCode;
    @JSONField(name = "硬件编码")
    public int hardwareModel;
    @JSONField(name = "版本号")
    public int version;
    @JSONField(name = "分块类型")
    public int blockType;
    @JSONField(name = "块号")
    public int blockNum;
    @JSONField(name = "固件类型")
    public int firmwareType;

    @Override
    public String toString() {
        return "GetFirmware{" +
                "signPkg='" + signPkg + '\'' +
                ", mno='" + mno + '\'' +
                ", head=" + head +
                ", customerCode=" + customerCode +
                ", hardwareModel=" + hardwareModel +
                ", version=" + version +
                ", firmwareType=" + firmwareType +
                ", blockType=" + blockType +
                ", blockNum=" + blockNum +
                '}';
    }

    @Override
    public void doBusiness() {
        logger.debug(this.toString());
        try {
            byte[] blockData = null;

            // 获取块信息
            blockData = TerGatewayData.getGjData(customerCode, hardwareModel, version, firmwareType, blockType, blockNum);
            if (null == blockData) {
                return;
            }
            // 为了修正终端bug 如果没有数据了就造一个数据包并且全设置为ff
            if (blockData.length == 0) {
                blockData = new byte[TerPubUtil.turnBlockIdToSize(blockType)];
                for (int i = 0; i < blockData.length; i++) {
                    blockData[i] = (byte) 0xff;
                }
            }
            logger.info(String.format("Down Ter [%d] Bin Pkg len:%d to mno:%s", blockNum, blockData.length, this.mno));
            TerTempData terTempData = TerGatewayData.getTerTempDataByMno(this.mno);
            if (null != terTempData) {
                AConnInfo info = terTempData.getConnInfo();
                AProtocol protocol = AAutoProtocol.getProtocol(terTempData.protocolName);
                if (info != null && null != protocol) {
                    // 再尝试构造下行消息
                    final ByteBuf byteBuf = ((WA206DeEnCoder) protocol).builtGetFirmwareRsp(info, this, blockData);
                    // 最后发送下行数据
                    if (byteBuf != null) {
                        info.downMsg(byteBuf);
                        // 如果发送完成了 且这个是最后一个数据包，那么就标志升级完成
                        if (blockData.length != TerPubUtil.turnBlockIdToSize(blockType)) {
                            TerGatewayMain.terEvnet.terUpdateCompete(this);
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("doBusiness", e);
        }
    }

    @Override
    public boolean autoRsp() {
        return false;
    }

    @Override
    public ByteBuf getRsp(AConnInfo info, AProtocol protocol) {
        return ((WA206DeEnCoder) protocol).getGetFirmwareRsp(info, this);
    }

    @Override
    public String getMachineNO() {
        return this.mno;
    }
}
