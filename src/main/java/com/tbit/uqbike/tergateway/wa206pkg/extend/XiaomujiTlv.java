package com.tbit.uqbike.tergateway.wa206pkg.extend;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019-8-15 15:13
 */
public class XiaomujiTlv extends XiaomujiBase {

    @JSONField(name = "VALUE")
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public XiaomujiTlv(String value, String type, Integer len) {
        this.key = type;
        this.len = len;
        this.value = value;
    }
}
