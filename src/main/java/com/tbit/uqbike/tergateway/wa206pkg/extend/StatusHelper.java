package com.tbit.uqbike.tergateway.wa206pkg.extend;

import com.tbit.utils.BitUtil;

import java.util.AbstractMap;
import java.util.List;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019-10-31 11:31
 */
public final class StatusHelper {
    /**
     * 解析状态
     *
     * @param status
     * @param kvs
     * @return
     */
    public static String analyzeStatus(Integer status, List<AbstractMap.SimpleEntry<Integer, String[]>> kvs) {
        StringBuilder sb = new StringBuilder();
        for (AbstractMap.SimpleEntry<Integer, String[]> kv : kvs) {
            Integer key = kv.getKey();
            int size = ( key >> 8) + 1;
            int index = kv.getKey() & 0xff;
            int value = BitUtil.between(status, index, index + size);
            String[] sStr = kv.getValue();
            if (null == sStr || value >= sStr.length) {
                sb.append(String.format("第%d-%d位:%d,", index, index + size - 1, value));
            } else {
                sb.append(String.format("%s,", sStr[value]));
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        analyzeStatus(14997, CarStatusHelper.bitNameList);
    }
}
