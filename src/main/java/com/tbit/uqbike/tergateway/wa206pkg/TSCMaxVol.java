package com.tbit.uqbike.tergateway.wa206pkg;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019/2/26 0026 11:13
 * 终端最大电压变化
 */
public class TSCMaxVol extends TerStatusChangeItem {
    @JSONField(name = "最大电压")
    public int maxVol;
}
