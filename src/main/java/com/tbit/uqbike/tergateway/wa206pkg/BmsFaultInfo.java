package com.tbit.uqbike.tergateway.wa206pkg;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.protocol.ATerPkg;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.WA206DeEnCoder;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import io.netty.buffer.ByteBuf;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019-10-29 11:21
 */
public class BmsFaultInfo extends ATerPkg {
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(AlarmInfo.class);

    @JSONField(name = "时间", format = "yyyy-MM-dd HH:mm:ss")
    public Date dt;
    @JSONField(name = "读写成本条形码")
    public String codeStr;
    @JSONField(name = "故障/恢复时间")
    public String dtStr;
    @JSONField(name = "总电压")
    public Long u;
    @JSONField(name = "总电流")
    public Integer i;
    @JSONField(name = "电池温度")
    public List<Integer> tempList = new LinkedList<>();
    @JSONField(name = "湿度")
    public Integer humidity;
    @JSONField(name = "单体电压")
    public List<Integer> uList = new LinkedList<>();

    @JSONField(name = "满电容量")
    public Integer batteryElectricity;
    /**
     * 可用剩余容量
     */
    @JSONField(name = "剩余容量")
    public Integer batteryRemaining;
    @JSONField(name = "充放电次数")
    public int dischargeCnt;
    @JSONField(name = "SOC")
    public int soc;
    @JSONField(name = "电池状态")
    public String batteryStatus;
    @JSONField(name = "电池状态")
    public String flag;

    @Override
    public void doBusiness() {
        logger.debug(JSONObject.toJSONString(this));
    }

    @Override
    public boolean autoRsp() {
        return true;
    }

    @Override
    public ByteBuf getRsp(AConnInfo conn, AProtocol protocol) {
        return ((WA206DeEnCoder) protocol).getBmsFaultInfoRsp(conn, this);
    }
}
