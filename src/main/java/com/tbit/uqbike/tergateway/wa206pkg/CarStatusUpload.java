package com.tbit.uqbike.tergateway.wa206pkg;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.protocol.ATerPkg;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.WA206DeEnCoder;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import com.tbit.uqbike.tergateway.entity.CarExData;
import io.netty.buffer.ByteBuf;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2018/11/22 0022 14:08
 */
public class CarStatusUpload extends ATerPkg {

    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(BatteryInfo.class);

    @JSONField(name = "时间", format = "yyyy-MM-dd HH:mm:ss")
    public Date dt;
    @JSONField(name = "持续采样时间")
    public int lastTime;
    @JSONField(serialize = false)
    public List<CarExData> exDataList = new LinkedList<>();
    @JSONField(name = "负载数据")
    public List<String> exDataShow = new LinkedList<>();

    @Override
    public void doBusiness() {
        logger.debug(JSONObject.toJSONString(this));
    }

    @Override
    public boolean autoRsp() {
        return true;
    }

    @Override
    public ByteBuf getRsp(AConnInfo conn, AProtocol protocol) {
        return ((WA206DeEnCoder) protocol).getCarStatusUploadRsp(conn, this);
    }
}
