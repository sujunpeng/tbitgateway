package com.tbit.uqbike.tergateway.wa206pkg;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019-7-17 18:58
 */
public class TerSpotItem {
    @JSONField(name = "埋点类型")
    public int spotType;
    @JSONField(name = "埋点数据长度")
    public int spotDataLen;
    @JSONField(name = "数据内容")
    public String spotContent;

    public TerSpotItem() {
    }

    public TerSpotItem(int spotType, int spotDataLen, String spotContent) {
        this.spotType = spotType;
        this.spotDataLen = spotDataLen;
        this.spotContent = spotContent;
    }
}
