package com.tbit.uqbike.tergateway.wa206pkg.extend;

import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.util.ProtocolUtil;
import com.tbit.utils.BitUtil;
import io.netty.buffer.ByteBuf;

import java.util.Date;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2020-02-15 9:36
 */
public class MuPos {
    @JSONField(name = "定位原因")
    public int pointReason;
    @JSONField(name = "时间", format = "yyyy-MM-dd HH:mm:ss")
    public Date dt;
    @JSONField(name = "经度")
    public double lon;
    @JSONField(name = "纬度")
    public double lat;
    @JSONField(name = "高度")
    public int high;
    @JSONField(name = "角度")
    public int dir;
    @JSONField(name = "速度")
    public int speed;
    @JSONField(name = "信号状态")
    public int signalStatus;
    @JSONField(name = "信号状态解析")
    public String getSignalStatusStr() {
        return String.format("定位标志:%d,GPS:%d,POWER:%d,GSM:%d",
                signalStatus & 3,
                BitUtil.between(signalStatus, 2, 6),
                BitUtil.between(signalStatus, 6, 10),
                BitUtil.between(signalStatus, 10, 14)
        );
    }


    public int initData(ByteBuf in, int i) {
        int len = 0;
        this.pointReason = in.getUnsignedByte(i + len);
        len += 1;
        this.dt = ProtocolUtil.BinGetTime(in, i + len);
        len += 4;
        this.lat = ProtocolUtil.BinGetLatLon(in, i + len);
        len += 4;
        this.lon = ProtocolUtil.BinGetLatLon(in, i + len);
        len += 4;
        this.high = in.getUnsignedShort(i + len);
        len += 2;
        this.dir = in.getUnsignedByte(i + len) * 2;
        len++;
        this.speed = in.getUnsignedByte(i + len);
        len++;
        this.signalStatus = in.getUnsignedShort(i + len);
        len += 2;
        return len;
    }
}
