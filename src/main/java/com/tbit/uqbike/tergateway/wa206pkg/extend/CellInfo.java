package com.tbit.uqbike.tergateway.wa206pkg.extend;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019/2/14 0014 10:25
 */
public class CellInfo {
    @JSONField(name = "国家码")
    public int mcc;
    @JSONField(name = "运营商码")
    public int mnc;
    @JSONField(name = "小区码")
    public int lac;
    @JSONField(name = "基站号")
    public int cellid;
    @JSONField(serialize = false)
    public int rssi;
    @JSONField(name = "信号强度")
    public String getRssiStr(){
        return String.format("-%ddBm", rssi);
    }
}
