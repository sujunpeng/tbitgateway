package com.tbit.uqbike.tergateway.wa206pkg.extend;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019/2/26 0026 10:10
 */
public class WifiInfo {
    @JSONField(name = "物理地址")
    public String apMAC;
    @JSONField(name = "信号强度")
    public int apLevel;
    @JSONField(name = "保留")
    public int reserved;
}
