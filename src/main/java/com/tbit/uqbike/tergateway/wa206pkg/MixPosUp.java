package com.tbit.uqbike.tergateway.wa206pkg;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.TerGatewayMain;
import com.tbit.uqbike.protocol.ATerPkg;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.MqAnalyzer;
import com.tbit.uqbike.protocol.AnalyzeImpl.WA206DeEnCoder;
import com.tbit.uqbike.tergateway.config.TerGatewayConfig;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import com.tbit.uqbike.tergateway.entity.AroundBle;
import com.tbit.uqbike.tergateway.wa206pkg.extend.BleInfo;
import com.tbit.uqbike.tergateway.wa206pkg.extend.CellInfo;
import com.tbit.uqbike.tergateway.wa206pkg.extend.WifiInfo;
import com.tbit.uqbike.util.ConstDefine;
import io.netty.buffer.ByteBuf;

import java.util.Date;
import java.util.List;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019/2/14 0014 10:13
 */
public class MixPosUp extends ATerPkg {
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(BatteryInfo.class);

    @JSONField(name = "时间", format = "yyyy-MM-dd HH:mm:ss")
    public Date dt;

    @JSONField(name = "经度")
    public double lon;
    @JSONField(name = "纬度")
    public double lat;
    @JSONField(name = "高度")
    public int high;
    @JSONField(name = "角度")
    public int dir;
    @JSONField(name = "速度")
    public int speed;
    @JSONField(name = "信号状态")
    public int signalStatus;

    @JSONField(name = "终端状态")
    public int terStatus;
    @JSONField(name = "电池电压")
    public int batteryVoltage;
    @JSONField(name = "备用电池电压")
    public int bakBatVoltage;
    @JSONField(name = "相对SOC")
    public int soc;
    @JSONField(name = "保留")
    public int reservedData;

    @JSONField(name = "基站列表")
    public List<CellInfo> cellInfoList;
    @JSONField(name = "WIFI列表")
    public List<WifiInfo> wifiInfoList;
    @JSONField(name = "BLE列表")
    public List<BleInfo> bleInfoList;

    @Override
    public void doBusiness() {
        logger.debug(JSONObject.toJSONString(this));
        // 如果有蓝牙列表数据需要透传给业务平台处理
        AroundBle aroundBle = getAroundBle();
        // 推送给公共平台
        String content = MqAnalyzer.buildPushMsg(aroundBle, ConstDefine.MQ_MSG_ID_AROUND_BLES_PUSH);
        TerGatewayMain.getProducer().sendDataToQueue(TerGatewayConfig.aroundBleInfoPushRouteKey, content);
        TerGatewayMain.getProducer().sendDataToQueue(TerGatewayConfig.aroundBleInfoPushMainRouteKey, content);

    }

    @Override
    public boolean autoRsp() {
        return true;
    }

    @Override
    public ByteBuf getRsp(AConnInfo conn, AProtocol protocol) {
        return ((WA206DeEnCoder) protocol).getMixPosUpRsp(conn, this);
    }

    private AroundBle getAroundBle() {
        AroundBle aroundBle = new AroundBle();

        aroundBle.mno = this.getMachineNO();
        for (BleInfo item : bleInfoList) {
            aroundBle.addBleNode(item);
        }

        return aroundBle;
    }
}
