package com.tbit.uqbike.tergateway.wa206pkg;


import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.protocol.ATerPkg;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.WA206DeEnCoder;
import com.tbit.uqbike.tergateway.data.TerGatewayData;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import com.tbit.uqbike.tergateway.entity.RemoteControl;
import com.tbit.uqbike.tergateway.entity.TerTempData;
import com.tbit.uqbike.util.StringUtil;
import io.netty.buffer.ByteBuf;

import java.util.Date;

/**
 * Created by MyWin on 2017/4/27.
 */
public class RemoteConVoiceRsp extends ATerPkg {

    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(RemoteConVoiceRsp.class);

    @JSONField(name = "时间", format = "yyyy-MM-dd HH:mm:ss")
    public Date dt;
    @JSONField(name = "控制结果")
    public int ret;
    @JSONField(name = "数字流水号")
    public int serNo;
    @JSONField(name = "字符流水号")
    public String serNoStr;

    @Override
    public String toString() {
        return "RemoteConVoiceRsp{" +
                "signPkg='" + signPkg + '\'' +
                ", mno='" + mno + '\'' +
                ", head=" + head +
                ", dt=" + dt +
                ", ret=" + ret +
                ", serNo=" + serNo +
                ", serNoStr='" + serNoStr + '\'' +
                '}';
    }

    @Override
    public void doBusiness() {
        logger.info(this.toString());
        int serNo = head.getSerNo();
        TerTempData terTempData = TerGatewayData.getTerTempDataByMno(this.mno);
        Object obj = null;
        // 先根据流水号来
        if (!StringUtil.IsNullOrEmpty(this.serNoStr)) {
            obj = TerGatewayData.getStrSerNoMap(this.serNoStr);
        } else {
            obj = terTempData.getSerNoAndDel(serNo);
        }
        if (null != obj) {
            if (obj instanceof RemoteControl) {
                RemoteControl remoteControl = (RemoteControl) obj;
                remoteControl.buildTerRetRsp(ret);
            }
        }
    }

    @Override
    public boolean autoRsp() {
        return false;
    }

    @Override
    public ByteBuf getRsp(AConnInfo info, AProtocol protocol) {
        return ((WA206DeEnCoder) protocol).getRemoteControlVoiceRspRsp(this);
    }

    @Override
    public String getMachineNO() {
        return this.mno;
    }
}
