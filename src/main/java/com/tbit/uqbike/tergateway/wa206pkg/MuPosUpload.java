package com.tbit.uqbike.tergateway.wa206pkg;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.TerGatewayMain;
import com.tbit.uqbike.protocol.ATerPkg;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.WA206DeEnCoder;
import com.tbit.uqbike.tergateway.config.TerGatewayConfig;
import com.tbit.uqbike.tergateway.data.TerGatewayData;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import com.tbit.uqbike.tergateway.entity.TerTempData;
import com.tbit.uqbike.tergateway.pojo.TerBattery;
import com.tbit.uqbike.tergateway.pojo.TerPos;
import com.tbit.uqbike.tergateway.wa206pkg.extend.CellInfo;
import com.tbit.uqbike.tergateway.wa206pkg.extend.MuPos;
import com.tbit.uqbike.util.ErrorCode;
import com.tbit.uqbike.util.StringUtil;
import io.netty.buffer.ByteBuf;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2020-02-15 9:33
 */
public class MuPosUpload  extends ATerPkg {

    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(MuPosUpload.class);

    @JSONField(name = "时间", format = "yyyy-MM-dd HH:mm:ss")
    public Date dt;
    @JSONField(name = "数据标志")
    public int dataFlag;
    @JSONField(name = "位置个数")
    public int posCount;
    @JSONField(name = "位置信息")
    public List<MuPos> posList = new LinkedList<>();
    @JSONField(name = "基站个数")
    public int cellCount;
    @JSONField(name = "基站信息")
    public List<CellInfo> cellList = new LinkedList<>();
    // 车辆信息
    @JSONField(name = "总里程")
    public int totalMile;
    @JSONField(name = "单次里程")
    public int signMile;
    @JSONField(name = "霍尔速度")
    public int hallSpeed;
    @JSONField(name = "终端状态")
    public int terStatus;
    @JSONField(name = "振动加速度")
    public int vibA;
    @JSONField(name = "偏向角")
    public int angle;
    @JSONField(name = "翻滚角")
    public int tumbleAngle;
    // 电池信息
    @JSONField(name = "电瓶电压")
    public int powEU;
    @JSONField(name = "相对SOC")
    public int soc;
    @JSONField(name = "可用剩余容量")
    public int batteryRemaining;
    @JSONField(name = "SOH")
    public int soh;
    @JSONField(name = "读写成本条形码")
    public String codeStr;

    @Override
    public void doBusiness() {
        logger.debug(JSONObject.toJSONString(this));
        if (TerGatewayConfig.bHandlePos) {
            TerTempData terTempData = TerGatewayData.getTerTempDataByMno(this.mno);
            for (MuPos item : posList){
                // 获取电量对象
                TerBattery newterBattery = getTerBattery(item);
                // 处理电量
                TerGatewayMain.terEvnet.terRecvNewBattery(terTempData, newterBattery);
                // 先判断是否合法，不合法直接过滤掉不处理
                TerPos newPos = getTerPos(item);
                int code = ErrorCode.OK;
                // 判断经纬度合法性
                if (!newPos.isLatlonOk()) {
                    code = ErrorCode.ErrorLngLatError;
                } else {
                    code = TerGatewayMain.terEvnet.terRecvNewPos(terTempData, newPos);
                    // 更新电压
                    terTempData.updateVol(this.dt, powEU);
                }
                if (code != ErrorCode.OK) {
                    logger.info(String.format("由于:[%s]过滤掉轨迹:[%s]", ErrorCode.ErrorString(code), newPos.toString()));
                }
            }
        }
    }

    private TerPos getTerPos(MuPos item) {
        TerPos terPos = new TerPos();
        terPos.setDir(item.dir);
        terPos.setDt(item.dt);
        terPos.setExData(StringUtil.Empty);
        terPos.setHigh(item.high);
        terPos.setMachineNO(this.mno);
        terPos.setSpeed(item.speed);
        terPos.setLat(item.lat);
        terPos.setLon(item.lon);
        terPos.setMile(this.signMile);
        terPos.setCarStatus(this.terStatus);

        terPos.signalStatus = item.signalStatus;
        terPos.signMile = this.signMile;
        terPos.hallSpeed = this.hallSpeed;
        terPos.wa206c = ((WA206Head) head).isWA206CVersion();
        terPos.lacId = "";
        terPos.cellId = "";

        int pointType = item.signalStatus & 0x3;
        terPos.setPointType(pointType);
        // 添加备注方法
        terPos.addExData("V", Integer.toString(powEU));
        return terPos;
    }

    private TerBattery getTerBattery(MuPos item) {
        TerBattery terBattery = new TerBattery();
        terBattery.setMachineNO(this.mno);
        terBattery.setDt(this.dt);
        terBattery.setBatteryEU(this.powEU);
        terBattery.setSoh(this.soh);
        terBattery.setSoc(this.soc);
        terBattery.setDischargeCnt(0);
        terBattery.setBatteryRemaining(batteryRemaining);
        terBattery.setBatteryRelativelyRemaining((short)0);
        return terBattery;
    }

    @Override
    public boolean autoRsp() {
        return true;
    }

    @Override
    public ByteBuf getRsp(AConnInfo conn, AProtocol protocol) {
        return WA206DeEnCoder.getCommRspPkg(conn, this, WA206Head.MuPosUploadCmdRsp);
    }
}
