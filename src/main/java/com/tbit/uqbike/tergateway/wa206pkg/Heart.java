package com.tbit.uqbike.tergateway.wa206pkg;


import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.TerGatewayMain;
import com.tbit.uqbike.protocol.ATerPkg;
import com.tbit.uqbike.protocol.AnalyzeImpl.AAutoProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.WA206DeEnCoder;
import com.tbit.uqbike.tergateway.data.TerGatewayData;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import com.tbit.uqbike.tergateway.entity.TerSoftInfo;
import com.tbit.uqbike.tergateway.entity.TerTempData;
import com.tbit.uqbike.tergateway.pojo.TerBattery;
import com.tbit.uqbike.tergateway.wa206pkg.extend.CarStatusHelper;
import com.tbit.uqbike.tergateway.wa206pkg.extend.StatusHelper;
import com.tbit.utils.BitUtil;
import io.netty.buffer.ByteBuf;

import java.util.Date;

/**
 * Created by MyWin on 2017/4/27.
 */
public class Heart extends ATerPkg {

    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Heart.class);

    @JSONField(name = "时间", format = "yyyy-MM-dd HH:mm:ss")
    public Date dt;
    @JSONField(name = "信号状态")
    public int signStatus;
    @JSONField(name = "终端状态")
    public int terStatus;

    @JSONField(name = "终端状态解析")
    public String getTerStatusStr() {
        return StatusHelper.analyzeStatus(terStatus, CarStatusHelper.bitNameList);
    }

    @JSONField(name = "信号状态解析")
    public String getSignalStatusStr() {
        return String.format("定位标志:%d,GPS:%d,POWER:%d,GSM:%d",
                signStatus & 3,
                BitUtil.between(signStatus, 2, 6),
                BitUtil.between(signStatus, 6, 10),
                BitUtil.between(signStatus, 10, 14)
        );
    }

    @JSONField(name = "电源电压")
    public int powerEU;
    @JSONField(name = "相对SOC", serialize = false)
    public int soc;

    /**
     * 可用剩余容量
     */
    @JSONField(name = "可用剩余容量")
    public Integer batteryRemaining;

    public Short batteryRelativelyRemaining;

    @Override
    public String toString() {
        return "Heart{" +
                "signPkg='" + signPkg + '\'' +
                ", mno='" + mno + '\'' +
                ", head=" + head +
                ", dt=" + dt +
                ", signStatus=" + signStatus +
                ", terStatus=" + terStatus +
                ", powerEU=" + powerEU +
                '}';
    }

    @Override
    public boolean autoRsp() {
        return false;
    }

    @Override
    public void doBusiness() {
        //logger.info(this.toString());
        // 更新最后状态
        // TerGatewayData.setTerCarStatus(this.mno, this.dt, this.terStatus, this.signStatus);

        TerTempData terTempData = TerGatewayData.getTerTempDataByMno(this.mno);
        // 更新最后状态
        terTempData.updateStatus(this.dt, this.terStatus, this.signStatus);
        // 更新电压
        terTempData.updateVol(this.dt, powerEU);
        // 获取电量对象
        TerBattery terBattery = getTerBattery();
        // 处理电量
        TerGatewayMain.terEvnet.terRecvNewBattery(terTempData, terBattery);

        AConnInfo conn = terTempData.getConnInfo();
        AProtocol protocol = AAutoProtocol.getProtocol(terTempData.protocolName);
        if (null != protocol && null != conn) {
            // 检测升级
            TerSoftInfo terSoftInfo = null;
            // 检测升级
            if (terTempData.getTerSoftInfoFlag()) {
                terSoftInfo = TerGatewayData.getTersoftwareKeyByMno(this.mno);
            }


            // 有值且不是默认值
            final ByteBuf byteBuf = ((WA206DeEnCoder) protocol).getHeartRspData(conn, this, terSoftInfo);
            // 最后发送下行数据
            if (byteBuf != null) {
                conn.downMsg(byteBuf);
            }
        }
        // 检查离线指令
        if (terTempData.getOfflineOrderFlag()) {
            terTempData.checkOfflineOrder();
        }
    }

    @Override
    public ByteBuf getRsp(AConnInfo info, AProtocol protocol) {
        return ((WA206DeEnCoder) protocol).getHeartRsp(info, this);
    }

    @Override
    public String getMachineNO() {
        return this.mno;
    }

    @JSONField(serialize = false)
    public TerBattery getTerBattery() {
        TerBattery bat = new TerBattery();
        bat.setMachineNO(this.mno);
        bat.setDt(this.dt);
        bat.setBatteryEU(this.powerEU);
        bat.setSoc(this.soc);
        bat.setBatteryRemaining(batteryRemaining);
        bat.setBatteryRelativelyRemaining(batteryRelativelyRemaining);
        return bat;
    }
}
