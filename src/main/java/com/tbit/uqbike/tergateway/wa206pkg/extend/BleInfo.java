package com.tbit.uqbike.tergateway.wa206pkg.extend;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019/2/26 0026 10:10
 */
public class BleInfo {
    @JSONField(name = "MAC地址")
    public String bleMAC;
    @JSONField(name = "BLE TID")
    public String bleTID;
    @JSONField(name = "信号强度")
    public int bleLevel;
    @JSONField(name = "节点类型")
    public Integer bleType;
    @JSONField(name = "硬件版本号")
    public Integer hdVer;
    @JSONField(name = "软件版本号")
    public Integer softVer;
    @JSONField(name = "状态信息")
    public int bleStatus;
}
