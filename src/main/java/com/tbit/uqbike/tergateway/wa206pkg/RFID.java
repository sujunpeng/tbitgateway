package com.tbit.uqbike.tergateway.wa206pkg;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author lzl
 * @date 2021/8/2 9:37
 * @Description NFC 类型   rfid 卡号，0~7字节：UID，8~11字节：站点ID
 */
public class RFID {
    @JSONField(name = "UID长度")
    public int UIDLength;
    @JSONField(name = "UID")
    public String UID;
    @JSONField(name = "siteId")
    public String siteId;
    @JSONField(name = "NFC控制类型")
    public int nfcControlType;    /** NFC 控制类型 */
    @JSONField(name = "操作计数")
    public long cnt;

    @Override
    public String toString() {
        return "RFID{" +
            "UIDLength=" + UIDLength +
            ", UID='" + UID + '\'' +
            ", siteId='" + siteId + '\'' +
            ", nfcControlType=" + nfcControlType +
            ", cnt=" + cnt +
            '}';
    }
}
