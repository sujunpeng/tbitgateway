package com.tbit.uqbike.tergateway.wa206pkg;


import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.protocol.ATerPkg;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.WA206DeEnCoder;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import com.tbit.uqbike.util.DateUtil;
import io.netty.buffer.ByteBuf;

import java.util.Date;

/**
 * Created by MyWin on 2017/4/27.
 */
public class SmsUpload extends ATerPkg {
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(SmsUpload.class);

    @JSONField(name = "时间", format = "yyyy-MM-dd HH:mm:ss")
    public Date dt;
    @JSONField(name = "号码")
    public String phone;
    @JSONField(name = "内容")
    public String content;
    @JSONField(name = "数字流水号")
    public int serNo;

    @Override
    public String toString() {
        return "SmsUpload{" +
                "signPkg='" + signPkg + '\'' +
                ", mno='" + mno + '\'' +
                ", head=" + head +
                ", dt=" + dt +
                ", phone='" + phone + '\'' +
                ", content='" + content + '\'' +
                ", serNo=" + serNo +
                '}';
    }

    @Override
    public void doBusiness() {
        logger.debug(this.toString());
        logger.info(String.format("[%s]于[%s]收到来自[%s]的短信[%s]", this.mno, DateUtil.ymdHmsDU.format(this.dt), this.phone, this.content));
    }

    @Override
    public boolean autoRsp() {
        return true;
    }

    @Override
    public ByteBuf getRsp(AConnInfo info, AProtocol protocol) {
        return ((WA206DeEnCoder) protocol).getSmsUploadRsp(info, this);
    }

    @Override
    public String getMachineNO() {
        return this.mno;
    }
}
