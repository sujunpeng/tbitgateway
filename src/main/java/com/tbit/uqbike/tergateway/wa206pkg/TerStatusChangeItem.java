package com.tbit.uqbike.tergateway.wa206pkg;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019/2/26 0026 11:11
 */
public abstract class TerStatusChangeItem {
    @JSONField(name = "状态类型")
    public int type;
    @JSONField(name = "状态长度")
    public int len;
}
