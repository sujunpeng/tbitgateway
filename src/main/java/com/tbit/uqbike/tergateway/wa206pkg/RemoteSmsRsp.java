package com.tbit.uqbike.tergateway.wa206pkg;


import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.protocol.ATerPkg;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.tergateway.data.TerGatewayData;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import com.tbit.uqbike.tergateway.entity.RemoteControl;
import com.tbit.uqbike.tergateway.entity.TerTempData;
import io.netty.buffer.ByteBuf;

import java.util.Date;

/**
 * Created by MyWin on 2017/4/27.
 */
public class RemoteSmsRsp extends ATerPkg {

    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(RemoteSmsRsp.class);

    @JSONField(name = "时间", format = "yyyy-MM-dd HH:mm:ss")
    public Date dt;
    @JSONField(name = "控制结果")
    public int ret;
    @JSONField(name = "数字流水号")
    public int serNo;

    @Override
    public String toString() {
        return "RemoteSmsRsp{" +
                "signPkg='" + signPkg + '\'' +
                ", mno='" + mno + '\'' +
                ", head=" + head +
                ", dt=" + dt +
                ", ret=" + ret +
                ", serNo=" + serNo +
                '}';
    }

    @Override
    public void doBusiness() {
        logger.debug(this.toString());
        int serNo = head.getSerNo();
        TerTempData terTempData = TerGatewayData.getTerTempDataByMno(this.mno);
        Object obj = terTempData.getSerNoAndDel(serNo);
        if (null != obj && obj instanceof RemoteControl) {
            RemoteControl remoteControl = (RemoteControl) obj;
            remoteControl.buildTerRetRsp(ret);
        }
    }

    @Override
    public boolean autoRsp() {
        return false;
    }

    @Override
    public ByteBuf getRsp(AConnInfo info, AProtocol protocol) {
        return null;
    }

    @Override
    public String getMachineNO() {
        return this.mno;
    }
}
