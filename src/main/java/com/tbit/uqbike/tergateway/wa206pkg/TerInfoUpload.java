package com.tbit.uqbike.tergateway.wa206pkg;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.TerGatewayMain;
import com.tbit.uqbike.protocol.ATerPkg;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.WA206DeEnCoder;
import com.tbit.uqbike.tergateway.config.TerGatewayConfig;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import com.tbit.uqbike.tergateway.entity.TerEventMsg;
import com.tbit.uqbike.util.ConstDefine;
import com.tbit.uqbike.util.StringUtil;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;

import java.util.Date;

/**
 * Created by MyWin on 2017/11/8 0008.
 */
public class TerInfoUpload extends ATerPkg {
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TerInfoUpload.class);

    @JSONField(name = "时间", format = "yyyy-MM-dd HH:mm:ss")
    public Date dt;
    @JSONField(name = "事件类型")
    public int eventType;
    @JSONField(name = "经度")
    public double lon;
    @JSONField(name = "纬度")
    public double lat;
    @JSONField(name = "高度")
    public int high;
    @JSONField(name = "角度")
    public int dir;
    @JSONField(name = "速度")
    public int speed;
    @JSONField(name = "信号状态")
    public int signalStatus;

    @JSONField(name = "国家码")
    public int mcc;
    @JSONField(name = "运营商码")
    public int mnc;
    @JSONField(name = "小区码")
    public int lac;
    @JSONField(name = "基站号")
    public int cellid;

    @JSONField(name = "相对SOC")
    public int soc;
    @JSONField(name = "电瓶电压")
    public int powEU;
    @JSONField(name = "总里程")
    public int totalMile;
    @JSONField(name = "单次里程")
    public int signMile;
    @JSONField(name = "终端状态")
    public int terStatus;

    @Override
    public void doBusiness() {
        logger.debug(this.toString());
        // 推送给公共平台
        TerGatewayMain.getProducer().sendDataToQueue(TerGatewayConfig.eventPushRouteKey, this.toEventPushMsg());
    }

    public TerEventMsg toTerEventMsg() {
        TerEventMsg msg = new TerEventMsg();
        msg.mno = this.mno;
        msg.dt = this.dt;
        msg.eventType = this.eventType;
        msg.lon = this.lon;
        msg.lat = this.lat;
        msg.high = this.high;
        msg.dir = this.dir;
        msg.speed = this.speed;
        msg.signalStatus = this.signalStatus;
        msg.mcc = this.mcc;
        msg.mnc = this.mnc;
        msg.lac = this.lac;
        msg.cellid = this.cellid;
        msg.soc = this.soc;
        msg.powEU = this.powEU;
        msg.totalMile = this.totalMile;
        msg.signMile = this.signMile;
        msg.terStatus = this.terStatus;
        return msg;
    }

    public String toEventPushMsg() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("msgId", ConstDefine.MQ_MSG_ID_EVENT_PUSH);
        jsonObject.put("feedback", StringUtil.Empty);
        jsonObject.put("data", this.toTerEventMsg());
        return jsonObject.toJSONString();
    }

    @Override
    public ByteBuf getRsp(AConnInfo info, AProtocol protocol) {
        WA206Head head206 = (WA206Head) this.head;
        ByteBuf rsp = PooledByteBufAllocator.DEFAULT.directBuffer();
        rsp.writeShort(WA206Head.StartCode); // 头
        rsp.writeShort(0);//长度
        rsp.writeByte(head206.version);//版本号
        rsp.writeByte(WA206Head.GetTerEventCmdRsp);//命令码
        rsp.writeByte(head206.serNo);//流水号
        rsp.writeMedium(head206.reserve);//保留字

        rsp.writeByte(1);
        rsp.writeByte(0);
        rsp.writeByte(0);

        rsp = WA206DeEnCoder.setLenAndCrc(rsp);
        return rsp;
    }

    @Override
    public boolean autoRsp() {
        return true;
    }

    @Override
    public String getMachineNO() {
        return this.mno;
    }

    @Override
    public String toString() {
        return "TerInfoUpload{" +
                "dt=" + dt +
                ", eventType=" + eventType +
                ", lon=" + lon +
                ", lat=" + lat +
                ", high=" + high +
                ", dir=" + dir +
                ", speed=" + speed +
                ", signalStatus=" + signalStatus +
                ", mcc=" + mcc +
                ", mnc=" + mnc +
                ", lac=" + lac +
                ", cellid=" + cellid +
                ", soc=" + soc +
                ", powEU=" + powEU +
                ", totalMile=" + totalMile +
                ", signMile=" + signMile +
                ", terStatus=" + terStatus +
                '}';
    }
}
