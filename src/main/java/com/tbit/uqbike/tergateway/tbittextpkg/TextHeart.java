package com.tbit.uqbike.tergateway.tbittextpkg;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.TerGatewayMain;
import com.tbit.uqbike.protocol.ATerPkg;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.TbitTextDeEnCoder;
import com.tbit.uqbike.tergateway.config.GateXmlConfig;
import com.tbit.uqbike.tergateway.config.TerGatewayConfig;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import com.tbit.uqbike.util.ConstDefine;
import com.tbit.uqbike.util.StringUtil;
import io.netty.buffer.ByteBuf;

import java.util.Date;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019/2/14 0014 15:18
 */
public class TextHeart extends ATerPkg {
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TextHeart.class);

    @JSONField(name = "信号强度")
    public String ggp;
    @JSONField(name = "电压")
    public String v;

    @Override
    public void doBusiness() {
        logger.debug(JSONObject.toJSONString(this));
        // 如果关心心跳记录在这里推送
        if (GateXmlConfig.pushPkgDt) {
            // 推送给公共平台
            TerGatewayMain.getProducer().sendDataToQueue(TerGatewayConfig.pkgDtRouteKey, getTerPkgDtMsg(mno));
        }
    }

    public static String getTerPkgDtMsg(String mno) {
        JSONObject data = new JSONObject();
        data.put("mno", mno);
        data.put("dt", new Date());

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("msgId", ConstDefine.MQ_MSG_ID_TER_PKG_DT_PUSH);
        jsonObject.put("feedback", StringUtil.Empty);
        jsonObject.put("data", data);
        return jsonObject.toJSONString();
    }

    @Override
    public boolean autoRsp() {
        return true;
    }

    @Override
    public ByteBuf getRsp(AConnInfo conn, AProtocol protocol) {
        return ((TbitTextDeEnCoder) protocol).buildCommRsp("S0");
    }
}
