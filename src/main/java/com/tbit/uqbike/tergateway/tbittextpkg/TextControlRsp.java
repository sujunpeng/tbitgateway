package com.tbit.uqbike.tergateway.tbittextpkg;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.protocol.ATerPkg;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.tergateway.data.TerGatewayData;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import com.tbit.uqbike.tergateway.entity.RemoteControl;
import com.tbit.uqbike.util.StringUtil;
import io.netty.buffer.ByteBuf;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019/2/14 0014 15:19
 */
public class TextControlRsp extends ATerPkg {
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TextControlRsp.class);

    @JSONField(name = "控制名称")
    public String controlName;
    @JSONField(name = "控制结果")
    public String rspContent;

    @Override
    public void doBusiness() {
        logger.info(JSONObject.toJSONString(this));
        String serNo = String.format("%s.%s", this.mno, controlName);
        Object obj = null;
        // 先根据流水号来
        if (!StringUtil.IsNullOrEmpty(serNo)) {
            obj = TerGatewayData.getStrSerNoMap(serNo);
        }
        if (null != obj) {
            if (obj instanceof RemoteControl) {
                RemoteControl remoteControl = (RemoteControl) obj;
                remoteControl.buildTerKvRsp(rspContent);
            }
        }
    }

    @Override
    public boolean autoRsp() {
        return false;
    }

    @Override
    public ByteBuf getRsp(AConnInfo conn, AProtocol protocol) {
        return null;
    }
}
