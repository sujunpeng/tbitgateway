package com.tbit.uqbike.tergateway.tbittextpkg;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.TerGatewayMain;
import com.tbit.uqbike.protocol.ATerPkg;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.TbitTextDeEnCoder;
import com.tbit.uqbike.service.ice.icebase.CellIdManage.DPoint;
import com.tbit.uqbike.service.ice.impl.CellIdManageImpl;
import com.tbit.uqbike.tergateway.config.GateXmlConfig;
import com.tbit.uqbike.tergateway.config.TerGatewayConfig;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import com.tbit.uqbike.tergateway.entity.SignalInfo;
import com.tbit.uqbike.util.ConstDefine;
import com.tbit.uqbike.util.StringUtil;
import io.netty.buffer.ByteBuf;

import java.util.Date;
import java.util.Objects;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019/2/14 0014 15:19
 */
public class TextPos extends ATerPkg {
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TextPos.class);

    @JSONField(name = "应答命令")
    public String rspCmd;
    @JSONField(name = "定位时间", format = "yyyy-MM-dd HH:mm:ss")
    public Date pointDt;
    @JSONField(name = "定位方式")
    public int pointType;
    @JSONField(name = "经度")
    public double lng;
    @JSONField(name = "纬度")
    public double lat;
    @JSONField(name = "速度")
    public int speed;
    @JSONField(name = "角度")
    public int direction;
    @JSONField(name = "状态")
    public String status;
    @JSONField(name = "告警")
    public String alarm;
    @JSONField(name = "基站")
    public String cellId;
    @JSONField(name = "信号强度")
    public String ggp;


    public static class TextTerPos {
        public String mno;
        public Date pointDt;
        public int pointType;
        public double lng;
        public double lat;
        public int speed;
        public int direction;
        public String status;
        public String alarm;
        public String cellId;
        public String ggp;

        public void addAlarm(Integer alarmCode) {
            if (null == alarm || alarm.length() == 0) {
                alarm = alarmCode.toString();
            } else {
                alarm = String.format("%s:%d", alarm, alarmCode);
            }
        }
    }

    @Override
    public void doBusiness() {
        logger.debug(JSONObject.toJSONString(this));
        TextTerPos pos = _getPos();
        if (GateXmlConfig.interferenceSignalFlag) {
            if (ggp != null && ggp.length() >= 3) {
                SignalInfo si = new SignalInfo();
                si.cellId = cellId;
                si.gps = Integer.parseInt(ggp.substring(0, 1));
                si.gsm = Integer.parseInt(ggp.substring(1, 2));
                if (TerGatewayMain.terEvnet.interferenceSignalChcek(this.mno, si)) {
                    pos.addAlarm(TextTerDef.ALARM_SIGNAL_INTERFERENCE);
                }
            }
        }
        // 推送给公共平台
        TerGatewayMain.getProducer().sendDataToQueue(TerGatewayConfig.posPushRouteKey, getPosPushMsg(pos));
    }

    public TextTerPos _getPos() {
        TextTerPos pos = new TextTerPos();
        pos.pointDt = this.pointDt;
        pos.mno = this.mno;
        pos.pointType = this.pointType;
        pos.lng = this.lng;
        pos.lat = this.lat;
        pos.speed = this.speed;
        pos.direction = this.direction;
        pos.status = this.status;
        pos.alarm = this.alarm;
        pos.cellId = this.cellId;
        pos.ggp = this.ggp;

        // 进行小区定位
        if (!Objects.equals(pos.pointType, ConstDefine.POINT_TYPE_BEIDOU) && !Objects.equals(pos.pointType, ConstDefine.POINT_TYPE_GPS)) {
            DPoint dPoint = null;
            try {
                dPoint = CellIdManageImpl.GetDpByCellId(cellId);
            } catch (Exception e) {
                logger.error("CellIdManageImpl.GetDpByCellId", e);
            }
            if (null == dPoint) {
                pointType = ConstDefine.POINT_TYPE_UNKNOW;
            } else {
                pointType = ConstDefine.POINT_TYPE_CELL;
                pos.lng = dPoint.x;
                pos.lat = dPoint.y;
            }
        }

        return pos;
    }

    public static String getPosPushMsg(TextTerPos pos) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("msgId", ConstDefine.MQ_MSG_ID_POS_PUSH);
        jsonObject.put("feedback", StringUtil.Empty);
        jsonObject.put("data", pos);
        return jsonObject.toJSONString();
    }

    @Override
    public boolean autoRsp() {
        return true;
    }

    @Override
    public ByteBuf getRsp(AConnInfo conn, AProtocol protocol) {
        return ((TbitTextDeEnCoder) protocol).buildCommRsp(rspCmd);
    }
}
