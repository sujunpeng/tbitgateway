package com.tbit.uqbike.tergateway.tbittextpkg;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019/2/14 0014 15:21
 */
public class TextTerDef {

    //状态字符串
    public static final int STATUS_LOCK = 0; //已设防
    public static final int STATUS_UNLOCK = 1; //未设防
    public static final int STATUS_SLEEP = 2; //休眠
    public static final int STATUS_POWEROFF = 3; //断电
    public static final int STATUS_MOVE = 4; //运动
    public static final int STATUS_ACCON = 5; //ACC开
    public static final int STATUS_DOORON = 6; //车门开
    public static final int STATUS_OILOFF = 7; //油门断
    public static final int STATUS_SWON = 8; //电门断
    public static final int STATUS_EMLOCK = 9; //电机锁

    public static final int STATUS_TOUCH = 10;      // 胶带已连接
    public static final int STATUS_UNTOUCH = 11;      // 胶带未连接
    public static final int STATUS_MAGNETIC_ON = 12;      // 门磁设备开
    public static final int STATUS_LOW_POWER_SLEEP = 13;        // 低电休眠

    //报警字符串
    public static final int ALARM_POWER_LACK = 0; //电量不足
    public static final int ALARM_CAR_MOVED = 1; //车辆被移动
    public static final int ALARM_SOS = 2; //SOS报警
    public static final int ALARM_DOOR_OPENED = 3; //车门打开报警
    public static final int ALARM_BATTERY_BREAK = 4; //主电池被断开
    public static final int ALARM_SHAKING = 5; //车辆震动
    public static final int ALARM_STOLEN = 6;  //失窃报警
    public static final int ALARM_DEVIANT = 7; //偏离路线
    public static final int ALARM_ENTER = 8;   //进入区域
    public static final int ALARM_LEAVE = 9;   //离开区域
    public static final int ALARM_STOP_TIMEOUT = 10; // 停车超时
    public static final int ALARM_SPEEDING = 11; //超速报警
    public static final int ALARM_ACCON = 12;    //ACC异常接通

    public static final int ALARM_IN_POINT = 13;    // 进入点
    public static final int ALARM_OUT_POINT = 14;   // 离开点
    public static final int ALARM_IN_LINE = 15;     // 进入线
    public static final int ALARM_OUT_LINE = 16;    // 离开线
    public static final int ALARM_IN_REGION = 17;   // 进入区域
    public static final int ALARM_OUT_REGION = 18;  // 离开区域

    public static final int ALARM_TOUCH_FAIL = 19;       // 胶带接触异常
    public static final int ALARM_TOUCH_BROKEN = 20;       // 胶带损毁

    public static final int ALARM_SYSTEM_STATUS = 21;       //电池开关以及系统开关告警

    public static final int ALARM_TEAR_DOWN = 23;  // 设备拆除告警
    public static final int ALARM_TER_OFFLINE_ALARM = 24;  // 离线告警
    public static final int ALARM_MAGNETIC_ON = 25;      // 门磁设备开
    public static final int ALARM_PROVINCE_REGION = 26; //省界提醒
    public static final int ALARM_POWER_OFF = 27; //关机报警
    public static final int ALARM_UN_BIND = 28; //未绑定
    public static final int ALARM_MEGATHERMAL = 29; //高温报警

    public static final int ALARM_SIGNAL_INTERFERENCE = 30; //终端干扰预警


    public static final String CONTROL_SF = "SF";
    public static final String CONTROL_CF = "CF";
    public static final String CONTROL_KY = "KY";
    public static final String CONTROL_DY = "DY";
    public static final String CONTROL_KY_DY_RSP = "KY_DY_RSP";
    public static final String CONTROL_SZCS = "SET";
    public static final String CONTROL_CQ = "CQ";
    public static final String CONTROL_DW = "DW";
    public static final String CONTROL_CXCS = "GET";
    public static final String CONTROL_SEND_SMS = "SEND_SMS";
    public static final String CONTROL_UPDATE_NOTICE = "UPDATE_NOTICE";
}
