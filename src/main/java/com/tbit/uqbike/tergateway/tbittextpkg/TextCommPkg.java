package com.tbit.uqbike.tergateway.tbittextpkg;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.protocol.ATerPkg;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.TbitTextDeEnCoder;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import io.netty.buffer.ByteBuf;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019/2/15 0015 8:47
 */
public class TextCommPkg extends ATerPkg {
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TextCommPkg.class);
    @JSONField(name = "应答命令")
    public String rspCmd;

    @Override
    public void doBusiness() {
        logger.debug(JSONObject.toJSONString(this));
    }

    @Override
    public boolean autoRsp() {
        return true;
    }

    @Override
    public ByteBuf getRsp(AConnInfo conn, AProtocol protocol) {
        return ((TbitTextDeEnCoder) protocol).buildCommRsp(rspCmd);
    }
}
