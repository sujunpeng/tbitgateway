package com.tbit.uqbike.tergateway.tbittextpkg;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.protocol.ATerPkg;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.protocol.AnalyzeImpl.TbitTextDeEnCoder;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import com.tbit.uqbike.util.DateUtil;
import io.netty.buffer.ByteBuf;

import java.util.Date;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019/2/14 0014 15:18
 */
public class TextTerLogin extends ATerPkg {
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TextTerLogin.class);

    @JSONField(name = "副卡号")
    public String simNO;                   // SIM卡号
    @JSONField(name = "车主号")
    public String ownerMobile;             // 车主号
    @JSONField(name = "密码")
    public String pwd;                     // 密码
    @JSONField(name = "登录原因")
    public String loginReason;             // 重新登录原因
    @JSONField(name = "IMSI")
    public String imsi;               // IMSI
    @JSONField(name = "IMEI")
    public String imei;              // IMEI
    @JSONField(name = "厂家")
    public String manufacture;        // 生产厂家

    @Override
    public void doBusiness() {
        logger.debug(JSONObject.toJSONString(this));
    }

    @Override
    public boolean autoRsp() {
        return true;
    }

    @Override
    public ByteBuf getRsp(AConnInfo conn, AProtocol protocol) {
        String rspPkgStr = String.format("[%s,S1,1]", DateUtil.ymdHmsDU.format(new Date()));
        return ((TbitTextDeEnCoder) protocol).buildPkg(rspPkgStr);
    }
}
