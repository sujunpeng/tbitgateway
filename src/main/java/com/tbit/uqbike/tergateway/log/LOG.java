package com.tbit.uqbike.tergateway.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by MyWin on 2018/3/22 0022.
 */
public class LOG {
    public static boolean bConnMnoLog = false;

    public static Logger NET = LoggerFactory.getLogger("TID_CONN_LOG");

    public boolean isbConnMnoLog() {
        return bConnMnoLog;
    }

    public void setbConnMnoLog(boolean bConnMnoLog) {
        LOG.bConnMnoLog = bConnMnoLog;
    }


    public static boolean bOnline = false;

    public boolean isbOnline() {
        return bOnline;
    }

    public void setbOnline(boolean bOnline) {
        LOG.bOnline = bOnline;
    }

    public static Logger ONLINE = LoggerFactory.getLogger("ONLINE_LOG");
}
