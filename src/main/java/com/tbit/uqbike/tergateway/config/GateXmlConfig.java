package com.tbit.uqbike.tergateway.config;

public class GateXmlConfig {
    public static String platform;
    public static String productKey;
    // 是否开启在线离线记录功能
    public static boolean onlineFlag = true;
    // 是否开启在线离线历史记录功能
    public static boolean onlineHis = true;
    // 是否推送终端消息时间
    public static boolean pushPkgDt = false;
    // 是否开启统计数据
    public static boolean statStatus = false;

    public static String statMqHost;
    public static Integer statMqPort;
    public static String statMqExchange;
    public static String statMqUsername;
    public static String statMqPassword;
    // 轨迹过滤开关
    public static boolean bPosFilter = true;
    // 静态飘逸过滤的距离阈值
    public static Integer stopFilterDis = 50;
    public static Integer stopFilterMinDis = 1000;
    // 扩展过滤开关
    public static Integer cellMaxDis = 2000;
    public static Integer lacMaxDis = 35000;
    public static Integer stopFilterMinSec = 20;
    public static Integer maxAvgSpeed = 200;
    // filter轨迹入库
    public static boolean filterPosInsertDb = false;
    // 默认支持基站定位
    public static boolean supportCellDw = true;
    // 是否开启信号干扰预警
    public static boolean interferenceSignalFlag = false;

    public boolean isInterferenceSignalFlag() {
        return interferenceSignalFlag;
    }

    public void setInterferenceSignalFlag(boolean interferenceSignalFlag) {
        interferenceSignalFlag = interferenceSignalFlag;
    }

    public boolean isSupportCellDw() {
        return supportCellDw;
    }

    public void setSupportCellDw(boolean supportCellDw) {
        GateXmlConfig.supportCellDw = supportCellDw;
    }

    public boolean isFilterPosInsertDb() {
        return filterPosInsertDb;
    }

    public void setFilterPosInsertDb(boolean filterPosInsertDb) {
        GateXmlConfig.filterPosInsertDb = filterPosInsertDb;
    }

    public Integer getCellMaxDis() {
        return cellMaxDis;
    }

    public void setCellMaxDis(Integer cellMaxDis) {
        GateXmlConfig.cellMaxDis = cellMaxDis;
    }

    public Integer getLacMaxDis() {
        return lacMaxDis;
    }

    public void setLacMaxDis(Integer lacMaxDis) {
        GateXmlConfig.lacMaxDis = lacMaxDis;
    }

    public Integer getStopFilterMinSec() {
        return stopFilterMinSec;
    }

    public void setStopFilterMinSec(Integer stopFilterMinSec) {
        GateXmlConfig.stopFilterMinSec = stopFilterMinSec;
    }

    public Integer getMaxAvgSpeed() {
        return maxAvgSpeed;
    }

    public void setMaxAvgSpeed(Integer maxAvgSpeed) {
        GateXmlConfig.maxAvgSpeed = maxAvgSpeed;
    }

    public Integer getStopFilterDis() {
        return stopFilterDis;
    }

    public void setStopFilterDis(Integer stopFilterDis) {
        GateXmlConfig.stopFilterDis = stopFilterDis;
    }

    public Integer getStopFilterMinDis() {
        return stopFilterMinDis;
    }

    public void setStopFilterMinDis(Integer stopFilterMinDis) {
        GateXmlConfig.stopFilterMinDis = stopFilterMinDis;
    }

    public boolean isbPosFilter() {
        return bPosFilter;
    }

    public void setbPosFilter(boolean bPosFilter) {
        GateXmlConfig.bPosFilter = bPosFilter;
    }

    public String getStatMqHost() {
        return statMqHost;
    }

    public void setStatMqHost(String statMqHost) {
        GateXmlConfig.statMqHost = statMqHost;
    }

    public Integer getStatMqPort() {
        return statMqPort;
    }

    public void setStatMqPort(Integer statMqPort) {
        GateXmlConfig.statMqPort = statMqPort;
    }

    public String getStatMqExchange() {
        return statMqExchange;
    }

    public void setStatMqExchange(String statMqExchange) {
        GateXmlConfig.statMqExchange = statMqExchange;
    }

    public String getStatMqUsername() {
        return statMqUsername;
    }

    public void setStatMqUsername(String statMqUsername) {
        GateXmlConfig.statMqUsername = statMqUsername;
    }

    public String getStatMqPassword() {
        return statMqPassword;
    }

    public void setStatMqPassword(String statMqPassword) {
        GateXmlConfig.statMqPassword = statMqPassword;
    }

    public boolean isStatStatus() {
        return statStatus;
    }

    public void setStatStatus(boolean statStatus) {
        GateXmlConfig.statStatus = statStatus;
    }

    public boolean isPushPkgDt() {
        return pushPkgDt;
    }

    public void setPushPkgDt(boolean pushPkgDt) {
        GateXmlConfig.pushPkgDt = pushPkgDt;
    }

    public boolean isOnlineFlag() {
        return onlineFlag;
    }

    public void setOnlineFlag(boolean onlineFlag) {
        GateXmlConfig.onlineFlag = onlineFlag;
    }

    public boolean isOnlineHis() {
        return onlineHis;
    }

    public void setOnlineHis(boolean onlineHis) {
        GateXmlConfig.onlineHis = onlineHis;
    }

    public String getProductKey() {
        return productKey;
    }

    public void setProductKey(String productKey) {
        GateXmlConfig.productKey = productKey;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        GateXmlConfig.platform = platform;
    }
}
