package com.tbit.uqbike.tergateway.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.UUID;

/**
 * Created by MyWin on 2017/4/28.
 */
public class TerGatewayConfig {

    private static Logger logger = LoggerFactory.getLogger(TerGatewayConfig.class);

    public static final String configFileName = "/tergatewayconfig.properties";

    // 业务相关配置，定义在导入之前，这样如果没有配置文件也会有一个初始值
    public static Boolean bHandlePos = false;

    public static String moduleName = "project.module.other";

    public static Integer iCacheBatWriteInvSec = 15;

    public static Integer iCacheBatReadInvSec = 15;

    public static Integer iFirstRunCacheBatWriteWaitSec = 30;
    // 离线终端检测间隔
    public static Integer iOfflineTerCheckIvnMin = 5;
    // 超过多少个小时没有上线的设备从内存中删除
    public static Integer iDelMemTerObjTimeOutHour = 12;

    //
    public static Integer iDbBatSize = 2000;
    public static Integer iRedisBatSize = 2000;

    public static int TcpCutSec = 300;
    public static String TcpListenIp = "0.0.0.0";
    public static int TcpListenPort;
    public static double DefaultPosLon = 116.404844;
    public static double DefaultPosLat = 39.91582;
    public static boolean BopenPkgLog = true;

    public static int OrderTimeOutCheckThreadPool = 3;
    public static int OrderTimeOutMin = 10;
    public static int MaxHisPosSec = 60 * 10;
    public static int MaxBatterySec = 60 * 10;
    public static int MixBatterySec = -30 * 24 * 60 * 60;
    public static int MaxAlarmSec = 60 * 10;
    /**
     * 高频数据延迟更新时间间隔 sec
     */
    public static int DelayUpdateInv = 20;
    /**
     * 高频数据延迟更新检测间隔 sec
     */
    public static int DelayUpdateCheckInv = 10;
    /**
     * 最后位置是否必须为精确定位
     */
    public static boolean LastPosMustSatellite = true;

    public static String routeKey;

    public static String alarmPushRouteKey = "uqAlarmPush";

    public static String terStatusChange = "uqTerStatusChangePush";

    public static String batteryPushRouteKey = "tbit_uqbike_ter_battery";

    public static String posPushRouteKey = "tbit_uqbike_ter_position";

    public static String pkgDtRouteKey = "ter_pkg_dt";

    public static String aroundBleInfoPushRouteKey = "tbit_uqbike_ter_around_ble";

    /**
     * 发送给运维端的queue
     */
    public static String aroundBleInfoPushMainRouteKey = "tbit_uqbike_ter_main_around_ble";


    public static String eventPushRouteKey = "tbit_uqbike_ter_event";

    public static String orderDataStatKey = "order_data";

    public static String pointDataStatKey = "point_data";

    public static String gatewayNameIdent = "";

    static {
        String configFilePath = TerGatewayConfig.class.getResource(TerGatewayConfig.configFileName).getPath();
        Properties pro = LoadConfig(configFilePath);
        if (pro != null) {
            try {
                SaveConfig(pro, configFilePath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        InitConfig();
        configFilePath = TerGatewayConfig.class.getResource("/rabbitmq-config.properties").getPath();
        LoadMqConfig(configFilePath);
    }

    private static Properties LoadConfig(String configFilePath) {
        FileInputStream in = null;
        Properties pro = null;
        try {
            pro = new Properties();
            in = new FileInputStream(configFilePath);
            pro.load(in);
            in.close();
            in = null;

            // 赋值参数
            if (pro.containsKey("TcpCutSec")) {
                TerGatewayConfig.TcpCutSec = Integer.parseInt(pro.getProperty("TcpCutSec"));
            } else {
                pro.put("TcpCutSec", Integer.toString(TerGatewayConfig.TcpCutSec));
            }

            if (pro.containsKey("iDbBatSize")) {
                TerGatewayConfig.iDbBatSize = Integer.parseInt(pro.getProperty("iDbBatSize"));
            } else {
                pro.put("iDbBatSize", TerGatewayConfig.iDbBatSize.toString());
            }

            if (pro.containsKey("iRedisBatSize")) {
                TerGatewayConfig.iRedisBatSize = Integer.parseInt(pro.getProperty("iRedisBatSize"));
            } else {
                pro.put("iRedisBatSize", TerGatewayConfig.iRedisBatSize.toString());
            }

            if (pro.containsKey("iCacheBatWriteInvSec")) {
                TerGatewayConfig.iCacheBatWriteInvSec = Integer.parseInt(pro.getProperty("iCacheBatWriteInvSec"));
            } else {
                pro.put("iCacheBatWriteInvSec", TerGatewayConfig.iCacheBatWriteInvSec.toString());
            }

            if (pro.containsKey("iCacheBatReadInvSec")) {
                TerGatewayConfig.iCacheBatReadInvSec = Integer.parseInt(pro.getProperty("iCacheBatReadInvSec"));
            } else {
                pro.put("iCacheBatReadInvSec", TerGatewayConfig.iCacheBatReadInvSec.toString());
            }

            if (pro.containsKey("iFirstRunCacheBatWriteWaitSec")) {
                TerGatewayConfig.iFirstRunCacheBatWriteWaitSec = Integer.parseInt(pro.getProperty("iFirstRunCacheBatWriteWaitSec"));
            } else {
                pro.put("iFirstRunCacheBatWriteWaitSec", TerGatewayConfig.iFirstRunCacheBatWriteWaitSec.toString());
            }

            if (pro.containsKey("TcpListenPort")) {
                TerGatewayConfig.TcpListenPort = Integer.parseInt(pro.getProperty("TcpListenPort"));
            } else {
                pro.put("TcpListenPort", Integer.toString(TerGatewayConfig.TcpListenPort));
            }

            if (pro.containsKey("BopenPkgLog")) {
                TerGatewayConfig.BopenPkgLog = Boolean.parseBoolean(pro.getProperty("BopenPkgLog"));
            } else {
                pro.put("BopenPkgLog", Boolean.toString(TerGatewayConfig.BopenPkgLog));
            }

            if (pro.containsKey("bHandlePos")) {
                TerGatewayConfig.bHandlePos = Boolean.parseBoolean(pro.getProperty("bHandlePos"));
            } else {
                pro.put("bHandlePos", TerGatewayConfig.bHandlePos.toString());
            }

            if (pro.containsKey("moduleName")) {
                TerGatewayConfig.moduleName = pro.getProperty("moduleName");
            } else {
                pro.put("moduleName", TerGatewayConfig.moduleName);
            }

            if (pro.containsKey("gatewayNameIdent")) {
                TerGatewayConfig.gatewayNameIdent = pro.getProperty("gatewayNameIdent");
            }
            if (TerGatewayConfig.gatewayNameIdent == null || TerGatewayConfig.gatewayNameIdent.isEmpty()) {
                TerGatewayConfig.gatewayNameIdent = UUID.randomUUID().toString();
                pro.put("gatewayNameIdent", TerGatewayConfig.gatewayNameIdent);
            }

            if (pro.containsKey("MaxHisPosSec")) {
                TerGatewayConfig.MaxHisPosSec = Integer.parseInt(pro.getProperty("MaxHisPosSec"));
            } else {
                pro.put("MaxHisPosSec", Integer.toString(TerGatewayConfig.MaxHisPosSec));
            }
            if (pro.containsKey("MaxAlarmSec")) {
                TerGatewayConfig.MaxAlarmSec = Integer.parseInt(pro.getProperty("MaxAlarmSec"));
            } else {
                pro.put("MaxAlarmSec", Integer.toString(TerGatewayConfig.MaxAlarmSec));
            }

            if (pro.containsKey("OrderTimeOutMin")) {
                TerGatewayConfig.OrderTimeOutMin = Integer.parseInt(pro.getProperty("OrderTimeOutMin"));
            } else {
                pro.put("OrderTimeOutMin", Integer.toString(TerGatewayConfig.OrderTimeOutMin));
            }


        } catch (Exception e) {
            logger.error("TerGatewayConfig Con.", e);
        } finally {
            if (null != in) {
                try {
                    in.close();
                } catch (IOException e) {
                    logger.error("TerGatewayConfig Con.", e);
                }
            }
        }
        return pro;
    }

    private static void LoadMqConfig(String configFilePath) {
        FileInputStream in = null;
        Properties pro = null;
        try {
            pro = new Properties();
            in = new FileInputStream(configFilePath);
            pro.load(in);
            in.close();
            in = null;

            if (pro.containsKey("mq.key")) {
                TerGatewayConfig.routeKey = pro.getProperty("mq.key");
            }
        } catch (Exception e) {
            logger.error("TerGatewayConfig Con.", e);
        } finally {
            if (null != in) {
                try {
                    in.close();
                } catch (IOException e) {
                    logger.error("TerGatewayConfig Con.", e);
                }
            }
        }
    }

    private static void SaveConfig(Properties pro, String configFilePath) throws IOException {
        FileOutputStream oFile = new FileOutputStream(configFilePath);
        pro.store(oFile, "TerGateway TerGatewayConfig");
        oFile.close();
    }

    private static void InitConfig() {

    }
}
