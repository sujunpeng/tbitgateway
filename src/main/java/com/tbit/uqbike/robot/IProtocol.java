package com.tbit.uqbike.robot;

import java.util.List;

/**
 * Created by MyWin on 2018/3/7 0007.
 */
public interface IProtocol {
    // 构建包
    byte[] getPkg(VirTer ter, TerEvent event);

    // 流转包
    List<byte[]> spiltStream(VirTer ter, byte[] data);

    // 包处理
    void handlePkg(VirTer ter, byte[] pkg);
}
