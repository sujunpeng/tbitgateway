package com.tbit.uqbike.robot;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by MyWin on 2018/3/7 0007.
 */
public class StaticHelper {
    private AtomicLong pkgUpCnt;
    private AtomicLong pkgRspCnt;

    public StaticHelper() {
        pkgUpCnt = new AtomicLong();
        pkgRspCnt = new AtomicLong();
    }

    public void SendCount() {
        pkgUpCnt.incrementAndGet();
    }

    public void RspCount() {
        pkgRspCnt.incrementAndGet();
    }
}
