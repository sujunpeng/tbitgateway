package com.tbit.uqbike.robot.net.aio.client;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.concurrent.CountDownLatch;

public class WriteHandler implements CompletionHandler<Integer, ByteBuffer> {
    private AsyncClientHandler client;
    private long writeByteCnt;

    public WriteHandler(AsyncClientHandler client) {
        this.client = client;
    }

    @Override
    public void completed(Integer result, ByteBuffer buffer) {
        writeByteCnt += result;
        TcpClientTest.sendCnt.addAndGet(writeByteCnt / 26);
        writeByteCnt = writeByteCnt % 26;
        //System.out.println(String.format("连接:%s发送:%d字节", client.getClientId(), result));
    }

    @Override
    public void failed(Throwable exc, ByteBuffer attachment) {
        client.closeConnect();
    }
}