package com.tbit.uqbike.robot.net;

import com.tbit.uqbike.robot.VirTer;

/**
 * Created by MyWin on 2018/3/7 0007.
 */
public interface INetFactory {
    INetFun createNet(VirTer ter);
}
