package com.tbit.uqbike.robot.net;

import com.tbit.uqbike.robot.net.aio.client.AsyncClientHandler;
import com.tbit.uqbike.robot.net.aio.client.ReadHandler;
import com.tbit.uqbike.util.ByteUtil;
import com.tbit.uqbike.util.CharsetName;

import java.io.UnsupportedEncodingException;

/**
 * Created by MyWin on 2018/3/8 0008.
 */
public class Wa206Handler extends ReadHandler {
    public Wa206Handler(AsyncClientHandler client) {
        super(client);
    }

    @Override
    public void handleRecv(byte[] data) {
        //System.out.println(String.format("[%s]Recv:[%s]", client.getClientId(), ByteUtil.BytesToHexString(data)));
    }
}
