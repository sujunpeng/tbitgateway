package com.tbit.uqbike.robot.net;

import com.tbit.uqbike.robot.VirTer;
import com.tbit.uqbike.robot.manager.VirTerManager;
import com.tbit.uqbike.robot.net.INetFun;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MyWin on 2018/3/7 0007.
 */
public class TcpClient implements INetFun {
    private VirTer ter;
    private ArrayList<Byte> buffer;

    public TcpClient(VirTer ter) {
        this.ter = ter;
        this.buffer = new ArrayList<>();
    }

    @Override
    public void sendData(byte[] data) {
        VirTerManager.stat.SendCount();
    }
}
