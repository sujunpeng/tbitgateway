package com.tbit.uqbike.robot.net.aio.client;

/**
 * Created by MyWin on 2018/3/8 0008.
 */
public enum TcpClientStatus {
    Normal,
    Connecting,
    Connected
}
