package com.tbit.uqbike.robot.net.aio.client;

import com.tbit.uqbike.robot.TerEvent;
import com.tbit.uqbike.robot.VirTer;
import com.tbit.uqbike.robot.manager.VirTerManager;
import com.tbit.uqbike.util.ProtocolUtil;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

public class TcpClientTest {
    public static AtomicLong sendCnt = new AtomicLong();
    public static AtomicLong recvCnt = new AtomicLong();

    @SuppressWarnings("resource")
    public static void main(String[] args) throws Exception {
        for (String str : args) {
            System.out.println(str);
        }
        List<AsyncClientHandler> list = new LinkedList<>();
        for (int i = 0; i < 20000; i++) {
            AsyncClientHandler client = new AsyncClientHandler("192.168.1.98", 8080);
            list.add(client);
            client.ter = new VirTer(VirTerManager.protocol206, VirTerManager.strategy206, VirTerManager.netFactory206);
            client.ter.setTid(Long.toString(System.currentTimeMillis() % 10000000));
            client.beginConnect();
        }
        //AsyncClientHandler client = new AsyncClientHandler("www.baidu.com", 80);
        while (true) {
//            System.out.println("请输入请求消息：");
//            Scanner scanner = new Scanner(System.in);
//            String msg = scanner.nextLine();
            String msg = "AAAA001A0405C8000006700537424FA640068CBAE720110460FE";
            byte[] bs = ProtocolUtil.BinGetBcdBytes(msg, msg.length() >> 1);
            int connCnt = 0;
            for (AsyncClientHandler client : list) {
                if (Objects.equals(client.getStatus(), TcpClientStatus.Normal)) {
                    client.beginConnect();
                } else if (Objects.equals(client.getStatus(), TcpClientStatus.Connected)) {
                    client.sendBytes(VirTerManager.protocol206.getPkg(client.ter, TerEvent.Heart));
                    connCnt++;
                }
            }
            Thread.sleep(1000 * 15);
            long s = sendCnt.get();
            long r = recvCnt.get();
            System.out.println(String.format("Conn:%d,Send:%d,Recv:%d,Dif:%d", connCnt, s, r, Math.abs(s - r)));
        }
    }
}