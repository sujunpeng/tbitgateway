package com.tbit.uqbike.robot;

import java.util.Date;
import java.util.Objects;

import com.tbit.uqbike.robot.net.INetFactory;
import com.tbit.uqbike.robot.net.INetFun;

/**
 * Created by MyWin on 2018/3/7 0007.
 */
public class VirTer implements Runnable {
    /**
     * 开机时间
     */
    private Date openDt;
    /**
     * 最后一条有效报文的时间
     */
    private Date lastPkgDt;
    /**
     * 登录标志
     */
    private boolean login;

    private IProtocol protocol;
    private IStrategy strategy;
    private INetFactory netFactory;
    private INetFun net;
    private String tid;

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public Date getOpenDt() {
        return openDt;
    }

    public void setOpenDt(Date openDt) {
        this.openDt = openDt;
    }

    public Date getLastPkgDt() {
        return lastPkgDt;
    }

    public void setLastPkgDt(Date lastPkgDt) {
        this.lastPkgDt = lastPkgDt;
    }

    public boolean isLogin() {
        return login;
    }

    public void setLogin(boolean login) {
        this.login = login;
    }

    public IProtocol getProtocol() {
        return protocol;
    }

    public void setProtocol(IProtocol protocol) {
        this.protocol = protocol;
    }

    public IStrategy getStrategy() {
        return strategy;
    }

    public void setStrategy(IStrategy strategy) {
        this.strategy = strategy;
    }

    public INetFactory getNetFactory() {
        return netFactory;
    }

    public void setNetFactory(INetFactory netFactory) {
        this.netFactory = netFactory;
    }

    public INetFun getNet() {
        return net;
    }

    public void setNet(INetFun net) {
        this.net = net;
    }

    @Override
    public void run() {
        TerEvent event = strategy.selectEvent(this);
        if (Objects.equals(event, TerEvent.None)) {
            return;
        }
        byte[] pkg = protocol.getPkg(this, event);
        if (null != pkg && pkg.length >= 0) {
            if (null == net) {
                net = netFactory.createNet(this);
            }
            if (null != net) {
                net.sendData(pkg);
            }
        }
    }

    public VirTer(IProtocol protocol, IStrategy strategy, INetFactory netFactory) {
        this.openDt = new Date();
        this.protocol = protocol;
        this.strategy = strategy;
        this.netFactory = netFactory;
    }
}
