package com.tbit.uqbike.robot.manager;

import com.tbit.uqbike.robot.*;
import com.tbit.uqbike.robot.net.*;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by MyWin on 2018/3/7 0007.
 */
public class VirTerManager {
    public static StaticHelper stat = new StaticHelper();
    public static IProtocol protocol206 = new Wa206Protocol();
    public static IStrategy strategy206 = new Wa206Strategy();
    public static INetFactory netFactory206 = new NetFactory();

    public static void main(String[] args) {
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        IProtocol protocol = new Wa206Protocol();
        IStrategy strategy = new Wa206Strategy();
        INetFactory netFactory = new NetFactory();
        // 第二个参数为首次执行的延时时间，第三个参数为定时执行的间隔时间
        service.scheduleAtFixedRate(new VirTer(protocol, strategy, netFactory), 10, 1, TimeUnit.SECONDS);
    }
}
