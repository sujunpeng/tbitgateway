package com.tbit.uqbike.robot;

/**
 * Created by MyWin on 2018/3/7 0007.
 */
public interface IStrategy {
    TerEvent selectEvent(VirTer ter);
}
