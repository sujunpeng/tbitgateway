package com.tbit.uqbike.robot;

import com.tbit.uqbike.util.ProtocolUtil;

import java.util.List;

/**
 * Created by MyWin on 2018/3/7 0007.
 */
public class Wa206Protocol implements IProtocol {
    @Override
    public byte[] getPkg(VirTer ter, TerEvent event) {
        byte[] data = null;
        int i = 0;
        switch (event) {
            case Heart:
                data = new byte[26];
                data[i++] = (byte) 0xaa;
                data[i++] = (byte) 0xaa;

                data[i++] = (byte) 0x00;
                data[i++] = (byte) 0x1a;
                data[i++] = (byte) 0x04;
                data[i++] = (byte) 0x05;
                data[i++] = (byte) 0xc8;
                data[i++] = (byte) 0x00;
                data[i++] = (byte) 0x00;
                data[i++] = (byte) 0x06;

                data[i++] = (byte) 0x88;
                int temp = Integer.parseInt(ter.getTid());
                data[i++] = (byte) ((temp >>> 24) & 0xff);
                data[i++] = (byte) ((temp >>> 16) & 0xff);
                data[i++] = (byte) ((temp >>> 8) & 0xff);
                data[i++] = (byte) ((temp >>> 0) & 0xff);

                data[i++] = (byte) 0xA6;
                data[i++] = (byte) 0x40;
                data[i++] = (byte) 0x06;
                data[i++] = (byte) 0x8C;
                data[i++] = (byte) 0xBA;
                data[i++] = (byte) 0xE7;
                data[i++] = (byte) 0x20;
                data[i++] = (byte) 0x11;
                data[i++] = (byte) 0x04;
                temp = ProtocolUtil.GetCrcByteArray(data, 2, data.length - 4);
                data[i++] = (byte) ((temp >>> 8) & 0xff);
                data[i++] = (byte) ((temp >>> 0) & 0xff);
                break;
            default:
                data = new byte[0];
        }
        return data;
    }

    @Override
    public List<byte[]> spiltStream(VirTer ter, byte[] data) {
        return null;
    }

    @Override
    public void handlePkg(VirTer ter, byte[] pkg) {

    }
}
