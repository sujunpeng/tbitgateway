package com.tbit.uqbike.util;

/**
 * Created by MyWin on 2018/3/15 0015.
 */
public class Pair<K, V> {
    private K key;
    private V value;

    public K getKey() {
        return this.key;
    }

    public V getValue() {
        return this.value;
    }

    public Pair(K var1, V var2) {
        this.key = var1;
        this.value = var2;
    }
}
