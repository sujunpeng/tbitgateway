package com.tbit.uqbike.util;

import java.util.LinkedHashMap;

/**
 * LRU缓存
 *
 * @param <K>
 * @param <V>
 * @author Leon
 */
public class LRUCache<K, V> extends LinkedHashMap<K, V> {
    private static final long serialVersionUID = 1L;
    /*默认100万条数据缓存*/
    static final int DEFAULT_CAPACITY = 2 << 19;

    private int cacheSize;

    public LRUCache() {
        this.cacheSize = DEFAULT_CAPACITY;
    }

    public LRUCache(int cacheSize) {
        this.cacheSize = cacheSize;
    }

    @Override
    protected boolean removeEldestEntry(java.util.Map.Entry<K, V> eldest) {
        return size() > this.cacheSize;
    }
}
