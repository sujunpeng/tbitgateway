package com.tbit.uqbike.util;

import java.util.LinkedList;

/**
 * Created by MyWin on 2017/5/9.
 */
public interface IBatHandle<T> extends Runnable {
    void insertData(LinkedList<IBatHandle> insertList);

    void setData(T t);

    T getData();
}
