package com.tbit.uqbike.util;

import io.netty.buffer.ByteBuf;

import java.util.Arrays;

/**
 * Created by John on 2016/12/21.
 */
public class ByteUtil {
    public static final String HEX = "0123456789ABCDEF";
    public static final String[] HEX_ARRAY = new String[]{
            "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "0A", "0B", "0C", "0D", "0E", "0F",
            "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "1A", "1B", "1C", "1D", "1E", "1F",
            "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "2A", "2B", "2C", "2D", "2E", "2F",
            "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "3A", "3B", "3C", "3D", "3E", "3F",
            "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "4A", "4B", "4C", "4D", "4E", "4F",
            "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "5A", "5B", "5C", "5D", "5E", "5F",
            "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "6A", "6B", "6C", "6D", "6E", "6F",
            "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "7A", "7B", "7C", "7D", "7E", "7F",
            "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "8A", "8B", "8C", "8D", "8E", "8F",
            "90", "91", "92", "93", "94", "95", "96", "97", "98", "99", "9A", "9B", "9C", "9D", "9E", "9F",
            "A0", "A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "AA", "AB", "AC", "AD", "AE", "AF",
            "B0", "B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8", "B9", "BA", "BB", "BC", "BD", "BE", "BF",
            "C0", "C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8", "C9", "CA", "CB", "CC", "CD", "CE", "CF",
            "D0", "D1", "D2", "D3", "D4", "D5", "D6", "D7", "D8", "D9", "DA", "DB", "DC", "DD", "DE", "DF",
            "E0", "E1", "E2", "E3", "E4", "E5", "E6", "E7", "E8", "E9", "EA", "EB", "EC", "ED", "EE", "EF",
            "F0", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "FA", "FB", "FC", "FD", "FE", "FF"
    };

    public static String BytesToHexString(byte[] barr) {
        StringBuffer sb = new StringBuffer();
        for (byte b : barr) {
            sb.append(ByteToHexString(b));
        }
        return sb.toString();
    }

    public static String BytesToHexString(ByteBuf bb, int index, int len) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < len; i++) {
            sb.append(ByteToHexString(bb.getByte(index + i)));
        }
        return sb.toString();
    }

    public static String BytesToHexString(byte[] barr, int index, int len) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < len; i++) {
            byte b = barr[index + i];
            sb.append(ByteToHexString(b));
        }
        return sb.toString();
    }

    public static String ByteToHexString(byte b) {
        return HEX_ARRAY[b & 0xff];
    }

    public static byte HexStringToByte(String hex) {
        String temp = hex;
        if (hex.length() == 1) {
            temp = "0" + hex;
        }
        int index = Arrays.binarySearch(HEX_ARRAY, temp.substring(0, 2));
        return (byte) index;
    }

    public static byte[] HexStringToBytesEndMask(String hex, char ch) {
        String str = hex;
        if (str.length() % 2 == 1) {
            str += ch;
        }
        return HexStringToBytesEndMask(str, ch, str.length() / 2);
    }

    public static byte[] HexStringToBytesEndMask(String hex, char ch, int len) {
        byte[] data = new byte[len];
        String str = hex;
        if (str.length() % 2 == 1) {
            str += ch;
        }
        byte dfByte = (byte) Integer.parseInt(new String(new char[]{ch, ch}), 16);
        int strlen = str.length() / 2;
        for (int i = 0; i < len; i++) {
            if (i < strlen) {
                data[i] = (byte) Integer.parseInt(str.substring(i << 1, (i << 1) + 2), 16);
                ;
            } else {
                data[i] = dfByte;
            }
        }
        return data;
    }

    public static int getUnsignedShort(byte[] bs, int index) {
        return ((bs[index + 0] & 0xff) << 8
                | (bs[index + 1] & 0xff));
    }

    public static int getShort(byte[] bs, int index) {
        return (bs[index + 0] << 8
                | (bs[index + 1] & 0xff));
    }

    public static int getUnsignedInt(byte[] bs, int index) {
        return ((bs[index + 0] & 0xff) << 24
                | (bs[index + 1] & 0xff) << 16
                | (bs[index + 1] & 0xff) << 8
                | (bs[index + 1] & 0xff));
    }

    public static int getInt(byte[] bs, int index) {
        return ((bs[index + 0]) << 24
                | (bs[index + 1] & 0xff) << 16
                | (bs[index + 1] & 0xff) << 8
                | (bs[index + 1] & 0xff));
    }
}
