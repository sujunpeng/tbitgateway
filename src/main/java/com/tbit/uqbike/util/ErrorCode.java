package com.tbit.uqbike.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by MyWin on 2017/4/27.
 */
public class ErrorCode {
    /**
     * 操作成功
     */
    public static final int OK = 0;
    /**
     * 校验和错误
     */
    public static final int ErrorCheckSum = -1;
    /**
     * 起始位不匹配
     */
    public static final int ErrorStartCode = -2;
    /**
     * 未知的操作命令
     */
    public static final int ErrorUnkonwCmd = -3;
    /**
     * 时间太老
     */
    public static final int ErroeTimeOlder = -4;
    /**
     * 过滤静态飘逸
     */
    public static final int ErrorFilterStopMove = -5;
    /**
     * 经纬度错误
     */
    public static final int ErrorLngLatError = -6;
    /**
     * 终端不在线
     */
    public static final int ErrorTerOffline = -7;
    public static final int ErrorConnNotExist = -8;
    public static final int ErrorCtxNotExist = -9;
    public static final int ErrorDownBufEmpty = -10;
    public static final int ErrorNotSupport = -11;
    public static final int ErrorAnalyzePkgError = -12;


    /**
     * 字典
     */
    private static final Map<Integer, String> _errorDic = new HashMap<Integer, String>() {
        {
            put(0, "操作成功");
            put(-1, "校验和错误");
            put(-2, "起始位不匹配");
            put(-3, "未知的操作命令");
            put(-4, "时间不合法");
            put(-5, "过滤静态飘逸");
            put(-6, "经纬度错误");
            put(-7, "终端不在线");
        }
    };

    /**
     * 解析成文字
     *
     * @param code
     * @return
     */
    public static String ErrorString(Integer code) {
        String str;
        if (_errorDic.containsKey(code)) {
            str = _errorDic.get(code);
        } else {
            str = code.toString();
        }
        return str;
    }
}
