package com.tbit.uqbike.util;

/**
 * Created by MyWin on 2018/3/14 0014.
 */
public interface ITurnTS<T, S> {
    S turn(T t);
}
