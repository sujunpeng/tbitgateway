package com.tbit.uqbike.util;

import com.tbit.uqbike.tergateway.config.TerGatewayConfig;
import com.tbit.uqbike.tergateway.data.TerGatewayData;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by MyWin on 2018/3/23 0023.
 */
public class TurnTidToHashSet implements ITurnTS<String, Pair<String, List<Pair<String, String>>>> {
    @Override
    public Pair<String, List<Pair<String, String>>> turn(String s) {
        List<Pair<String, String>> value = new LinkedList<>();
        value.add(new Pair<String, String>(TerGatewayData.terMqRouteKey, TerGatewayConfig.routeKey));
        String tid = TerGatewayData.getTerHashId(s);
        Pair<String, List<Pair<String, String>>> item = new Pair<>(tid, value);
        return item;
    }
}
