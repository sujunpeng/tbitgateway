package com.tbit.uqbike.util;

import com.tbit.uqbike.tergateway.data.TerGatewayData;

/**
 * Created by MyWin on 2018/3/19 0019.
 */
public class TurnTidToOrderKey implements ITurnT<String> {
    @Override
    public String turn(String s) {
        return String.format("%s.%s", TerGatewayData.REIDS_ORDER_LIST_TER, s);
    }
}
