package com.tbit.uqbike.util;

import com.alibaba.fastjson.JSON;
import com.tbit.uqbike.tergateway.data.TerGatewayData;
import com.tbit.uqbike.tergateway.entity.CacheData;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by MyWin on 2018/3/14 0014.
 */
public class TurnCacheDataToHashSet implements ITurnTS<CacheData, Pair<String, List<Pair<String, String>>>> {
    // 电量、位置、新数据包、状态、新终端、协议
    private int batCnt = 0;
    private int posCnt = 0;
    private int activeCnt = 0;
    private int statusCnt = 0;
    private int keyCnt = 0;
    private int protoCnt = 0;
    private int totalCnt = 0;
    private int volCnt = 0;

    @Override
    public Pair<String, List<Pair<String, String>>> turn(CacheData cacheData) {
        List<Pair<String, String>> values = new LinkedList<>();
        String hashId = TerGatewayData.getTerHashId(cacheData.getSn());
        Pair<String, List<Pair<String, String>>> pair = new Pair<>(hashId, values);
        if (cacheData.getLastBattery() != null) {
            values.add(new Pair<String, String>(TerGatewayData.terLastBattery, JSON.toJSONString(cacheData.getLastBattery())));
            batCnt++;
        }
        if (cacheData.getLastPos() != null) {
            values.add(new Pair<String, String>(TerGatewayData.terLastPos, JSON.toJSONString(cacheData.getLastPos())));
            posCnt++;
        }
        if (cacheData.getStatus() != null) {
            values.add(new Pair<String, String>(TerGatewayData.terLastStatusKey, JSON.toJSONString(cacheData.getStatus())));
            statusCnt++;
        }
        if (cacheData.getLastPkgDt() != null) {
            values.add(new Pair<String, String>(TerGatewayData.terLastPkgDt, DateUtil.ymdHmsDU.format(cacheData.getLastPkgDt())));
            values.add(new Pair<String, String>(TerGatewayData.terOnlineFlag, Long.toString(cacheData.getLastPkgDt().getTime())));
            activeCnt++;
        }
        if (cacheData.getRouteKey() != null && !cacheData.getRouteKey().isEmpty()) {
            values.add(new Pair<String, String>(TerGatewayData.terMqRouteKey, cacheData.getRouteKey()));
            keyCnt++;
        }
        if (cacheData.getProtocolData() != null) {
            values.add(new Pair<String, String>(TerGatewayData.terProtoclData, JSON.toJSONString(cacheData.getProtocolData())));
            protoCnt++;
        }
        if (cacheData.getVol() != null) {
            values.add(new Pair<String, String>(TerGatewayData.terLastVol, JSON.toJSONString(cacheData.getVol())));
            volCnt++;
        }
        totalCnt++;
        return pair;
    }

    public void resetCnt() {
        batCnt = 0;
        posCnt = 0;
        activeCnt = 0;
        statusCnt = 0;
        keyCnt = 0;
        protoCnt = 0;
        totalCnt = 0;
        volCnt = 0;
    }

    public int getBatCnt() {
        return batCnt;
    }

    public int getPosCnt() {
        return posCnt;
    }

    public int getActiveCnt() {
        return activeCnt;
    }

    public int getStatusCnt() {
        return statusCnt;
    }

    public int getKeyCnt() {
        return keyCnt;
    }

    public int getProtoCnt() {
        return protoCnt;
    }

    public int getTotalCnt() {
        return totalCnt;
    }
}
