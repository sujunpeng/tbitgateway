package com.tbit.uqbike.util;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by MyWin on 2018/3/13 0013.
 */
public class CollExUtils {
    /**
     * 拆分集合
     *
     * @param list
     * @param size
     * @param <T>
     * @return
     */
    public static <T> List<List<T>> spiltList(List<T> list, Integer size) {
        List<List<T>> listRet = new LinkedList<>();
        List<T> temp = new LinkedList<>();
        int currSize = 0;
        for (T t : list) {
            temp.add(t);
            currSize++;
            if (currSize >= size) {
                listRet.add(temp);
                temp = new LinkedList<>();
                currSize = 0;
            }
        }
        if (!temp.isEmpty()) {
            listRet.add(temp);
        }
        return listRet;
    }

    public static <K, T> List<List<T>> spiltMap(Map<K, T> map, Integer size) {
        List<List<T>> listRet = new LinkedList<>();
        List<T> temp = new LinkedList<>();
        int currSize = 0;
        Set<Map.Entry<K, T>> sets = map.entrySet();
        for (Map.Entry<K, T> t : sets) {
            temp.add(t.getValue());
            currSize++;
            if (currSize >= size) {
                listRet.add(temp);
                temp = new LinkedList<>();
                currSize = 0;
            }
        }
        if (!temp.isEmpty()) {
            listRet.add(temp);
        }
        return listRet;
    }

    public static <T> List<List<T>> spiltListEx(List<T> list, Integer size, ITurnT<T> turn) throws Exception {
        if (null == turn) {
            throw new Exception("turn can not null");
        }
        List<List<T>> listRet = new LinkedList<>();
        List<T> temp = new LinkedList<>();
        int currSize = 0;
        for (T t : list) {
            temp.add(turn.turn(t));
            currSize++;
            if (currSize >= size) {
                listRet.add(temp);
                temp = new LinkedList<>();
                currSize = 0;
            }
        }
        if (!temp.isEmpty()) {
            listRet.add(temp);
        }
        return listRet;
    }

    public static <T> List<List<T>> spiltSet(Set<T> set, Integer size, ITurnT<T> turn) {
        List<List<T>> listRet = new LinkedList<>();
        List<T> temp = new LinkedList<>();
        int currSize = 0;
        for (T t : set) {
            if (null != turn) {
                temp.add(turn.turn(t));
            } else {
                temp.add(t);
            }
            currSize++;
            if (currSize >= size) {
                listRet.add(temp);
                temp = new LinkedList<>();
                currSize = 0;
            }
        }
        if (!temp.isEmpty()) {
            listRet.add(temp);
        }
        return listRet;
    }

    public static <T, S> List<List<S>> spiltSetToS(Set<T> set, Integer size, ITurnTS<T, S> turn) throws Exception {
        if (null == turn) {
            throw new Exception("turn can not null");
        }
        List<List<S>> listRet = new LinkedList<>();
        List<S> temp = new LinkedList<>();
        int currSize = 0;
        for (T t : set) {
            temp.add(turn.turn(t));
            currSize++;
            if (currSize >= size) {
                listRet.add(temp);
                temp = new LinkedList<>();
                currSize = 0;
            }
        }
        if (!temp.isEmpty()) {
            listRet.add(temp);
        }
        return listRet;
    }

    public static <T> LinkedList<ArrayList<T>> addItemToList(LinkedList<ArrayList<T>> list, T t, Integer size) {
        ArrayList<T> subList = null;
        if (list.isEmpty()) {
            subList = new ArrayList<>();
            list.addLast(subList);
        } else {
            subList = list.getLast();
            if (subList.size() >= size) {
                subList = new ArrayList<>();
                list.addLast(subList);
            }
        }
        subList.add(t);
        return list;
    }

    /**
     * 拆分集合，并转化对象
     *
     * @param list
     * @param size
     * @param turn
     * @param <T>
     * @param <S>
     * @return
     * @throws Exception
     */
    public static <T, S> List<List<S>> spiltList(List<T> list, Integer size, ITurnTS<T, S> turn) throws Exception {
        if (null == turn) {
            throw new Exception("turn can not null");
        }
        List<List<S>> listRet = new LinkedList<>();
        List<S> temp = new LinkedList<>();
        int currSize = 0;
        for (T t : list) {
            temp.add(turn.turn(t));
            currSize++;
            if (currSize >= size) {
                listRet.add(temp);
                temp = new LinkedList<>();
                currSize = 0;
            }
        }
        if (!temp.isEmpty()) {
            listRet.add(temp);
        }
        return listRet;
    }

    public static <T> List<T> turnList(List<T> list, ITurnT<T> trun) {
        List<T> ret = new LinkedList<>();
        for (T t : list) {
            ret.add(trun.turn(t));
        }
        return ret;
    }
}
