package com.tbit.uqbike.util;

/**
 * Created by MyWin on 2018/3/14 0014.
 */
public interface ITurnT<T> {
    T turn(T t);
}
