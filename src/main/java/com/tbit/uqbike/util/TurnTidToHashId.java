package com.tbit.uqbike.util;

import com.tbit.uqbike.tergateway.data.TerGatewayData;

/**
 * Created by MyWin on 2018/3/14 0014.
 */
public class TurnTidToHashId implements ITurnT<String> {
    @Override
    public String turn(String str) {
        return TerGatewayData.getTerHashId(str);
    }
}
