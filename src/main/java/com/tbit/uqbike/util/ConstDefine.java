package com.tbit.uqbike.util;

/**
 * Created by MyWin on 2017/5/8.
 */
public class ConstDefine {
    //region POINT_TYPE
    public static final int POINT_TYPE_UNKNOW = 0;
    public static final int POINT_TYPE_GPS = 1;
    public static final int POINT_TYPE_CELL = 2;
    public static final int POINT_TYPE_WIFI = 3;
    public static final int POINT_TYPE_BEIDOU = 4;
    //endregion

    //region MQ_MSG_ID
    public static final int MQ_MSG_ID_CONTROL_TER = 10001;
    public static final int MQ_MSG_ID_TER_SYNC_CHANGE = 10002;

    public static final int MQ_MSG_ID_CONTROL_DOWN_RSP = 20001;

    public static final int MQ_MSG_ID_CONTROL_TER_RSP = 30001;

    public static final int MQ_MSG_ID_ALARM_PUSH = 40001;
    public static final int MQ_MSG_ID_BATTERY_PUSH = 40002;
    public static final int MQ_MSG_ID_POS_PUSH = 40003;
    public static final int MQ_MSG_ID_EVENT_PUSH = 40004;
    public static final int MQ_MSG_ID_STATUS_CHANGE_PUSH = 40005;
    public static final int MQ_MSG_ID_AROUND_BLES_PUSH = 40006;
    public static final int MQ_MSG_ID_TER_PKG_DT_PUSH = 40007;

    public static final int MQ_MSG_ID_TER_PP = 50001;
    public static final int MQ_MSG_ID_PLATFORM_PP = 50002;
    public static final int MQ_MSG_ID_ORDER_RECORD = 50003;
    //endregion

    //region SYS_IDENT
    public static final String SYS_IDENT_MQ = "MQ";
    public static final String SYS_IDENT_SYSTEM = "SYSTEM";
    public static final String SYS_IDENT_REDIS = "REDIS";
    //endregion

    //region CONTROL_TYPE
    public static final String CONTROL_TYPE_GET = "get";
    public static final String CONTROL_TYPE_SET = "set";
    public static final String CONTROL_TYPE_CONTROL = "control";
    public static final String CONTROL_TYPE_VOICE = "voice";
    public static final String CONTROL_TYPE_SMS = "sms";
    //endregion

    //region TER_CONFIG
    public static final int TER_CONFIG_SYNC_TIME = 0;
    public static final int TER_CONFIG_SYNC_VALUE = 1;
    public static final int TER_CONFIG_LOG_CONFIG = 2;
    public static final int TER_CONFIG_TER_UPDATE = 3;
    //endregion
    /**
     * 经度0.1
     */
    public static final int TEMP_KELVINS = 2731;
}
