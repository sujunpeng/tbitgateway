package com.tbit.uqbike.util;

/**
 * Created by John on 2016/12/21.
 */
public class StringUtil {
    /**
     * 字符串是否为空
     *
     * @param str
     * @return
     */
    public static boolean IsNullOrEmpty(String str) {
        return str == null || str.length() == 0;
    }

    /**
     * 空字符串
     */
    public static final String Empty = "";

    /**
     * fdsa
     *
     * @param str fdas
     * @param ch  fdsa
     * @return fdas
     */
    public static String TrimEnd(String str, char ch) {
        char[] chs = str.toCharArray();
        int end = chs.length;
        while (--end >= 0) {
            if (chs[end] == ch) {
                continue;
            } else {
                break;
            }
        }
        return new String(chs, 0, end + 1);
    }

    public static String TrimHead(String str, char ch) {
        char[] chs = str.toCharArray();
        int head = 0;
        while (head < chs.length) {
            if (chs[head] == ch) {
                head++;
            } else {
                break;
            }
        }
        return new String(chs, head, chs.length);
    }
}
