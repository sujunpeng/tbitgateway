package com.tbit.uqbike.util;

import com.tbit.uqbike.tergateway.entity.CacheData;
import com.tbit.uqbike.tergateway.pojo.TerBattery;
import com.tbit.uqbike.tergateway.pojo.TerInfo;
import com.tbit.uqbike.tergateway.pojo.TerOnline;
import com.tbit.uqbike.tergateway.pojo.TerPos;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by MyWin on 2018/3/14 0014.
 */
public class BatUpdateResult {
    public LinkedList<ArrayList<TerInfo>> infoLists;
    public LinkedList<ArrayList<TerPos>> posLists;
    public LinkedList<ArrayList<TerBattery>> batLists;
    public LinkedList<ArrayList<TerOnline>> batOnlines;

    private int size = 500;

    private int infoCnt;
    private int posCnt;
    private int batCnt;
    private int onlineCnt;
    private int totalCnt;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getInfoCnt() {
        return infoCnt;
    }

    public void setInfoCnt(int infoCnt) {
        this.infoCnt = infoCnt;
    }

    public int getPosCnt() {
        return posCnt;
    }

    public void setPosCnt(int posCnt) {
        this.posCnt = posCnt;
    }

    public int getBatCnt() {
        return batCnt;
    }

    public void setBatCnt(int batCnt) {
        this.batCnt = batCnt;
    }

    public int getOnlineCnt() {
        return onlineCnt;
    }

    public void setOnlineCnt(int onlineCnt) {
        this.onlineCnt = onlineCnt;
    }

    public int getTotalCnt() {
        return totalCnt;
    }

    public void setTotalCnt(int totalCnt) {
        this.totalCnt = totalCnt;
    }

    public BatUpdateResult(int size) {
        this.size = size;
        infoLists = new LinkedList<>();
        posLists = new LinkedList<>();
        batLists = new LinkedList<>();
        batOnlines = new LinkedList<>();
    }

    public void resetCnt() {
        infoCnt = 0;
        posCnt = 0;
        batCnt = 0;
        onlineCnt = 0;
        totalCnt = 0;
    }

    public void readCacheData(CacheData cacheData) {
        if (cacheData.getLastBattery() != null) {
            CollExUtils.addItemToList(batLists, cacheData.getLastBattery(), this.size);
            batCnt++;
        }
        if (cacheData.getLastPos() != null) {
            CollExUtils.addItemToList(posLists, cacheData.getLastPos(), this.size);
            posCnt++;
        }
        if (cacheData.getTerInfo() != null) {
            CollExUtils.addItemToList(infoLists, cacheData.getTerInfo(), this.size);
            infoCnt++;
        }
        if (cacheData.getLastPkgDt() != null) {
            CollExUtils.addItemToList(batOnlines, new TerOnline(cacheData.getSn(), cacheData.getLastPkgDt(), (byte) 1), this.size);
            onlineCnt++;
        }
        totalCnt++;
    }
}
