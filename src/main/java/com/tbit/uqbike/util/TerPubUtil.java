package com.tbit.uqbike.util;

import java.util.Arrays;

/**
 * Created by MyWin on 2017/5/8.
 */
public class TerPubUtil {

    public static byte[] getWA206BlockData(byte[] all, int signMaxSize, int index) {
        int len = all.length;
        int beginIndex = index * signMaxSize;
        int endIndex = beginIndex + signMaxSize;
        if (beginIndex < len) {
            if (endIndex < len) {
                return Arrays.copyOfRange(all, beginIndex, endIndex);
            } else {
                return Arrays.copyOfRange(all, beginIndex, len);
            }
        } else {
            return new byte[0];
        }
    }

    public static int turnBlockIdToSize(int block) {
        return (block + 1) * 240;
    }

    /**
     * 计算两点之间的距离，单位是公里
     *
     * @param lng1
     * @param lat1
     * @param lng2
     * @param lat2
     * @return
     */
    public static double calculateDistance(double lng1, double lat1, double lng2, double lat2) {
        double alpha = lat1 * 3.14 / 180;
        return Math.sqrt(Math.pow((lat1 - lat2) * 111.12, 2) + Math.pow((lng1 - lng2) * Math.cos(alpha) * 111.12, 2));
    }

    public static void main(String[] args) {
        double lo1,lo2,la1,la2;
        la1 = 22.382711666666665;
        lo1 = 113.34265;
        la2 = 22.382685;
        lo2 =113.34261777777778;
        System.out.println(Double.toString(1000 * calculateDistance(lo1, la1, lo2, la2)));
    }
}
