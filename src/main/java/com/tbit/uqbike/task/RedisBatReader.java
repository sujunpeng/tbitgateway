package com.tbit.uqbike.task;

import com.alibaba.fastjson.JSON;
import com.tbit.uqbike.TerGatewayMain;
import com.tbit.uqbike.service.db.DbService;
import com.tbit.uqbike.service.redis.RedisService;
import com.tbit.uqbike.tergateway.config.TerGatewayConfig;
import com.tbit.uqbike.tergateway.data.TerGatewayData;
import com.tbit.uqbike.tergateway.entity.TerLastStatus;
import com.tbit.uqbike.tergateway.entity.TerTempData;
import com.tbit.uqbike.tergateway.pojo.TerBattery;
import com.tbit.uqbike.tergateway.pojo.TerInfo;
import com.tbit.uqbike.tergateway.pojo.TerPos;
import com.tbit.uqbike.util.CollExUtils;
import com.tbit.uqbike.util.TurnTidToHashId;
import com.tbit.uqbike.util.TurnTidToOrderKey;

import java.util.*;

/**
 * Created by MyWin on 2018/1/25 0025.
 */
public class RedisBatReader implements Runnable {
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(RedisBatReader.class);

    private TurnTidToOrderKey turnTidToOrderKey = new TurnTidToOrderKey();
    private TurnTidToHashId turnTidToHashId = new TurnTidToHashId();

    @Override
    public void run() {
        try {
            List<String> tidlist = TerGatewayData.getAllTerTid();
            if (tidlist.isEmpty()) {
                return;
            }
            List<String> allMnoList = new ArrayList<>();
            for (String item : tidlist) {
                allMnoList.add(item);
            }
            int totalSize = allMnoList.size();
            // 获取升级配置
            List<List<String>> batUpdateList = CollExUtils.spiltListEx(allMnoList, TerGatewayConfig.iRedisBatSize, turnTidToHashId);

            // 获取离线指令配置
            List<List<String>> batOrderList = CollExUtils.spiltListEx(allMnoList, TerGatewayConfig.iRedisBatSize, turnTidToOrderKey);

            // 批量读取离线指令配置和升级配置数据
            List<String> updateList = new ArrayList<>(allMnoList.size());
            List<Long> orderList = new ArrayList<>(allMnoList.size());
            Date start = new Date();
            RedisService redis = TerGatewayMain.getRedis();
            for (List<String> item : batUpdateList) {
                List<String> list = redis.BatGetHashValue(item, TerGatewayData.terNewSoftwave);
                updateList.addAll(list);
            }
            for (List<String> item : batOrderList) {
                List<Long> list = redis.BatGetListLen(item);
                orderList.addAll(list);
            }
            Date end = new Date();
            long lMs = end.getTime() - start.getTime();
            // 进行检测并刷新内存
            int updateCnt = 0, order = 0;
            for (int i = 0; i < allMnoList.size(); i++) {
                String mno = allMnoList.get(i);
                String update = updateList.get(i);
                Long orderCnt = orderList.get(i);
                TerTempData data = TerGatewayData.getTerTempDataByMno(mno);
                if (null != update && !update.isEmpty()) {
                    data.setTerSoftInfoFlag(true);
                    updateCnt++;
                } else {
                    data.setTerSoftInfoFlag(false);
                }
                if (orderCnt > 0) {
                    data.setOfflineOrderFlag(true);
                    order++;
                } else {
                    data.setOfflineOrderFlag(false);
                }
            }
            logger.info(String.format("Bat Get [%d] Offline Order And Update Config,Order:%d,Config:%d,Use:%d", totalSize, order, updateCnt, lMs));
        } catch (Exception e) {
            logger.error("RedisBatReader", e);
        }
    }
}
