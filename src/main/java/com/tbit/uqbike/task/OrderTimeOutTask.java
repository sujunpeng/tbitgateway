package com.tbit.uqbike.task;

import com.tbit.uqbike.tergateway.data.TerGatewayData;
import com.tbit.uqbike.tergateway.entity.RemoteControl;
import com.tbit.uqbike.tergateway.entity.TerTempData;
import com.tbit.uqbike.util.StringUtil;

/**
 * Created by MyWin on 2017/5/19.
 */
public class OrderTimeOutTask implements Runnable {
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(OrderTimeOutTask.class);

    public OrderTimeOutTask(RemoteControl remoteControl) {
        this.remoteControl = remoteControl;
    }

    private RemoteControl remoteControl;

    @Override
    public void run() {
        if (null != remoteControl && !remoteControl.bTerRsp) {
            // 超时了移除指令
            if (remoteControl.iSerNO == Integer.MIN_VALUE) {
                logger.info(String.format("Del TO Order:[%s].[%s]", remoteControl.sn, remoteControl.serNO));
                TerGatewayData.getStrSerNoMap(remoteControl.serNO);
            } else {
                logger.info(String.format("Del TO Order:[%s].[%d]", remoteControl.sn, remoteControl.iSerNO));
                TerTempData terTempData = TerGatewayData.getTerTempDataByMno(remoteControl.sn);
                if (terTempData != null) {
                    terTempData.getSerNoAndDel(remoteControl.iSerNO);
                }
            }
        }
    }
}
