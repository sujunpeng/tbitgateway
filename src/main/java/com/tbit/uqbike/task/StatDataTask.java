package com.tbit.uqbike.task;

import com.tbit.uqbike.tergateway.config.GateXmlConfig;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.util.AbstractMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019-11-12 18:38
 */
public class StatDataTask implements Runnable {
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(StatDataTask.class);

    private List<AbstractMap.SimpleEntry<String, String>> statDataList = new LinkedList<>();

    private ReentrantLock lock = new ReentrantLock();

    private RabbitTemplate rabbitTemplate;
    private ConnectionFactory connectionFactory;

    public void addStatData(String key, String data) {
        try {
            lock.lock();

            statDataList.add(new AbstractMap.SimpleEntry<>(key, data));
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void run() {
        List<AbstractMap.SimpleEntry<String, String>> list;
        try {
            lock.lock();

            list = statDataList;
            statDataList = new LinkedList<>();
        } finally {
            lock.unlock();
        }
        if (null != list && !list.isEmpty()) {
            int succ = 0,fail = 0,forgo = 0;
            for (AbstractMap.SimpleEntry<String, String> kv : list) {
                try {
                    if (GateXmlConfig.statStatus) {
                        // 推送给公共平台
                        RabbitTemplate temp = getRabbitTemplate();
                        if (null != temp) {
                            temp.send(kv.getKey(), new Message(kv.getValue().getBytes("UTF-8"), new org.springframework.amqp.core.MessageProperties()));
                        }
                        succ++;
                    } else {
                        forgo++;
                    }
                } catch (Exception e){
                    fail++;
                }
            }
            logger.info(String.format("push stat data,succ:%d,fail:%d,forgo:%d",
                    succ, fail, forgo));
        }
    }


    private ConnectionFactory getConnectionFactory() {
        if (null == connectionFactory) {
            CachingConnectionFactory ccf = new CachingConnectionFactory();
            ccf.setHost(GateXmlConfig.statMqHost);
            ccf.setPort(GateXmlConfig.statMqPort);
            ccf.setUsername(GateXmlConfig.statMqUsername);
            ccf.setPassword(GateXmlConfig.statMqPassword);

            connectionFactory = ccf;
        }
        return connectionFactory;
    }

    private RabbitTemplate getRabbitTemplate() {
        if (rabbitTemplate == null) {

            ConnectionFactory cf = getConnectionFactory();
            if (null != cf) {
                rabbitTemplate = new RabbitTemplate();
                rabbitTemplate.setConnectionFactory(cf);
                rabbitTemplate.setExchange(GateXmlConfig.statMqExchange);
            }
        }
        return rabbitTemplate;
    }
}
