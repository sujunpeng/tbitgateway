package com.tbit.uqbike.task;

import com.tbit.uqbike.tergateway.entity.RemoteControl;

import java.util.Comparator;

/**
 * Created by MyWin on 2018/4/17 0017.
 */
public class RemoteControlComparator implements Comparator<RemoteControl> {

    @Override
    public int compare(RemoteControl o1, RemoteControl o2) {
        int i = (int) ((o1.planDownDt - o2.planDownDt) / 1000L);
        if (i != 0) {
            return i;
        } else {
            return o1.sn.compareTo(o2.sn);
        }
    }
}
