package com.tbit.uqbike.task;

import com.alibaba.fastjson.JSONObject;
import com.tbit.uqbike.TerGatewayMain;
import com.tbit.uqbike.service.db.DbService;
import com.tbit.uqbike.service.redis.RedisService;
import com.tbit.uqbike.tergateway.config.GateXmlConfig;
import com.tbit.uqbike.tergateway.log.LOG;
import com.tbit.uqbike.tergateway.pojo.TerOnline;

import java.util.List;

/**
 * @author MyWin E-mail:335918956@qq.com
 * @version 1.0
 * @createTime 2019-10-15 17:33
 * 分布式定时任务检测终端是否离线
 */
public class TerOfflineCheckTask implements Runnable {
    private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(RedisBatWriterSec.class);

    @Override
    public void run() {
        DbService db = TerGatewayMain.getDbService();
        RedisService rs = TerGatewayMain.getRedis();
        try {
            if (!GateXmlConfig.onlineFlag) {
                return;
            }
            List<TerOnline> items = db.checkTimeOutOfflineTer();
            if (!items.isEmpty()) {
                if (LOG.bOnline) {
                    LOG.ONLINE.info(String.format("设备:%s 超时离线", JSONObject.toJSONString(items)));
                }
            }
        } catch (Exception e) {
            logger.error("TerOfflineCheckTask", e);
        }
    }
}
