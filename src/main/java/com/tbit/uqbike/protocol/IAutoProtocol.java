package com.tbit.uqbike.protocol;

import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import io.netty.buffer.ByteBuf;

/**
 * Created by MyWin on 2018/5/16 0016.
 */
public interface IAutoProtocol {
    AProtocol autoProtocol(ByteBuf buf);

    AProtocol autoProtocol(byte[] pkg);
}
