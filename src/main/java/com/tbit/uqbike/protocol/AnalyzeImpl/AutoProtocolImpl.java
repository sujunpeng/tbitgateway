package com.tbit.uqbike.protocol.AnalyzeImpl;

import io.netty.buffer.ByteBuf;

/**
 * Created by MyWin on 2018/5/16 0016.
 */
public class AutoProtocolImpl extends AAutoProtocol {
    static {
        initData();
    }

    @Override
    public AProtocol autoProtocol(ByteBuf buf) {
        int baseIndex = buf.readerIndex();
        if (buf.readableBytes() >= 2) {
            if (buf.getUnsignedByte(0 + baseIndex) == 0xAA && buf.getUnsignedByte(1 + baseIndex) == 0xAA) {
                return getProtocol(WA206DeEnCoder.protocolName);
            } else if (buf.getByte(0 + baseIndex) == TurnDeEnCoder.PRO_HEAD[0] && buf.getByte(1 + baseIndex) == TurnDeEnCoder.PRO_HEAD[1]) {
                return getProtocol(TurnDeEnCoder.protocolName);
            } else if ((buf.getByte(0 + baseIndex) == Gt06DeEnCoder.PRO_HEAD[0] && buf.getByte(1 + baseIndex) == Gt06DeEnCoder.PRO_HEAD[1])
                    || (buf.getByte(0 + baseIndex) == Gt06DeEnCoder.PRO_HEAD_EX[0] && buf.getByte(1 + baseIndex) == Gt06DeEnCoder.PRO_HEAD_EX[1])) {
                return getProtocol(Gt06DeEnCoder.protocolName);
            }
        }
        if (buf.getByte(baseIndex) == Jt808DeEnCoder.PKG_FLAG) {
            return getProtocol(Jt808DeEnCoder.protocolName);
        }
        if (buf.getByte(baseIndex) == '[') {
            return getProtocol(TbitTextDeEnCoder.protocolName);
        }
        if (buf.readableBytes() >= 3) {
            if (buf.getByte(2 + baseIndex) == '{') {
                return getProtocol(WA205DeEnCoder.protocolName);
            }
        }
        if (buf.readableBytes() >= 26) {
            // 版本号 以及json的起始字符
            if (buf.getByte(1) == 0x01 || buf.getByte(1) == 0x02) {
                if (buf.readableBytes() >= 26 + 2) {
                    if (buf.getByte(26) == '{') {
                        return getProtocol(MeituanDeEnCoder.protocolName);
                    }
                } else if (buf.readableBytes() == 26) {
                    if (buf.getByte(0) == 0x53) {
                        return getProtocol(MeituanDeEnCoder.protocolName);
                    }
                }
            }
        }
        return null;
    }

    private static void initData() {
        if (protocolMap.isEmpty()) {
            regProtocol(new WA205DeEnCoder());
            regProtocol(new WA206DeEnCoder());
            regProtocol(new TurnDeEnCoder());
            regProtocol(new TbitTextDeEnCoder());
            regProtocol(new MeituanDeEnCoder());
            regProtocol(new Gt06DeEnCoder());
            regProtocol(new Jt808DeEnCoder());
        }
    }

    @Override
    public AProtocol autoProtocol(byte[] pkg) {
        initData();
        if (pkg.length >= 2) {
            if ((pkg[0] & 0xff) == 0xAA && (pkg[1] & 0xff) == 0xAA) {
                return getProtocol(WA206DeEnCoder.protocolName);
            } else if (pkg[0] == TurnDeEnCoder.PRO_HEAD[0] && pkg[1] == TurnDeEnCoder.PRO_HEAD[1]) {
                return getProtocol(TurnDeEnCoder.protocolName);
            } else if ((pkg[0] == Gt06DeEnCoder.PRO_HEAD[0] && pkg[1] == Gt06DeEnCoder.PRO_HEAD[1])
                    || (pkg[0] == Gt06DeEnCoder.PRO_HEAD_EX[0] && pkg[1] == Gt06DeEnCoder.PRO_HEAD_EX[1])) {
                return getProtocol(Gt06DeEnCoder.protocolName);
            }
        }
        if (pkg.length >= 3) {
            if (pkg[2] == '{') {
                return getProtocol(WA205DeEnCoder.protocolName);
            }
        }
        return null;
    }
}
