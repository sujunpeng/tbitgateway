package com.tbit.uqbike.protocol.AnalyzeImpl.wa206.codedifine;

import java.util.HashMap;

/**
 * @author lzl
 * @date 2021/7/6 20:06
 * @Description 简单描述这是做什么的
 */
public class CameraErrorCode {

    private static HashMap<Integer, String> ErrorCodeMap = new HashMap<>();

    public static final int DVR_ERROR_CHECK_FILEFAILED = 10;
    public static final int DVR_ERROR_CHECK_YELLOW = 11;
    public static final int DVR_ERROR_CHECK_WHITE = 12;
    public static final int DVR_ERROR_CHECK_DISTANCE = 13;
    public static final int DVR_ERROR_CHECK_DISTANCE2 = 14;
    public static final int DVR_ERROR_CHECK_POINTLOW = 15;
    public static final int DVR_ERROR_IS_RUNING = 100;

    static {
        ErrorCodeMap.put(DVR_ERROR_CHECK_FILEFAILED,"拍照检测文件坏了");
        ErrorCodeMap.put(DVR_ERROR_CHECK_YELLOW,"拍照检测失败1");
        ErrorCodeMap.put(DVR_ERROR_CHECK_WHITE,"拍照检测失败2");
        ErrorCodeMap.put(DVR_ERROR_CHECK_DISTANCE,"拍照检测距离1失败");
        ErrorCodeMap.put(DVR_ERROR_CHECK_DISTANCE2,"拍照检测距离2失败");
        ErrorCodeMap.put(DVR_ERROR_CHECK_POINTLOW,"拍照检测点比较少");
        ErrorCodeMap.put(DVR_ERROR_IS_RUNING,"运行行中");
    }

    public static String getErrorCodeString(Integer code) {
        if (null == code) {
            return "null";
        } else if (ErrorCodeMap.containsKey(code)) {
            return ErrorCodeMap.get(code);
        } else {
            return code.toString();
        }
    }

}
