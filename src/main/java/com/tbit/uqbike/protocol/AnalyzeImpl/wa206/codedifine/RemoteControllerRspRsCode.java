package com.tbit.uqbike.protocol.AnalyzeImpl.wa206.codedifine;

import java.util.HashMap;

/**
 * @author lzl
 * @date 2021/7/7 12:39
 * @Description 远程控制参数
 */
public class RemoteControllerRspRsCode {


    private static HashMap<Integer, String> rsCodeMap = new HashMap<>();

    public static final int RS_FAILED = 0x00;
    public static final int RS_SUCCESS = 0x01;
    public static final int RS_RUNING = 0x02;
    public static final int RS_OUTEL_NOT_EXIT = 0x03;
    public static final int RS_TK_OPEN = 0x04;
    public static final int RS_NOT_BACK_CAR = 0x05;
    public static final int RS_NOT_CHECK_RFID = 0x06;
    public static final int RS_NOT_CHECK_AI = 0x07;
    public static final int RS_NOT_SUPORT_AI = 0x08;


    static {
        rsCodeMap.put(RS_FAILED,"失败");
        rsCodeMap.put(RS_SUCCESS,"成功");
        rsCodeMap.put(RS_RUNING,"运动中");
        rsCodeMap.put(RS_OUTEL_NOT_EXIT,"外接电源不在位");
        rsCodeMap.put(RS_TK_OPEN,"头盔锁打开状态");
        rsCodeMap.put(RS_NOT_BACK_CAR,"马蹄锁手动操作还车，不允许平台还车");
        rsCodeMap.put(RS_NOT_CHECK_RFID,"未识别rfid标签，不允许还车");
        rsCodeMap.put(RS_NOT_CHECK_AI,"AI摄像头未识别还车站点标识");
        rsCodeMap.put(RS_NOT_SUPORT_AI,"终端不支持AI摄像头");

    }

    public static String getRsCodeString(Integer code) {
        if (null == code) {
            return "null";
        } else if (rsCodeMap.containsKey(code)) {
            return rsCodeMap.get(code);
        } else {
            return code.toString();
        }
    }

}
