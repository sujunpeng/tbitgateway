package com.tbit.uqbike.protocol.AnalyzeImpl;

import com.tbit.uqbike.protocol.IAutoProtocol;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by MyWin on 2018/5/16 0016.
 */
public abstract class AAutoProtocol implements IAutoProtocol {
    protected static Map<String, AProtocol> protocolMap;

    static {
        protocolMap = new HashMap<>();
    }

    public static void regProtocol(AProtocol protocol) {
        protocolMap.put(protocol.getProtocolName(), protocol);
    }

    public static AProtocol getProtocol(String protocolName) {
        return protocolMap.get(protocolName);
    }
}
