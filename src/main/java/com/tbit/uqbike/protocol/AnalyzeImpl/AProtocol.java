package com.tbit.uqbike.protocol.AnalyzeImpl;

import com.tbit.uqbike.protocol.IProtocol;

/**
 * Created by MyWin on 2018/5/16 0016.
 */
public abstract class AProtocol implements IProtocol {

    public abstract String getProtocolName();
}
