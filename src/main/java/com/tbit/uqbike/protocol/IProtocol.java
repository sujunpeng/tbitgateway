package com.tbit.uqbike.protocol;

import com.tbit.uqbike.tergateway.entity.AConnInfo;
import io.netty.buffer.ByteBuf;

import java.util.List;

/**
 * Created by MyWin on 2018/5/16 0016.
 */
public interface IProtocol {
    List<ATerPkg> analyzeTerPkg(AConnInfo conn, ByteBuf in);

    ByteBuf builtRemoteControlPkg(AConnInfo conn, ABaseHandleObj aBaseHandleObj);
}
