package com.tbit.uqbike.protocol;

import com.tbit.uqbike.tergateway.data.TerGatewayData;

/**
 * Created by MyWin on 2017/5/10.
 * 业务处理对象
 */
public abstract class ABaseHandleObj {

    public abstract void doBusiness();
}
