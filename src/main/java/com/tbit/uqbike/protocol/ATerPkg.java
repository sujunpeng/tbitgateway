package com.tbit.uqbike.protocol;

import com.alibaba.fastjson.annotation.JSONField;
import com.tbit.uqbike.protocol.AnalyzeImpl.AProtocol;
import com.tbit.uqbike.tergateway.entity.AConnInfo;
import com.tbit.uqbike.util.StringUtil;
import io.netty.buffer.ByteBuf;

/**
 * Created by MyWin on 2017/5/11.
 */
public abstract class ATerPkg implements ITerPkg {
    // 报文头
    public ATerPkgHead head;
    /**
     * 完整报文
     */
    @JSONField(serialize = false) // 不进行json序列化
    public byte[] signPkg;
    @JSONField(name = "设备编号")
    public String mno;
    private String proName; // 协议名
    private String connId; // 连接ID

    /**
     * 更新设备编号
     *
     * @param mno
     * @return
     */
    public boolean updateMno(String mno) {
        if (!StringUtil.IsNullOrEmpty(mno) && StringUtil.IsNullOrEmpty(this.mno)) {
            this.mno = mno;
            return true;
        }
        return false;
    }

    public String getMachineNO() {
        return mno;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getConnId() {
        return connId;
    }

    public void setConnId(String connId) {
        this.connId = connId;
    }

    public abstract void doBusiness();

    public abstract boolean autoRsp();

    //public abstract ByteBuf getRsp(ChannelHandlerContext ctx, AProtocol protocol);

    public abstract ByteBuf getRsp(AConnInfo conn, AProtocol protocol);
}
