package com.tbit.uqbike.protocol;

import java.util.List;

import org.springframework.amqp.core.Message;

/**
 * Created by MyWin on 2017/5/11.
 */
public interface IMqAnalyze {
    List<ABaseHandleObj> analyzeMqMsg(Message message);
}
